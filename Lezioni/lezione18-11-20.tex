\section{Lezione del 20/11/2018 [Marmi]}

\emph{Setting}: $ (X, \mathcal{A}, \mu, f) $ è un sistema dinamico misurabile.

\begin{lemma} \label{lem:insieme-quasi-invariante}
    Sia $ A \in \mathcal{A} $ un insieme quasi invariante, cioè tale che $ f(A) \subseteq A \cup N $ con $ \mu(N) = 0 $. Allora esiste $ A' \in \mathcal{A} $ che è $ f $-invariante ed è quasi uguale a $ A $, cioè $ \mu(A \Delta A') = 0 $.
\end{lemma}
\begin{proof}
    Basta prendere $ N' \coloneqq \bigcup_{j \geq 0} f^{-j}(N) $ e $ A' \coloneqq A \setminus N' $. Infatti se $ x \in A' $ allora $ x \in A $ ma $ x \notin N' $ cioè $ \forall j \geq 0, \ x \notin f^{-j}(N) $; quindi $ f(x) \in A \cup N $ e $ \forall j \geq 0, \ f(x) \notin f^{-j}(N) $ da cui in particolare $ f(x) \notin f^0(N) = N $ così $ f(x) \in A \setminus N' $ ovvero $ A' $ è $ f $-invariante. Inoltre per $ f $-invarianza della misura $ \mu(N') \leq \sum_{j \geq 0} \mu(f^{-j}(N)) = 0 $ così $ \mu(A\Delta A') = \mu(A\Delta(A\setminus N')) = \mu(A \cap N') \leq \mu(N') = 0 $.
\end{proof}

\begin{thm}
    Fissato un $ p \geq 1 $, le seguenti proprietà sono equivalenti:
    \begin{enumerate}[label=(\roman*)]
        \item $ (X, \mathcal{A}, \mu, f) $ è ergodico.
        \item $ (X, \mathcal{A}, \mu, f) $ è \emph{metricamente indecomponibile}, cioè $ \forall A \in \mathcal{A} : f(A) \subseteq A, \ \mu(A) > 0 \Rightarrow \mu(A) = 1 $, ovvero lo spazio delle fasi non può essere separato in due insiemi disgiunti, $ f $-invarianti ed entrambi di misura non nulla.
        \item $ (X, \mathcal{A}, \mu, f) $ ha solo integrali primi del moto banali cioè $ \forall \varphi \in L^p(X, \mathcal{A}, \mu; \R) : \varphi \circ f = \varphi $ $ \mu $-q.o. si ha che $ \varphi $ è costante $ \mu $-q.o. in $ X $.
        \item $ \forall \varphi \in L^p(X, \mathcal{A}, \mu; \R) $ la media temporale di $ \varphi $ (che esiste per il Teorema \ref{thm:Birkhoff}) è $ \mu $-q.o. uguale alla media spaziale, cioè
        \begin{equation} \label{eqn:ergodico-mediaosservabile}
            \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi \circ f^j)(x) = \int_{X} \varphi(y) \dif{\mu}(y)
        \end{equation}
       per $ \mu $-q.o. $ x \in X $.
        \item $ \forall A, B \in \mathcal{A} $
        \begin{equation}\label{eq:mescolamento_in_media}
            \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \mu(A) \mu(B)
        \end{equation}
        cioè il sistema dinamico è \emph{mescolante in media}.
    \end{enumerate}
\end{thm}
\begin{proof}
    Mostriamo le varie implicazioni.
    \begin{description}
        \item[$ (i) \Rightarrow (ii) $] Sia $ A\in\mathcal{A} : f(A) \subseteq A $ con $\mu(A) > 0 $. Poiché $ A $ è $ f $-invariante, $ \forall x\in A\ \chi_A(f^j(x)) = 1 $ e dunque $ \nu(x,A) = 1 $ da cui, per l'ergodicità, $ \mu(A) = 1 $.

        \item[$ (ii) \Rightarrow (iii) $] L'idea è che, poiché $ \varphi $ è costante sulle orbite, i suoi insiemi di livello sono $ f $-invarianti; se $ \varphi $ è costante allora un insieme di livello è tutto lo spazio e gli altri sono vuoti; se invece non fosse costante si potrebbe decomporre lo spazio in più insiemi $ f $-invarianti, in contrasto con l'ipotesi. \\
        Sia $ \varphi $ un integrale primo in $ L^p(X, \mathcal{A}, \mu; \R) $. Supponiamo ora che valga l'ipotesi più forte $ \varphi = \varphi \circ f $ ovunque. Fissato $ \gamma \in \R $, sia $ A_\gamma \coloneqq \{x \in X : \varphi(x) \leq \gamma\} $ il sottolivello che è per ipotesi misurabile. Esso è invariante poiché, preso $ y \in f(A_y) $, si ha che $ \exists x\in A_\gamma : y=f(x) $ e quindi $ \varphi(y) = \varphi(f(x)) = \varphi (x) \leq \gamma $, cioè $ y\in A_\gamma $. Perciò l'ipotesi \emph{(ii)} ci dice che $ \mu(A_\gamma) \in \{0, 1\} $. \\
        Consideriamo la funzione
        \begin{align*}
            \psi \colon \R & \to [0, +\infty) \\
            \gamma & \mapsto \mu(A_\gamma)
        \end{align*}
        che è monotona non decrescente ($ \gamma_1 < \gamma_2 \Rightarrow A_{\gamma_1} \subseteq A_{\gamma_2} \Rightarrow \mu(A_{\gamma_1}) \leq \mu(A_{\gamma_2}) $). Osserviamo che
        \[
            \lim_{\gamma \to -\infty} \mu(A_\gamma) = 0 \qquad \lim_{\gamma \to +\infty} \mu(A_\gamma) = 1
        \]
        da cui deduciamo che esiste $ \overline{\gamma} \coloneqq \inf{\{\gamma \in \R : \mu(A_\gamma) = 1\}} \in \R $. Ma essendo gli $ A_\gamma $ inscatolati
        \[
            A_{\overline{\gamma}} = \bigcap_{n \geq 1} A_{\overline{\gamma} + \frac{1}{n}} \quad \Rightarrow \quad A_{\overline{\gamma} + \frac{1}{n}} \downarrow A_{\overline{\gamma}}
        \]
        da cui, per l'Esercizio \ref{es:limitemisuredasopra}
        \[
            \mu(A_{\overline{\gamma}}) = \lim_{n \to +\infty} \mu\left(A_{\overline{\gamma} + \frac{1}{n}}\right) = 1.
        \]
        Concludiamo quindi che $ \varphi(x) = \overline{\gamma} $ per $ \mu $-q.o. $ x \in X $ essendo $ \mu(A_\gamma) = 0 $ per ogni $ \gamma < \overline{\gamma} $ e $ A_{\overline{\gamma}} $ di misura 1. \\
        Supponiamo ora che $ \varphi = \varphi \circ f $ $ \mu $-q.o. Detto allora $ B_\gamma \coloneqq f^{-1}(A_\gamma) = \{x \in X : \varphi(f(x)) \leq \gamma\} $ si ha $ \mu(A_\gamma \Delta B_\gamma) = 0 $ e pertanto $ \exists N \in \mathcal{A} $ con $ \mu(N) = 0 $ tale che $ A_\gamma = B_\gamma \Delta N $. Così $ B_\gamma = f^{-1}(A_\gamma) = f^{-1}(B_\gamma) \Delta f^{-1}(N) $. Quindi posto $ N' \coloneqq f^{-1}(N) $ per invarianza della misura si ha $ \mu(N') = 0 $ e inoltre
        \[
            f(B_\gamma \Delta N') = f(f^{-1}(B_\gamma)) \subseteq B_\gamma = (B_\gamma \Delta N') \Delta N'
        \]
        cioè $ B_\gamma \Delta N' $ è invariante a meno di un insieme di misura nulla. Per il Lemma \ref{lem:insieme-quasi-invariante} sappiamo che esiste $ B'_\gamma \in \mathcal{A} $ tale che $ f(B_\gamma) \subseteq B_\gamma $ e $ \mu(B_\gamma \Delta B'_\gamma) = 0 $. Per l'ipotesi \emph{(ii)} allora $ \mu(B'_\gamma) \in \{0, 1\} $ così\footnote{
            Se $ F, M \in \mathcal{A} $ e $ \mu(M) = 0 $ allora \[\mu(F \Delta M) = \mu((F \setminus M) \cup (M \setminus F)) = \mu(F \setminus M) + \mu(M \setminus F) = \mu(F) - \mu(F \cap M) + \mu(M \setminus F) = \mu(F)\] in quanto $ F \cap M $ e $ M \setminus F $ sono sottoinsiemi misurabili di un insieme di misura nulla.
        }
        \[
            \mu(A_\gamma) = \mu(B_\gamma \Delta N') = \mu(B_\gamma) = \mu((B_\gamma \Delta B'_\gamma) \Delta B'_\gamma) = \mu(B'_\gamma) \in \{0, 1\}.
        \]
        A questo punto si conclude come in precedenza.

        \item[$ (iii) \Rightarrow (iv) $] Osserviamo che la media temporale $ \tilde{\varphi} $ di un'osservabile $ \varphi $ è un integrale primo del moto in quanto
        \begin{align*}
            (\tilde{\varphi} \circ f)(x) & = \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j+1})(x) = \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 1}^{n} (\varphi \circ f^{j})(x) \\
            & = \lim_{n \to +\infty} \left(\frac{n+1}{n} \frac{1}{n+1} \sum_{j = 0}^{n} (\varphi \circ f^{j})(x) - \frac{\varphi(x)}{n} \right) \\
            & = 1 \cdot \tilde{\varphi}(x) - 0 = \tilde{\varphi}(x).
        \end{align*}
        e quindi per ipotesi $ \exists c \in \R : \tilde{\varphi}(x) = c $ per $\mu$-q.o $ x\in X $. Vogliamo adesso mostrare che $c = \int_{X}\varphi\dif{\mu}$. \\
        Supponiamo per ora $ \varphi \geq 0 $. Per $ f $-invarianza della misura si ha
        \begin{align*}
            \int_X \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j})(x) \dif{\mu}(x)
            & = \frac{1}{n} \sum_{j = 0}^{n-1} \int_X \varphi(f^{j}(x)) \dif{\mu}(x)
            = \frac{1}{n} \sum_{j = 0}^{n-1} \int_{f^j(X)} \varphi(y) \dif{f^j_\sharp\mu}(y) \\
            & = \frac{1}{n} \sum_{j = 0}^{n-1} \int_X \varphi(y) \dif{\mu}(y)
        \end{align*}
        dove abbiamo usato il fatto che $ X \subseteq f^{-1}(f(X)) \subseteq X $, così $ f^{-1}(f(X)) = X $ e quindi $ \mu(f(X)) = \mu(f^{-1}(f(X))) = \mu(X) $ da cui $ X $ e $ f(X) $ sono uguali a meno di insiemi di misura nulla. Concludiamo così che $ \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \in L^1 $. Possiamo quindi applicare il lemma di Fatou per ottenere
        \begin{align*}
            c & = \int_{X} \tilde{\varphi} \dif{\mu} = \int_{X} \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} = \int_{X} \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} \\
            & \leq \liminf_{n \to +\infty} \int_{X} \frac{1}{n} \sum_{j = 0}^{n-1} (\varphi \circ f^{j}) \dif{\mu} = \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \int_{X} (\varphi \circ f^{j}) \dif{\mu} \\
            & = \liminf_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \int_{X} \varphi \dif{\mu} = \int_{X} \varphi \dif{\mu}.
        \end{align*}
        dove nella prima uguaglianza si è usato il fatto che $ \tilde{\varphi} $ è quasi ovunque constante e $ \mu(X) = 1 $. \\
        Per la disuguaglianza opposta consideriamo la successione delle troncate $ \varphi_N = \min\{\varphi, N\} $ con $ N \in \N $, e definiamo:
        \[\widetilde{\varphi_N}(x) = \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\]
        Come prima si ottiene che $\widetilde{\varphi_N}$ è un integrale primo del moto, e quindi per ipotesi $ \forall N\in \N\ \exists c_N \in \R : \forall\mu\text{-q.o.}\ x\in X,\ \widetilde{\varphi_N}(x) = c_N $.
        Si ha adesso però che:
        \[\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x) \leq \frac{1}{n}\sum_{j=0}^{n-1} N = N \]
        e quindi per convergenza dominata si può concludere anche che:
        \begin{align*}
            c_N &= \int_{X}\widetilde{\varphi_N}(x)\dif{\mu} = \int_{X}\lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\dif{\mu}
            = \lim\limits_{n\to +\infty}\int_{X}\frac{1}{n}\sum_{j=0}^{n-1}(\varphi_N\circ f^j)(x)\dif{\mu}\\
            &= \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}\int_{X}(\varphi_N\circ f^j)(x)\dif{\mu}
            = \lim\limits_{n\to +\infty}\frac{1}{n}\sum_{j=0}^{n-1}\int_{X}\varphi_N\dif{\mu} = \int_{X} \varphi_N \dif{\mu}.
        \end{align*}
         Abbiamo adesso che $\varphi_N \uparrow \varphi$, da cui per convergenza monotona:
        \[c_N = \int_{X} \varphi_N \dif{\mu} \xrightarrow{N\to\infty} \int_{X} \varphi \dif{\mu}. \]
        Abbiamo infine che $\varphi_N\leq\varphi$ implica $\widetilde{\varphi_N}\leq\tilde{\varphi}$, da cui 
        \[ \int_{X}\varphi\dif{\mu} = \lim\limits_{n\to\infty}c_N = \lim\limits_{n\to\infty} \int_{X}\widetilde{\varphi_N}\dif{\mu}\leq\lim\limits_{n\to\infty} \int_{X}\tilde{\varphi}\dif{\mu} = \int_{X}\tilde{\varphi}\dif{\mu} = c \]
        che fornisce la disuguaglianza opposta tra $c$ e $\int_{X}\tilde{\varphi}\dif{\mu}$.
        

        \item[$ (iv) \Rightarrow (i) $] Basta scegliere $ \varphi = \chi_A $ con $ A \in \mathcal{A} $ e usare la formula \eqref{eqn:ergodico-mediaosservabile}.

        \item[$ (iv) \Rightarrow (v) $] Siano $ A, B \in \mathcal{A} $. Osservando che $ \chi_A \circ f^j = \chi_{f^{-j}(A)} $ abbiamo che
        \[
            \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \frac{1}{n} \sum_{j=0}^{n-1} \int_X \chi_{f^{-j}(A)} \cdot \chi_B \dif{\mu} = \int_{B} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu}.
        \]
        Ora $ \chi_A \circ f^j \leq 1 $ e $ 1 $ è integrabile essendo lo spazio di misura finita. Quindi per convergenza dominata e usando la formula \eqref{eqn:ergodico-mediaosservabile} con $ \varphi = \chi_A $ otteniamo
        \[
            \lim_{n \to +\infty} \int_{B} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu} =  \int_{B} \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \chi_A \circ f^j \dif{\mu} = \int_B \left(\int_X \chi_A \dif{\mu}\right) \dif{\mu}
        \]
        da cui segue la tesi
        \[
            \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = \mu(A) \int_B \dif{\mu} = \mu(A) \mu(B).
        \]
        \item[$ (v) \Rightarrow (ii) $] Dato $ A \in \mathcal{A} $ tale che $ f(A) \subseteq A $ e $ \mu(A) > 0 $ sia $ B \coloneqq X \setminus A $. Per la $ f $-invarianza di $ A $ si ha $ f^j(A) \subseteq A $ e quindi $ A \subseteq f^{-j}(f^j(A)) \subseteq f^{-j}(A) $; per la $ f $-invarianza della misura si ha invece
        \[
            \mu(A) = \mu(f^{-j}(A)) = \mu(f^{-j}(A) \cap A) + \mu(f^{-j}(A) \cap B) = \mu(A) + \mu(f^{-j}(A) \cap B)
        \]
        da cui $ \mu(f^{-j}(A) \cap B) = 0 $. Ma allora per ipotesi
        \[
            \mu(A)(1-\mu(A)) = \mu(A) \mu(B) = \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} \mu(f^{-j}(A) \cap B) = 0
        \]
        e quindi $ \mu(A)=1 $. \qedhere
    \end{description}
\end{proof}

\begin{proposition}\label{prop:rotazioni_erg}
    La rotazione $ R_\alpha \colon \T^1 \to \T^1 $ è ergodica se e solo se $ \alpha \in \R \setminus \Q $.
\end{proposition}
\begin{proof}
    Usiamo la caratterizzazione con gli integrali primi con $ p=2 $. Prendiamo $ \varphi \colon \T^1 \to \R $ in $ L^2 $ e lo sviluppiamo in serie di Fourier:
    \[
        \varphi(x) = \sum_{n \in \Z} \hat{\varphi}(n) e^{2\pi i n x}.
    \]
    Componendola con la rotazione
    \[
        (\varphi \circ R_\alpha)(x) = \varphi(x + \alpha) = \sum_{n \in \Z} \hat{\varphi}(n) e^{2\pi i n \alpha} e^{2\pi i n x}.
    \]
    Affinché $ \varphi $ sia un integrale primo deve valere $ \hat{\varphi}(n) \left(e^{2\pi i n \alpha} - 1 \right) = 0 $ per ogni $ n \in \Z $.
    Ora se $ \alpha \in \R \setminus \Q, \ e^{2\pi i n \alpha} \neq 1 $ per ogni $ n \neq 0 $ così $ \varphi(x) = \hat{\varphi}(0) $, cioè $ \varphi $ è costante q.o. Per mostrare l'implicazione inversa supponiamo per assurdo che $ \alpha \in \Q $ e sia della forma $ p/q $; allora $ e^{2\pi i n \alpha} - 1 = 0 $ per ogni $ n $ della forma $ kq $ con $ k \in \Z $ da cui possiamo trovare un integrale primo non costante contro l'ipotesi che $ R_\alpha $ fosse ergodico.
\end{proof}

\begin{example}[successione di Kolakoski]
    \textcolor{red}{mancante}
\end{example}

\begin{exercise}
    Mostrare che le dilatazioni sul toro sono ergodiche.
\end{exercise}
\begin{solution}
    Sia $ E_m\colon\T^1\to\T^1 $, $ E_m(x) = mx\pmod{1} $ per $ m\in\Z,\ \abs{m} \geq 2 $. Prendiamo $ \varphi\colon\T^1\to\R $ tale che $ \varphi\circ f = \varphi $; espandiamo $ \varphi $ in serie di Fourier e imponiamo che sia un integrale del moto
    \[ \varphi(x) = \sum_{n\in\Z} \hat\varphi(n) e^{2\pi i n x} = \varphi(E_m(x)) = \sum_{n\in\Z} \hat\varphi(n) e^{2\pi i n m x} \quad \forall x\in\T^1. \]
    Per l'ortogonalità della base di Fourier l'uguaglianza deve valere termine a termine; dunque si ha che $ \hat{\varphi}(n) = 0 $ per gli $ n $ non multipli di $ m $. Per tutti gli $ n $ multipli di $ m $ deve invece valere
    \[ \hat\varphi(nm) = \hat{\varphi}(n). \]
    Ora se $ n \neq 0 $ si hanno infiniti coefficienti di Fourier uguali fra loro; questi non possono essere non nulli poiché, essendo $ \varphi \in L^2 $, devono tendere a zero per $ \abs{n} \to +\infty $. Quindi l'unico coefficiente che può essere non nullo è $ \hat\varphi(0) $, ossia $ \varphi $ è una costante.
\end{solution}

\begin{exercise}
    La trasformazione $ T_\alpha \colon \T^2 \to \T^2 $ data da $ T_\alpha(x, y) \coloneqq (x+\alpha, x+y) $ è ergodica se e solo se $ \alpha \in \R \setminus \Q $.
\end{exercise}
