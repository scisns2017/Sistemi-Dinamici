\section{Lezione del 05/12/2018 [Tantari]}
\subsection{Teorema ergodico}
\begin{proof}[Dimostrazione del Teorema~\ref{thm:markov-ergodico}]
  Dato che $ P $ è irriducibile e $ I $ è finito, $ \PP(T_{i} < +\infty) = 1\ \forall i\in I $. Infatti, detto $ m $ un intero tale che $ {(P^{m})}_{ij} > 0 $
  \begin{align*}
    1 & = \PP_{j}(T_{j} < +\infty) = \PP_{j}(X_{n} = j \text{ infinite volte}) = \PP_{j}(X_{n} = j \text{ infinite volte con } n \geq m+1 ) \\
      & = \sum_{k\in I} \PP_{j}(X_{m} = k) \PP_{j}\left(x_{n} = j \text{ infinite volte con } n \geq m+1 \middle| X_{m} = k \right) \\
      &= \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(X_{n} = j \text{ infinite volte}) = \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(T_{j} < +\infty).
  \end{align*}
  Ora se fosse $ \PP_{k}(T_{j} < +\infty) < 1 $ sarebbe
  \[ 1 = \sum_{k\in I} {(P^{m})}_{jk} \PP_{k}(T_{j} < +\infty) < \sum_{k\in I} {(P^{m})}_{jk} = 1 \]
  perché $ P^{m} $ è stocastica, il che è assurdo. Dunque $ \PP_{k}(T_{j} < +\infty) = 1\ \forall k,j\in I $. È infine
  \[ \PP(T_{i} < +\infty) = \sum_{k\in I} \PP(X_{0} = k) \PP_{k}(T_{i} < +\infty) = 1. \]
  Questo risultato ci dice che, indipendentemente dal valore di $ X_{0} $, nell'evoluzione temporale lo stato $ i $ verrà visitato quasi certamente. Nel fare i limiti per $ n\to +\infty $, quindi, possiamo senza perdere di generalità supporre che sia $ X_{0} = i $, perché così facendo si stanno togliendo finiti punti. Possiamo dunque sostituire
  \[ \PP \rightarrow \PP_{i} \quad \text{ e } \quad \lambda \rightarrow \delta_{i}. \]
  Dimostriamo le due tesi:
  \begin{enumerate}[label=(\roman*)]
  \item Le variabili aleatorie $ S_{i}^{(r)} $ (definite nella~\eqref{eq:markov-escursione}) sono indipendenti e identicamente distribuite con $ E_{i}\left[S_{i}^{(r)}\right] = m_{i} $.
    Vale la stima
    \[ \sum_{k=1}^{V_{i}(n)-1} S_{i}^{(k)} \leq n \leq \sum_{k=1}^{V_{i}(n)} S_{i}^{(k)} \]
    che afferma che l'istante corrente è compreso tra il tempo necessario a passare per $ i $ $ {(V_{i}(n) - 1)} $ volte e il tempo necessario a passarvi $ V_{i}(n) $ volte. Infatti $ V_{i}(n) $ è il numero di volte in cui si passa per $ i $ prima dell'istante $ n $. Dividendo per $ V_{i}(n) $ si ha
    \[ \frac{\sum_{k=1}^{V_{i}(n)-1} S_{i}^{(k)}}{V_{i}(n)} \leq \frac{n}{V_{i}(n)} \leq \frac{\sum_{k=1}^{V_{i}(n)} S_{i}^{(k)}}{V_{i}(n)}. \]
    Ora poiché $ \PP\left( V_{i}(n) \to +\infty \right) = 1 $ (per ricorrenza), possiamo fare il limite in $ V_{i}(n) $ invece che in $ n $. Usando il Teorema~\eqref{thm:grandi-numeri} abbiamo
    \[ m_{i} \leq \lim_{n\to+\infty} \frac{n}{V_{i}(n)} \leq m_{i} \]
    da cui la tesi (tutte le stime sopra fatte valgono quasi certamente).
  \item Sia $ f\colon I\to\R $ limitata. Allora
    \begin{multline*}
      \frac{1}{n} \sum_{k=0}^{n-1} f(X_{k}) - \sum_{i\in I} \frac{f(i)}{m_{i}}  = \frac{1}{n} \sum_{k=0}^{n-1} \sum_{i\in I} f(i) \chi_{\{X_{k} = i\}} - \sum_{i\in I} \frac{f(i)}{m_{i}} \\
                                                                                = \sum_{i\in I} f(i) \left[ \frac{1}{n} \sum_{k=0}^{n-1} \chi_{\{X_{n} = i\}} - \frac{1}{m_{i}} \right] = \sum_{i\in I} \left[ \frac{V_{i}(n)}{n} - \frac{1}{m_{i}} \right] \to 0
    \end{multline*}
    quasi certamente.\qedhere
  \end{enumerate}
\end{proof}

\subsection{Inversione temporale}
Lo scopo è di definire una catena di Markov che sia il \emph{time-reversal} di una catena di Markov di finiti passi. Diamo implicitamente la definizione tramite il seguente
\begin{thm}[catena inversa]
  Sia $ P $ irriducibile e $ \pi $ un vettore di probabilità invariante per $ P $. Sia inoltre $ {(X_{n})}_{n=0}^{N} $ Markov $ (\pi, P) $. Se $ Y_n \coloneqq X_{N-n} $ allora $ {(Y_n)}_{n=0}^{N} $ è Markov $ (\pi \hat{P}) $ dove $ \hat{P} $ è data $ \forall i, j $ da\footnote{\label{fn:non-matrice-vettore}\emph{Non} è un prodotto matrice vettore.}
  \begin{equation}\label{eq:inversione-matrice}
    \pi_{j} \hat{P}_{ji} = \pi_{i} P_{ij}.
  \end{equation}
  Inoltre $ \hat{P} $ è irriducibile e $ \pi $ è invariante anche per $ \hat{P} $.
\end{thm}
\begin{proof}
  Prima di tutto $ \hat{P} $ è stocastica:
  \[
    \sum_{i\in I}\hat{P}_{ji} = \sum_{i\in I} \frac{\pi_{i} P_{ij}}{\pi_{j}} = \frac{1}{\pi_{j}} \left(\pi P\right)_{j} = \frac{\pi_{j}}{\pi_{j}} = 1.
  \]
  Per mostrare che $ (Y_n) $ è Markov mostriamo che vale l'equazione~\eqref{eq:markov-prodotto}; infatti usando la~\eqref{eq:inversione-matrice}
  \begin{align*}
    \PP(Y_0 = i_0, \ldots, Y_{n} = i_{n}) &= \PP(X_{N-n} = i_n, \ldots, X_{N} = i_{0}) \\
                                          & = \pi_{i_n} P_{i_n i_{n-1}} \cdots P_{i_1 i_0} = \hat{P}_{i_{n-1} i_n} \pi_{i_{n-1}} P_{i_{n-1} i_{n-2}} \cdots P_{i_1 i_0} \\
                                          & = \cdots = \hat{P}_{i_{n-1} i_n} \cdots \hat{P}_{i_1 i_2} \pi_{i_1} P_{i_1 i_0} = \hat{P}_{i_{n-1} i_n} \cdots \hat{P}_{i_0 i_1} \pi_{i_0} \\
                                          & = \pi_{i_0} \hat{P}_{i_0 i_1} \cdots \hat{P}_{i_{n-1} i_n}.
  \end{align*}
  Mostriamo ora che $ \pi $ è invariante:
  \[
    \left(\pi \hat{P}\right)_{i} = \sum_{j\in I} \pi_{j} \hat{P}_{ji} = \sum_{j\in I} \pi_{i} P_{ij} = \pi_{i} \sum_{j\in I} P_{ij} = \pi_{i}.
  \]
  Infine mostriamo che $ \hat{P} $ è irriducibile: essendo $ P $ irriducibile abbiamo $ \forall i, j, \ \exists m\in\N : {(P^{m})}_{ij} > 0 $, da cui $ \exists i_0 = i, i_1, \ldots, i_m=j $ tali che $ P_{i_0 i_1} \cdots P_{i_{n-1} i_n} > 0 $. Ma allora ``invertendo il cammino'' si ha
  \[
    \left(\hat{P}^{m}\right)_{ji} = \hat{P}_{i_{n} i_{n-1}} \cdots \hat{P}_{i_1 i_0} = \left(\frac{\pi_{i_{n-1}}}{\pi_{i_n}} P_{i_{n-1} i_n}\right) \cdots \left(\frac{\pi_{i_0}}{\pi_{i_1}} P_{i_0 i_1}\right) = \frac{\pi_{i_{n}}}{\pi_{i_0}} \left(P_{i_0 i_1} \cdots P_{i_{n-1} i_n}\right) > 0. \qedhere
  \]
\end{proof}

\begin{definition}[catena reversibile]
  $ {(X_n)}_{n=0}^{N} $ Markov $ (\pi, P) $ è reversibile se ammette un'inversione temporale e risulta $ P = \hat{P} $.
\end{definition}

\begin{definition}
  Una matrice stocastica $ P $ e un vettore di probabilità $ \lambda $ si dicono in bilancio dettagliato se vale\textsuperscript{\ref{fn:non-matrice-vettore}}
  \begin{equation}
    \lambda_{i} P_{ij} = \lambda_{j} P_{ji}
  \end{equation}
\end{definition}
Osserviamo che se $ \lambda $ è in bilancio dettagliato con $ P $ allora $ \lambda $ è distribuzione invariante per $ P $:
\[
  (\lambda P)_i = \sum_{j\in I}\lambda_j P_{ji} = \sum_{j\in I} \lambda_{i} P_{ij} = \lambda_{i} \sum_{j\in I} P_{ij} = \lambda_{i}.
\]
Tale proprietà suggerisce la seguente
\begin{proposition}
  Sia $ {(X_n)}_{n=0}^{N} $ Markov $ (\pi, P) $ \textcolor{red}{con $ P $ irriducibile}. Allora $ (X_n) $ è reversibile se e solo se $ \lambda $ e $ P $ sono in bilancio dettagliato.
\end{proposition}

\begin{definition}[corrente di probabilità]
  Sia $ P $ irriducibile e $ \lambda $ un vettore di probabilità. Chiamiamo corrente di probabilità $ i \to j $ \[ J(i\to j) \coloneqq \lambda_{i}P_{ij} - \lambda_{j}P_{ji}. \]
\end{definition}

\subsection{Generare numeri casuali con le catene di Markov}
\textcolor{red}{Mancante}

\subsubsection{Algoritmo di Matropolis-Hasting}
\textcolor{red}{Mancante}

\subsubsection{Algorimo ``un blocco alla volta''}
\textcolor{red}{Mancante}
