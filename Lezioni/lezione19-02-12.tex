\section{Lezione del 12/02/2019 [Marmi]}

\subsection{Forma di Poincaré-Cartan}
\emph{Setting}: Lo spazio delle fasi $ \mathcal{O} \subseteq \R^{2n} \times \R $ è un aperto semplicemente connesso. Un punto dello spazio delle fasi è $ (\vec{x}, t) = (\vec{q}, \vec{p}, t) \in \mathcal{O} $.

\begin{definition}[forma non singolare, direzioni caratteristiche]
  Una 1-forma $ \omega \colon \mathcal{O} \to (\R^{2n+1})^{*} $ data da $ \omega(\vec{x}) = \sum_{i=1}^{2n+1} \omega_i(\vec{x}) \dif{x_i} $ su $ \mathcal{O} $ si dice non singolare se $ \forall \vec{x} \in \mathcal{O} $ la matrice antisimmetrica
  \[
    A_{ij}(\vec{x}) = \pd{\omega_i}{x_j}(\vec{x}) - \pd{\omega_j}{x_i}(\vec{x})
  \]
  ha rango massimo ($ \operatorname{rank}A(\vec{x}) = 2n $). In questo caso, per ogni $ \vec{x} \in \mathcal{O} $ il nucleo della matrice $ \ker A(\vec{x}) $ è unidimensionale e definisce quindi un campo di direzioni nello spazio delle fasi, dette direzioni caratteristiche. Le curve integrali associate al campo di direzioni caratteristiche sono dette caratteristiche di $ \omega $.
\end{definition}

\begin{proposition}
  Se $ \omega $ e $ \xi $ sono forme non singolari e la loro differenza è esatta $ \omega - \xi = \dif{F} $, allora le due forme hanno le stesse caratteristiche.
\end{proposition}

\begin{definition}[forma di Poincaré-Cartan]
  Data un'hamiltoniana $ \ham \colon \mathcal{O} \to \R $ si chiama forma di Poincaré-Cartan la 1-forma su $ \mathcal{O} $
  \begin{equation}
    \omega(\vec{q}, \vec{p}, t) \coloneqq \sum_{i=1}^{n} p_i \dif{q_i} - \ham(\vec{q}, \vec{p}, t) \dif{t}.
  \end{equation}
\end{definition}

\begin{oss}
  La forma di Poincaré-Cartan è non singolare.
\end{oss}

\begin{proposition}
  Le caratteristiche della forma di Poincaré-Cartan sono le soluzioni delle equazioni di Hamilton associate ad $ \ham $.
\end{proposition}
\begin{proof}
  \textcolor{red}{Sketch} La matrice associata alla forma di Poincaré-Cartan è
  \[
    A =
    \begin{pmatrix}
      0                            & \Id_{n}                      & \nabla_{\vec{q}} \ham \\
      -\Id_{n}                     & 0                            & \nabla_{\vec{p}} \ham \\
      -(\nabla_{\vec{q}} \ham)^{T} & -(\nabla_{\vec{p}} \ham)^{T} & 0
    \end{pmatrix}
  \]
  quindi $ \vec{v} = (\nabla_{\vec{p}}\ham, -\nabla_{\vec{q}} \ham, 1) \in \ker{A} $ è tangente alle linee integrali delle equazioni di Hamilton, essendo $ \od{}{t}(\vec{q}, \vec{p}, 1) = \vec{v} $.
\end{proof}

\begin{thm}\label{thm:canonica-poincare-cartan}
  Sia $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ una trasformazione di coordinate $ \vec{x} \to \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \boldsymbol{\psi} $ la sua inversa. Siano inoltre $ \mathcal{K}(\vec{y}, t) = \ham(\boldsymbol{\psi}(\vec{y}, t), t) $ l'hamiltoniana scritta nelle nuove coordinate e $ \omega $ e $ \xi $ le forme di Poincaré-Cartan associate rispettivamente a $ \ham $ e a $ \mathcal{K} $. Allora $ \boldsymbol{\varphi} $ è una trasformazione canonica se e solo se la forma $ \omega - \xi = \dif{F} $ è esatta.
\end{thm}

\begin{example}
  Vogliamo capire per quali valori di $ \alpha, \beta, \gamma \in \R $ la seguente trasformazione
  \[
    \begin{cases}
      p = \alpha \sqrt{P} \cos{(\gamma Q)} \\
      q = \beta \sqrt{P} \sin{(\gamma Q)}
    \end{cases}
  \]
  è canonica. Osserviamo che, nelle variabili $ P $ e $ Q $ si ha
  \begin{align*}
    p\dif{q} & = \alpha \sqrt{P} \cos{(\gamma Q)} \frac{\beta}{2\sqrt{P}} \sin{(\gamma Q)} \dif{P} + \alpha \sqrt{P} \cos{(\gamma Q)} \beta \gamma \sqrt{P} \cos{(\gamma Q)} \dif{Q} \\
             & = \frac{\alpha \beta}{4} \sin{(2\gamma Q)} \dif{P} + \alpha \beta \gamma P \cos^{2}{(\gamma P)} \dif{Q} \\
             & = \frac{\alpha \beta \gamma}{2} \left(\frac{1}{2\gamma} \sin{(2\gamma Q)} \dif{P} + P \cos{(2 \gamma Q)} \dif{Q}\right) + \frac{\alpha \beta \gamma}{2} P \dif{Q} \\
             & = \frac{\alpha \beta \gamma}{2} \dif{\left(\frac{1}{2\gamma} \sin{(2\gamma Q)} P\right)} + \frac{\alpha \beta \gamma}{2} P \dif{Q}.
  \end{align*}
  Ma allora, detta $ \omega $ la forma di Poincaré-Cartan nelle variabili $ (q, p) $ e $ \xi $ quella relativa alle variabili $ (Q, P) $, si ha
  \[
    \omega - \xi = p \dif{q} - H(q, p, t) \dif{t} - P \dif{Q} + K(Q, P, t) = \dif{\left(\frac{\alpha \beta \gamma}{2} \frac{1}{2\gamma} \sin{(2\gamma Q)} P\right)} + \left(\frac{\alpha \beta \gamma}{2} - 1\right)P \dif{Q}
  \]
  essendo $ K(Q, P, t) = H(q(Q, P), p(Q, P), t) $. Grazie al teorema appena enunciato concludiamo che la trasformazione è canonica se e solo se $ \alpha \beta \gamma = 2 $.
\end{example}

\subsection{Condizioni di Lie e canonicità del flusso hamiltoniano}
Mostriamo ora come, in realtà, la canonicità di una trasformazione può essere verificata a ``tempo fissato''. A tale scopo diamo la seguente
\begin{definition}[differenziale virtuale]
  Sia $ f \colon \mathcal{O} \to \R $ una funzione di classe $ \mathcal{C}^{\infty} $. Definiamo il differenziale virtuale di $ f(\vec{x}, t) $ come il differenziale fatto solo ``rispetto alla variabile $ \vec{x} $'':
  \begin{equation}
    \vdif{f} \coloneqq \sum_{i=1}^{2n} \dpd{f}{x_i} \dif{x_i} = \dif{f} - \dpd{f}{t} \dif{t}.
  \end{equation}
\end{definition}
Definiamo inoltre la forma di Poincaré-Cartan a ``tempo fissato'' associata all'hamiltoniana $ \ham $ come
\begin{equation}
  \tilde{\omega}(\vec{q}, \vec{p}, t) \coloneqq \sum_{i=1}^{n} p_i \dif{q_i} = \omega(\vec{q}, \vec{p}, t) + \ham(\vec{q}, \vec{p}, t) \dif{t}.
\end{equation}
Possiamo allora togliere la ``dipendenza temporale'' nel teorema~\ref{thm:canonica-poincare-cartan} grazie al seguente
\begin{thm}\label{thm:condizioni-lie}
  Sia $ \boldsymbol{\varphi} \colon \mathcal{O} \to \R^{2n} $ una trasformazione di coordinate $ \vec{x} \to \vec{y} = \boldsymbol{\varphi}(\vec{x}, t) $ e $ \boldsymbol{\psi} $ la sua inversa. Siano inoltre $ \mathcal{K}(\vec{y}, t) = \ham(\boldsymbol{\psi}(\vec{y}, t), t) $ l'hamiltoniana scritta nelle nuove coordinate e $ \tilde{\omega} $ e $ \tilde{\xi} $ le forme di Poincaré-Cartan a ``tempo fissato'' associate rispettivamente a $ \ham $ e a $ \mathcal{K} $. Allora $ \boldsymbol{\varphi} $ è una trasformazione canonica se e solo se la forma $ \tilde{\omega} - \tilde{\xi} = \vdif{F} $ è esatta nel senso del differenziale virtuale.
\end{thm}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

Data un'hamiltoniana $ \ham $, consideriamo ora le equazioni di Hamilton
\[
  \begin{cases}
    \dot{q}_i = \dpd{\ham}{p_i}, & \dot{p}_i = -\dpd{\ham}{q_i} \\
    q_i(0) = Q_i, & p_i(0) = P_i \\
  \end{cases}
\]
Osserviamo che il flusso hamiltoniano è una trasformazione dello spazio delle fasi dipendente dal tempo che al tempo $ t $ porta il punto iniziale $ (\vec{Q}, \vec{P}) $ nel punto $ (\vec{q}, \vec{p}) = \Phi^{t}_\ham(\vec{Q}, \vec{P}) $. Possiamo allora vedere il flusso hamiltoniano come un cambio di coordinate $ \boldsymbol{\psi}(\vec{Q}, \vec{P}, t) = (\Phi^{t}_\ham(\vec{Q}, \vec{P}), t) $. Ciò che è sorprendente è il seguente
\begin{thm}[canonicità del flusso hamiltoniano]
  L'evoluzione temporale, ovvero la trasformazione di coordinate $ \boldsymbol{\psi} $ indotta dal flusso hamiltoniano, è una trasformazione canonica.
\end{thm}
\begin{proof}
  \textcolor{red}{Road map:} Definita l'azione hamiltoniana
  \begin{align*}
    S(\vec{Q}, \vec{P}, t) & \coloneqq \int_{0}^{t} \left\{\sum_{i=1}^{n} p_i(Q, P, \tau) \dpd{q_i}{\tau}(Q, P, \tau) - \ham_i(q(Q, P, \tau), p(Q, P, \tau), \tau)\right\} \dif{\tau} \\
                           & = \int_{\gamma(\vec{Q}, \vec{P}, t)} \omega
  \end{align*}
  dove $ \gamma(\vec{Q}, \vec{P}, t) $ è la curva che segue l'evoluzione temporale del punto $ (\vec{Q}, \vec{P}) $ dal tempo $ \tau=0 $ al tempo $ \tau=t $ data dalle equazioni di Hamilton. Si verifica che questa soddisfa le condizioni di Lie
  \[
    \tilde{\omega} - \tilde{\xi} = \vdif{S}. \qedhere
  \]
\end{proof}


\subsection{Funzioni generatrici}
Con il teorema~\ref{thm:canonica-poincare-cartan} abbiamo mostrato che la canonicità di una trasformazione dello spazio delle fasi è equivalente all'esistenza di una funzione $ F $ tale che
\[
  \omega - \xi = \sum_{i=1}^{n} \left(p_i \dif{q_i} - P_i\dif{Q_i}\right) + (\mathcal{K} - \ham) \dif{t} = \dif{F}.
\]
Fino ad ora abbiamo inteso che in questa scrittura occorre esplicitare le variabili $ Q_i, P_i $ rispetto alle variabili $ q_i, p_i $ o viceversa; in altri termini la funzione $ F $ è definita sullo spazio delle fasi della variabili ``maiuscole'' o di quelle ``minuscole''. Tuttavia tale scrittura suggerisce di poter esprimere la funzione $ F $ in termini delle variabili ``miste'' $ q_i $ e $ Q_i $: quando questo è possibile possiamo leggere la relazione precedente nel seguente modo
\[
  \begin{dcases}
    \dpd{F}{q_i} = p_i, & \dpd{F}{Q_i} = -P_i \\
    \mathcal{K} = \ham + \dpd{F}{t}
  \end{dcases}
\]
In altri termini, data un cambio di variabili, se troviamo una funzione $ F $ che soddisfa le equazioni $ p_i = \dpd{F}{q_i}, P_i = \dpd{F}{Q_i} $ allora la trasformazione è canonica e la nuova hamiltoniana è $ \mathcal{K} = \ham + \dpd{F}{t} $. $ F $ si dice \emph{funzione generatrice} della trasformazione. \\

Chiaramente, questo risultato ha come presupposto la possibilità di poter invertire parzialmente la trasformazione di coordinate: di solito una trasformazione di coordinate esprime le nuove coordinate in funzione di quelle vecchie $ (\vec{Q}, \vec{P}) = \boldsymbol{\varphi}(\vec{q}, \vec{p}, t) $ o viceversa; ora invece stiamo chiedendo che esista una funzione tale che $ (\vec{p}, \vec{P}) = \boldsymbol{\varphi'}(\vec{q}, \vec{Q}, t) $. Tuttavia non è detto che questa trasformazione sia possibile; potrebbe invece essere possibile esprimere altri set di coordinate miste in funzione delle restanti. Ad esempio potrebbe essere possibile esprimere il set $ (\vec{q}, \vec{P}) $ in funzione di $ (\vec{p}, \vec{Q}) $; questa scelta di coordnate darà origine a una diversa funzione generatrice. Possiamo passare da una funzione generatrice all'altra mediante un'opportuna \emph{trasformata di Legendre}; le quattro funzioni generatrici più frequenti sono le seguenti:
\begin{itemize}
\item $ F_1 = F_1(\vec{q}, \vec{Q}, t) $ che soddisfa
  \[
    \dpd{F_1}{q_i} = p_i \qquad \dpd{F_1}{Q_i} = -P_i;
  \]
\item $ F_2 = F_2(\vec{q}, \vec{P}, t) = F_1 + \sum_{i} Q_i P_i $ che soddisfa
  \[
    \dpd{F_2}{q_i} = p_i \qquad \dpd{F_2}{P_i} = Q_i;
  \]
\item $ F_3 = F_3(\vec{p}, \vec{Q}, t) = F_1 - \sum_{i} p_i q_i $ che soddisfa
  \[
    \dpd{F_3}{p_i} = -q_i \qquad \dpd{F_3}{Q_i} = -P_i;
  \]
\item $ F_4 = F_4(\vec{p}, \vec{P}, t) = F_1 + \sum_{i} Q_i P_i - \sum_{i} p_i q_i $ che soddisfa
  \[
    \dpd{F_4}{p_i} = -q_i \qquad \dpd{F_{4}}{P_i} = Q_i.
  \]
\end{itemize}

\begin{example}\label{ex:identita}
  La funzione $ F_{2}(\vec{q},\vec{P}) = \sum_{i} q_{i}P_{i} $ è la generatrice dell'identità, che è ovviamente una trasformazione canonica. Infatti per le formule sopra si ha
  \[ p_{i} = \dpd{F_{2}}{q_{i}} = P_{i} \qquad Q_{i} = \dpd{F_{2}}{P_{i}} = q_{i}. \]
  Osserviamo che non è ovviamente possibile esprimere la generatrice dell'identità solo rispetto alle posizioni o solo rispetto agli impulsi coniugati (casi $ F_{1} $ e $ F_{4} $).
\end{example}