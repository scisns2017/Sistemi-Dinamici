\section{Lezione del 17/10/2018 [Grotto]}
\subsection{Integrali}
\begin{definition}[Integrale di una funzione semplice]
    Sia $ (X,\mathcal{F},\mu) $ uno spazio di misura e sia $ \phi\colon X\to [0,+\infty] $ una funzione semplice $ \phi(x) \coloneqq \sum_{i=1}^N a_i\chi_{A_i}(x)$ con $ a_i\in[0,+\infty] $. Definiamo l'integrale di $ \phi $ come:
    \[ \int_X \phi(x)\dif\mu(x) \coloneqq \sum_{i=1}^{N} a_i \mu(A_i) \]
\end{definition}

\begin{definition}[Integrale di funzioni non negative]
    Sia $ f\colon X\to [0,+\infty] $ una funzione misurabile. Definiamo il suo integrale come:
    \[ \int_X f(x)\dif\mu(x) \coloneqq \sup\left\{ \int_X \phi(x)\dif\mu(x) : 0 \leq \phi(x) \leq f(x) \text{ con $\phi$ semplice} \right\} \]
    Se l'integrale di $ f $ è finito, questa si dirà \emph{integrabile} o \emph{sommabile}. \\
    Se $ A \in \mathcal{F} $ allora poniamo
    \[
        \int_A f(x)\dif\mu(x) \coloneqq \int_A f\rvert_A(x) \dif\mu\rvert_A(x).
    \]
\end{definition}

\begin{definition}[Integrale]
    Data $ f\colon X\to\R $ e dette $ f^+(x) \coloneqq \max\left\{f(x),0\right\} $ e $ f^-(x) \coloneqq \max\left\{-f(x), 0\right\} $, $ f $ si dice \emph{integrabile} o \emph{sommabile} se lo sono $ f^+ $ e $ f^- $ e in tal caso si pone:
    \[ \int_X f(x)\dif\mu(x) \coloneqq \int_X f^+(x)\dif\mu(x) - \int_X f^-(x)\dif\mu(x) \]
    Se infine $ f\colon X\to\C $ si pone:
    \[ \int_X f(x)\dif\mu(x) \coloneqq \int_X\Re[f(x)]\dif\mu(x) + i\int_X\Im[f(x)]\dif\mu(x) \]
\end{definition}

Elenchiamo alcune proprietà dell'integrale di una funzione $ f\colon X\to \R $:
\begin{enumerate}
    \item \label{it:monotonia} \emph{Monotonia}: $ 0 \leq g \leq f \Rightarrow \int\! g\dif\mu \leq \int\! f\dif\mu $;
    \item \label{it:quasi_ovunque}Se $ f=g $ quasi ovunque, allora $ \int f\dif\mu = \int g\dif\mu $;
    \item Sia $ \mu $ una misura su $ X $ e $ \bar{\mu} $ il suo completamento. Allora $ \int_X f\dif\mu = \int_X f\dif\bar{\mu} $;
    \item L'integrale è un'applicazione lineare sullo spazio delle funzioni misurabili;
    \item $ f $ è integrabile se e solo se $ \abs{f} $ è integrabile e in tal caso vale:
    \[ \abs{ \int_X f(x)\dif\mu(x)} \leq \int_X \abs{f(x)}\dif\mu(x) \]
    \item I seguenti fatti sono equivalenti:
    \begin{enumerate}[label=(\roman*)]
        \item $ \int_X \abs{f(x)}\dif\mu(x) = 0$;
        \item $ f = 0 $ quasi ovunque;
        \item $ \forall A\in\mathcal{F}, \ \int_A f(x)\dif\mu(x) = 0 $.
    \end{enumerate}
\end{enumerate}

\begin{definition}
    Chiamiamo $ \mathscr{L}^1(X,\mathcal{F},\mu) $ lo spazio delle funzioni (misurabili) integrabili.
\end{definition}

\begin{exercise}[Disuguaglianza di Markov]
    Sia $ f\in\mathscr{L}^1(X,\mathcal{F},\mu) $ con $ f\geq 0 $ e sia $ \lambda > 0 $. Allora:
    \[ \mu\left( \left\{ x\in X : f(x) \ge \lambda \right\}  \right) \leq \frac{1}{\lambda}\int_X f(x)\dif\mu(x). \]
\end{exercise}
\begin{solution}
    Basta osservare che
    \[
        \lambda \cdot \mu\left( \{ f \geq \lambda \} \right) = \int_X \chi_{ \{f\geq\lambda \} } \lambda \dif\mu = \int_{ \{ f \geq \lambda \} } \lambda \dif\mu \leq \int_{ \{ f \geq \lambda \} } f\dif\mu \leq \int_X f\dif\mu.
    \]
\end{solution}

\begin{thm}[Beppo Levi o Convergenza Monotona]
    Sia $ (f_n)_{n\in\N} $ una successione di funzioni misurabili con $ f_n \geq 0 $ e sia $ f\colon X\to[0,+\infty] $ tale che $ f_n\nearrow f $ quasi ovunque. Allora $ f $ è misurabile e vale:
    \[ \int_X f(x)\dif\mu(x) = \lim_{n\to+\infty} \int_X f_n(x)\dif\mu(x) \]
\end{thm}
\begin{proof}
    Senza perdere di generalità possiamo supporre, in virtù della proprietà \ref{it:quasi_ovunque}, che $ f_n(x)\to f(x) \ \forall x\in X $. Per ipotesi si ha definitivamente $ f_n \leq f $ e dunque, usando la \ref{it:monotonia} e passando al limite, si ottiene
    \[ \lim_{n\to+\infty}\int_X f_n\dif\mu \leq \int_X f\dif\mu. \]
    Mostriamo l'altra diseguaglianza. Sia $ \phi $ una step function tale che $ 0 \leq \phi \leq f $ e $ \int_X \phi\dif\mu < \int_X f\dif\mu $; una tale $ \phi $ è dunque limitata e inoltre vale $ \mu\left( \{\phi > 0\} \right) < +\infty $ (altrimenti esisterebbe $ \epsilon>0 $ tale che $ \forall x\in X\ \phi(x)>\epsilon $ e dunque $ \phi $ avrebbe integrale infinito). Siano ora $ E_n \coloneqq \{ f_n \geq \phi > 0 \} $ e $ E \coloneqq \{ \phi > 0 \} $. Gli $ E_n $ sono inscatolati e tendono dal basso ad $ E $, dunque si ha $ E\setminus E_n \to \emptyset $. Ora, poiché si è mostrato che $ E $ ha misura finita, vale
    \[ \lim_{n\to+\infty}\mu(E\setminus E_n) = \mu\left(\lim_{n\to +\infty}E\setminus E_n\right) = 0. \]
    Abbiamo quindi
    \[ \int_X f_n \dif\mu \geq \int_{E_n} f_n \dif\mu \geq \int_{E_n} \phi \dif\mu = \int_X \phi \dif\mu - \int_{X\setminus E_n} \phi\dif\mu \]
    Poiché $ \int_{X\setminus E_n} \phi\dif\mu = \int_{E\setminus E_n} \phi\dif\mu \leq (\sup\phi) \cdot \mu\left(E\setminus E_n\right) \to 0 $ si ha
    \[ \lim_{n\rightarrow +\infty}\int f_n\dif\mu \geq \int_X \phi\dif\mu \]
    da cui, passando al $ \sup $ sulle $ \phi \leq f $, si ottiene la diseguaglianza cercata.
\end{proof}

\begin{exercise}[Assoluta continuità]
    Dimostrare che per ogni $ f \in \mathscr{L}^1(X,\mathcal{F},\mu) $ vale:
    \[ \quad \forall\epsilon > 0, \ \exists \delta > 0 : \forall A\in\mathcal{F}, \ \mu(A) < \delta \Rightarrow \int_A \abs{f(x)} \dif\mu(x) < \epsilon \]
\end{exercise}
\begin{exercise}[Lemma di Fatou]
    Siano $ (f_n) $ con $ f_n\colon X\to[0,+\infty] $ misurabili. Allora:
    \[ \int_X \liminf_{n\to +\infty}f_n(x)\dif\mu(x) \leq \liminf_{n\to +\infty} \int_X f_n(x)\dif\mu(x) \]
\end{exercise}
\begin{exercise}[Convergenza dominata] \label{thm:convergenza-dominata}
    Siano $ f, f_n\colon X\to\R $ tali che $ f_n\to f $ quasi ovunque. Sia $ g\in\mathscr{L}^1(X) $ con:
    \[ \abs{f_n(x)} \leq g(x) \text{ quasi ovunque} \]
    Allora $ f $ è integrabile e vale:
    \[ \int_X f(x)\dif\mu(x) = \lim_{n\to +\infty}\int_X f_n(x)\dif\mu(x) \]
\end{exercise}
\begin{exercise}[Disuguaglianza di Jensen]
    Sia $ \mu $ una misura di probabilità su $ X $, $ f\in\mathscr{L}^1(X) $, $ \phi\colon \R\to\R $ convessa. Allora:
    \[ \phi\left(\int_X f(x)\dif\mu(x) \right) \leq \int_X (\phi\circ f)(x)\dif\mu(x) \]
\end{exercise}
\subsection{Spazi $ L^p $}
\begin{definition}[Spazio $ \mathscr{L}^p $]
    Sia $ (X,\mathcal{F},\mu) $ uno spazio di misura e $ p\in[0,+\infty) $. Definiamo:
    \[ \mathscr{L}^p(X,\mathcal{F},\mu) \coloneqq \left\{ f\colon X\to \R : \int_X \abs{f(x)}^p \dif\mu(x) < +\infty \right\}  \]
    Su tale spazio definiamo una \emph{semi-norma}:\footnote{Ossia una ``norma'' per cui non vale $ \norm{v} = 0 \Rightarrow v=0 $.}
    \[ \norm{f}_p \coloneqq \left( \int_X \abs{f(x)}^p\dif\mu(x) \right)^{\frac{1}{p}} \]
\end{definition}
\begin{exercise}[Disuguaglianza di Hölder]
    Siano $ p, q \in [1,+\infty) $ con $ \frac{1}{p} + \frac{1}{q} = 1 $. Allora $ \forall f,g \in \mathscr{L}^1 $ vale:
    \[ \norm{fg}_{1} \leq \norm{f}_p\norm{g}_q \]
\end{exercise}
\begin{exercise}[Disuguaglianza di Minkowski]
    Sia $ p\in[1,+\infty) $ e siano $ f,g \in\mathscr{L}^p$. Allora $ f+g \in\mathscr{L}^p $ e vale:
    \[ \norm{f+g}_p \leq \norm{f}_p + \norm{g}_p \]
\end{exercise}

Al fine di ottenere un vero spazio normato, quozientiamo $ \mathscr{L}^p $ per un'opportuna relazione di equivalenza, definita come:
\[ f \sim g \iff f = g \text{ quasi ovunque} \]
Abbiamo dunque:
\[ L^p(X,\mathcal{F},\mu) \coloneqq \faktor{\mathscr{L}^p(X,\mathcal{F},\mu)}{\sim} \]
La \emph{quasi-norma} passa al quoziente per la proprietà \eqref{it:quasi_ovunque} e diventa una vera norma. \\

Notiamo che gli elementi di $ L^p $ sono \emph{classi di equivalenza} di funzioni, ma d'ora in poi, per semplicità, diremo che sono semplicemente \emph{funzioni}, sottintendendo che si sta scegliendo un rappresentante della classe di equivalenza in questione.

\subsection{Misure prodotto e integrali multipli}
Siano $ (X,\mathcal{E},\mu) $ e $ (Y,\mathcal{F},\nu) $ spazi di misura $ \sigma $-finiti. Definiamo la $ \sigma $-algebra prodotto come:
\[
    \mathcal{E} \otimes \mathcal{F} \coloneqq \sigma(\mathcal{E}\times\mathcal{F}) = \sigma\left(\left\{ E\times F : E \in \mathcal{E}, F \in \mathcal{F} \right\}\right).
\]
Dato $ A \in \mathcal{E}\otimes\mathcal{F} $, $ \pi_x(A) $ e $ \pi_y(A) $ (sezioni di A a rispettivamente $ x $ o $ y $ fissate) sono $ \mathcal{F} $- e $ \mathcal{E} $-misurabili rispettivamente.
Esiste unica una misura $ \mu\otimes\nu $ su $ (X\times Y, \mathcal{E}\otimes\mathcal{F}) $ che estende
\[
    (\mu\otimes\nu)(E\times F) = \mu(E)\mu(F) \qquad \forall E\in\mathcal{E}\ \forall F\in\mathcal{F}
\]
\[
    (\mu\otimes\nu)(A) = \int_X \nu(\pi_x A)\dif\mu(x) = \int_Y \mu(\pi_y A)\dif\nu(y)
\]

\begin{thm}[Tonelli]\label{thm:Tonelli}
    Sia $ f\colon X\times Y \to [0,+\infty] $ misurabile su $ (X\times Y,\mathcal{E}\otimes\mathcal{F}) $. Allora:
    \[ \int_{X\times Y} f(x,y)\dif{(\mu\otimes\nu)}(x,y) = \int_X \left( \int_Y f(x,y) \dif\nu(y) \right) \dif\mu(x) = \int_Y \left( \int_X f(x,y) \dif\mu(x) \right) \dif\nu(y) \]
\end{thm}

\begin{thm}[Fubini]
    Sia $ f \in L^1(X\times Y,\mathcal{E}\otimes\mathcal{F},\mu\otimes\nu) $. Allora vale la tesi del Teorema \ref{thm:Tonelli} per funzioni a valori di segno qualsiasi.
\end{thm}

\subsection{Cambi di variabili}
Dato $ (X,\mathcal{F},\mu) $ $ \sigma $-finito, $ f\colon X\to[0,+\infty] $ misurabile, chiamiamo \emph{misura con densità} una misura del tipo:
\[ \nu(A) \coloneqq \int_A f(x)\dif\mu(x). \]
Se $ f $ è anche integrabile, allora la misura con densità è una misura finita.

\begin{example}[Gaussiana su $ \R $]
    \[ \dif\mu(x) = \frac{1}{\sqrt{2\pi\sigma^2}} \exp{\left (-\frac{x^2}{2\sigma}\right )} \dif x. \]
\end{example}

\begin{example}[Distribuzione di Cauchy]
    \[ \dif\mu(x) = \frac{1}{\pi(1+x^2)}\dif x. \]
\end{example}

\begin{definition}[Assoluta continuità delle misure]
    Date due misure $ \mu $ e $ \nu $ su $ (X, \mathcal{F}) $ diciamo che $ \nu $ è assolutamente continua rispetto a $ \mu $, e scriviamo $ \nu \ll \mu $, se $ \forall A \in \mathcal{F} $ vale $ \mu(A) = 0 \Rightarrow \nu(A) = 0$.
\end{definition}
\begin{thm}[Radon-Nicodym]
    Se $ \nu \ll \mu $, allora $ \exists f\colon X\to [0,+\infty] $ misurabile tale che $ \nu(A) = \int_A f(x)\dif\mu(x) $.
\end{thm}

\begin{thm}[Cambio di variabile per funzioni regolari]
    Sia $ V \subseteq\R^d $ aperto, $ W\subseteq\R^d $ aperto limitato e $ T\colon V \to W $ differenziabile, invertibile con inversa continua. Allora $ \forall f\in L^1(W, \textcolor{red}{\mathcal{M}, \lambda}) $
    \[ \int_W f(y)\dif y = \int_V f(T(x)) \abs{\det{\jac{T}(x)}} \dif x \]
\end{thm}
\begin{oss}
    Data $ \dif\mu(x) = \rho(x)\dif x $, si ha
    \begin{equation}\label{eq:pushforward-misure}
        \dif{(T_\sharp\mu)(y)} = \rho(T^{-1}(y)) \; \big\lvert \! \det{\jac T}(T^{-1}(y))\big\rvert^{-1} \dif y \: .
    \end{equation}
\end{oss}
%\textcolor{red}{Con $ \dif{x} $ si intende la misura di Lebesgue?} \\
%\textcolor{red}{Cose su misure invarianti.}

\begin{exercise}[Mappa logistica]
    Sia $ Q\colon [0,1] \to [0,1] $ definita come $ Q(x) = 4x(1-x) $; una misura invariante è:
    \[ \dif\mu(x) = \frac{\dif x}{\pi\sqrt{x(1-x)}} \]
\end{exercise}
\begin{exercise}
    Sia $ N\colon \R\to\R $ definita come $ N(x) = \frac{1}{2} \left( x-\frac{1}{x} \right) $; una misura invariante è:
    \[ \dif y(x) = \frac{\dif x}{\pi(1+x^2)} \]
\end{exercise}
