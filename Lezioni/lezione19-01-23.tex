\section{Lezione del 23/01/2019 [Bindini]}

\begin{center}
    Le note seguenti sono ad opera di Ugo Bindini.
\end{center}

\subsection{Mappe $C^\infty$ e differenziali}

Data una $n$-varietà $M$ e una funzione $\varphi \colon M \to \R$, vogliamo cercare di dare una definizione soddisfacente di ``gradiente'' della funzione $\varphi$. Per fare questo, dobbiamo intanto stabilire quali funzioni sono derivabili.

\begin{definition}
    Una funzione $\varphi \colon M \to \R$ è di classe $C^\infty$ nel punto $x_0 \in M$ se, scelto un sistema di coordinate locali $\pi, \sigma$ attorno ad $x_0$, si ha che $\varphi \circ \sigma \colon U \subseteq \R^n \to \R$ è di classe $C^\infty$.
\end{definition}

Osserviamo innanzitutto che la proprietà della funzione $\varphi$ di essere $C^\infty$ \emph{non} dipende dalle coordinate scelte, ed è quindi una proprietà intrinseca. Se $\pi', \sigma'$ è un altro sistema di coordinate, infatti, si ha che
\[ \varphi \circ \sigma' = \varphi \circ \sigma \circ \pi \circ \sigma' = (\varphi \circ \sigma) \circ (\pi \circ \sigma'). \]
Da un lato, $\varphi \circ \sigma \colon U \subseteq \R^n \to \R$ è di classe $C^\infty$ per definizione; dall'altro, $\pi \circ \sigma' \colon U' \subseteq \R^n \to \R^n$ è composizione di due mappe $C^\infty$.

Il problema nel definire il gradiente di una funzione definita solo sulla varietà $M$ è che non è possibile calcolare le derivate direzionali di $\varphi$ lungo un vettore di $\R^N$. Se infatti $x_0 \in M$ e $v \in \R^N$, gli incrementi lineari del tipo $x_0 + tv$ potrebbero non appartenere alla varietà non appena $t \neq 0$, e dunque l'espressione $\varphi(x_0 + tv)$ sarebbe priva di significato.

Questo ostacolo è superato dalla seguente

\begin{definition}[differenziale]
    Data $\varphi \colon M \to \R$ di classe $C^\infty$, dati $x_0 \in M$ e $v \in T_{x_0} M$, definiamo il differenziale di $\varphi$ in $x_0$ lungo $v$ come
    \begin{equation}
        \dif{\varphi}_{x_0} (v) = \left. \dod{}{t} \varphi(\gamma(t)) \right|_{t=0}
    \end{equation}
    ove $\gamma$ è una curva che genera $v$.
\end{definition}

Occorre innanzitutto dimostrare che la definizione non dipende dalla scelta della curva. Siano $\gamma_1, \gamma_2$ due curve che generano $v$. Scegliamo coordinate locali attorno al punto $x_0$ e, come di consueto, assumiamo per semplicità che le coordinate libere siano le prime $n$. Allora possiamo scrivere che $\gamma_j(t) = (q(t), f(q(t)))$ e $\varphi \circ \gamma_j = \varphi \circ \sigma \circ \pi \circ \gamma_j$. Dunque per $j = 1, 2$
\[ \dod{}{t} \varphi(\gamma(t)) = \nabla (\varphi \circ \sigma) (\pi(\gamma(t)) \cdot \dod{}{t} (\pi \circ \gamma_j)(t) = \nabla (\varphi \circ \sigma)(q_j(t)) \cdot \dot{q}_j (t).  \]

Calcolando in $t=0$ si ottiene per $j = 1, 2$
\[ \dif{\varphi}_{x_0} (v) = \nabla (\varphi \circ \sigma)(q_j(0)) \cdot \dot{q}_j(0) = \nabla (\varphi \circ \sigma)(q_0) \cdot \dot{q}_j(0); \]
tuttavia si può osservare che
\[ \dot{q_1}(0) = \pi(\dot{\gamma}_1(0)) = \pi(v) = \pi(\dot{\gamma}_2(0)) = \dot{q}_2(0), \]
e dunque la definizione non dipende dalla curva. Inoltre, abbiamo ottenuto la seguente espressione per il differenziale in coordinate locali:
\begin{equation} \label{diff}
\dif{\varphi}_{x_0} (v) = \nabla (\varphi \circ \sigma)(q_0) \cdot \pi(v).
\end{equation}

Come corollario, poiché $\pi$ è lineare, si ha immediatamente il seguente
\begin{corollary}
    Il differenziale $\dif{\varphi}_{x_0} \colon T_{x_0} M \to \R$ è un'applicazione lineare.
\end{corollary}

In altri termini, possiamo scrivere che $\dif{\varphi}_{x_0} \in (T_{x_0} M)^*$, ovvero che il differenziale in un punto della varietà è un elemento del duale dello spazio tangente in quel punto alla varietà.

\subsection{Fibrato tangente}

Sia $M \subseteq \R^N$ una $n$-varietà. Vogliamo definire un oggetto che racchiuda in sé tutte le informazioni riguardo alla varietà $M$ e ai suoi spazi tangenti in ogni punto. Questo si ottiene mediante la seguente

\begin{definition}[fibrato tangente]
    Il fibrato tangente della varietà $M$ è
    \[ TM \coloneqq \left\{(x,v) \in \R^N \times \R^N : x \in M, v \in T_x M\right\}. \]
\end{definition}

Ciò che rende lo studio del fibrato tangente possibile e interessante è il fatto che esso stesso risulti essere una varietà.

\begin{thm}
    Sia $M \subseteq \R^N$ una $n$-varietà. Allora $TM \subseteq \R^N \times \R^N$ è una $2n$-varietà.
\end{thm}

\begin{proof}
    Dobbiamo innanzitutto trovare $2(N-n)$ equazioni che siano i vincoli per $TM$. Imponiamo le seguenti condizioni su $(x,v) \in \R^N \times \R^N$:
    \begin{equation} \label{sistemone} \left\{ \begin{array}{ll} F_j(x) = 0 \quad & j = n+1, \dotsc, N \\ \nabla F_j(x) \cdot v = 0 \quad & j = n+1, \dotsc, N. \end{array} \right. \end{equation}

    Sfruttando la caratterizzazione geometrica dello spazio tangente $T_x M$, è immediato verificare che $(x,v) \in TM$ se e solo se $(x,v)$ risolve il sistema \eqref{sistemone}. Dobbiamo però controllare la condizione di lineare indipendenza dei gradienti. La matrice dei gradienti nel punto $(x,v)$ ha la seguente forma:

    \[ \begin{pmatrix}
    \nabla F_{N}(x) & \\ \vdots & 0 \\ \nabla F_{n+1}(x) & \\ & \nabla F_{n+1}(x) \\ * & \vdots \\ & \nabla F_{N}(x)
    \end{pmatrix}. \]

    Quando $x \in M$, scegliendo opportunamente $N-n$ colonne fra le prime $N$ e le corrispondenti $N-n$ colonne fra le altre $N$, si ottiene un minore invertibile, poiché il determinante può essere calcolato ``a blocchi''.
\end{proof}
