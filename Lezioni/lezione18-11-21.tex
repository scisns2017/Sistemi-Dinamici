\section{Lezione del 21/11/2018 [Marmi]}

\begin{thm}[di ricorrenza di Poincaré]
    Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Allora $ \forall A \in \mathcal{A} $, per $ \mu $-q.o. $ x \in A $, $ x $ è ricorrente in $ A $.
\end{thm}
\begin{proof}
    Sia $ A_r \coloneqq \{x \in A : x \text{ è ricorrente in } A\} \subseteq A $. Riscriviamo $ A_r $ in un modo più comodo. Definiamo $ B_n \coloneqq \{x \in A : \forall m \geq n, \ f^{m}(x) \notin A\} $ come l'insieme degli $ x \in A $ che non visitano più $ A $ dopo $ n $ passi; possiamo scrivere tale insieme come
    \[
        B_n = A \setminus \bigcup_{j \geq n} f^{-j}(A).
    \]
    Così $ \bigcup_{n\geq1} B_n = \{x \in A : \exists n \geq 1 : \forall m \geq n, \ f^{m}(x) \notin A\} $ è l'insieme degli $ x \in A $ che non tornano mai in $ A $ ovvero degli $ x \in A $ che tornano in $ A $ entro un tempo finito $ n $, ma che dopo tale tempo non tornano più. Pertanto l'insieme dei punti ricorrenti è
    \[
        A_r = A \setminus \bigcup_{n \geq 1} B_n
    \]
    Essendo $ A_r $ differenza e unione numerabile di insiemi misurabili, anche $ A_r $ misurabile. Osservando che $ A \subseteq \bigcup_{j \geq 0} f^{-j}(A) $ essendo $ f^{0}(A) = A $, otteniamo
    \[
        B_n \subseteq \bigcup_{j \geq 0} f^{-j}(A) \setminus  \bigcup_{j \geq n} f^{-j}(A).
    \]
    Definendo allora $ \overline{A} \coloneqq \bigcup_{j \geq 0} f^{-j}(A) $ abbiamo che
    \[
        \mu(B_n) \leq \mu(\overline{A}) - \mu\left(\textstyle{\bigcup_{j \geq n}} f^{-j}(A)\right) = \mu(\overline{A}) - \mu\left(f^{-n}\left(\textstyle{\bigcup_{j \geq 0}} f^{-j}(A)\right)\right) = \mu(\overline{A}) - \mu(f^{-n}(\overline{A})).
    \]
    Ma $ \mu $ è $ f $-invariante quindi
    \[
        \mu(B_n) \leq \mu(\overline{A}) -\mu(f^{-n}(\overline{A})) = \mu(\overline{A}) - \mu(\overline{A}) = 0.
    \]
    da cui $ \mu\left(\bigcup_{n \geq 1} B_n\right) \leq \sum_{n \geq 1} \mu(B_n) = 0 $. Così concludiamo che
    \[
        \mu(A_r) = \mu(A) - \mu\left(\textstyle{\bigcup_{n \geq 1}} B_n\right) = \mu(A)
     \]
    cioè $ \mu $-q.o. $ x \in A $ è ricorrente in $ A $.
\end{proof}

\begin{thm}[Birkhoff] \label{thm:Birkoff}
    Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Allora $ \forall A \in \mathcal{A} $, per $ \mu $-q.o. $ x \in X $, $ \overline{\nu}(x, A) = \underline{\nu}(x, A) $ cioè
    \[
        \exists \, \lim_{n \to +\infty} \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j}(x)) \eqqcolon \nu(x, A).
    \]
    Inoltre $ \forall \varphi \in L^1(X, \mathcal{A}, \mu; \R) $ e per $ \mu $-q.o. $ x \in X $
    \[
        \exists \, \lim_{n \to +\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi \circ f^j)(x) \eqqcolon \tilde{\varphi}(x).
    \]
\end{thm}
\begin{proof}
    Trattiamo solo la parte con le funzioni caratteristiche. Sappiamo che esistono
    \[
        \underline{\nu}(x, A) \coloneqq \liminf_{n \to +\infty} \nu(x, A, n) \qquad \overline{\nu}(x, A) \coloneqq \limsup_{n \to +\infty} \nu(x, A, n)
    \]
    dove $ \nu(x, A, n) \coloneqq \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j}(x)) = \frac{1}{n} T(x, A, n) $. Sicuramente vale $ \underline{\nu}(x, A) \leq \overline{\nu}(x, A) $ quindi è sufficiente mostrare la disuguaglianza opposta. \\
    Fissato $ \epsilon > 0 $ sia
    \[
        \overline{\tau}(x, A, \epsilon) \coloneqq \min{\{n \in \N : \nu(x, A, n) \geq \overline{\nu}(x, A) - \epsilon\}}.
    \]
    Supponiamo per ora che tale quantità sia limitata uniformemente in $ x $, cioè $ \exists M_\epsilon > 0 : \forall x \in X, \ \overline{\tau}(x, A, \epsilon) \leq M_\epsilon $. L'idea è di dividere l'orbita da 0 a $ n-1 $ (abbiamo in mente $ n \gg M_\epsilon $ in quanto manderemo poi $ n \to +\infty $) in sotto-segmenti di lunghezza $ \overline{\tau} $ su cui abbiamo un controllo essendo $ \overline{\tau} $ limitato. Dato $ n > M_\epsilon $ consideriamo quindi il segmento di orbita $ (f^{j}(x))_{j = 0}^{n-1} $ di lunghezza $ n $. Definiamo la successione ricorsiva
    \[
        \begin{cases}
        x_0 = x & \tau_0 = \overline{\tau}(x_0, A, \epsilon) \\
        x_{k+1} = f^{\overline{\tau}(x_k, A, \epsilon)}(x_k) = f^{\tau_k}(x_0) & \tau_k = \sum_{j = 0}^{k} \overline{\tau}(x_j, A, \epsilon)
        \end{cases}
    \]
    definita da $ k = 0 $ fino a $ k = K $ tale che $ \tau_{K} < n - 1 $ ma $ \tau_{K+1} \geq n - 1 $. \\
    Ora per definizione $ \nu(x, A, \overline{\tau}(x, A , \epsilon)) \geq \overline{\nu}(x, A) - \epsilon $, mentre fissato $ N \in \N $ si ha
    \[
        \frac{1}{n} \sum_{j = 0}^{n-1} \chi_A(f^{j+N}(x)) = \frac{1}{n} \sum_{j = N}^{n+N-1} \chi_A(f^{j}(x)) = \frac{n+N}{n} \frac{1}{n+N} \sum_{j = 0}^{n+N-1} \chi_A(f^{j}(x)) - \frac{1}{n} \sum_{j = 0}^{N-1} \chi_A(f^{j}(x))
    \]
    da cui passando al $ \limsup $ abbiamo $ \overline{\nu}(f^N(x), A) = \overline{\nu}(x, A) $. Quindi per ogni $ k \in \{0, \ldots, K\} $ il numero di visite in ogni sotto-segmento si stima come
    \begin{align*}
        T(x_k, A, \overline{\tau}(x_k, A, \epsilon)) & = \overline{\tau}(x_k, A, \epsilon) \cdot \nu(x_k, A, \overline{\tau}(x, A , \epsilon)) \\
        & \geq \overline{\tau}(x_k, A, \epsilon) \cdot (\overline{\nu}(x_k, A) - \epsilon) \\
        & = \overline{\tau}(x_k, A, \epsilon) \cdot (\overline{\nu}(x, A) - \epsilon).
    \end{align*}
    Il numero totale di visite lungo il segmento di orbita di lunghezza $ n $ si stima considerando i sotto-segmenti su cui abbiamo un controllo come
    \begin{align*}
        T(x, A, n) & = \left[\sum_{k = 0}^{K} T(x_k, A, \overline{\tau}(x_k, A, \epsilon))\right] + T(x_{K}, A, n - \tau_{K}) \\
        & \geq \left[\sum_{k = 0}^{K}  \overline{\tau}(x_k, A, \epsilon)\right] (\overline{\nu}(x, A) - \epsilon) = \tau_{K} \cdot (\overline{\nu}(x, A) - \epsilon) \\
        & \geq (n - 1 - M_\epsilon)(\overline{\nu}(x, A) - \epsilon)
    \end{align*}
    dove abbiamo maggiorato il termine $ T(x_{K}, A, n - \tau_{K}) $ con $ 0 $ in quanto è il pezzo di orbita su cui non abbiamo controllo e abbiamo osservato che per costruzione la somma dei tempi $ \tau_K \geq n - M_{\epsilon} $. Ora per $ f $-invarianza della misura
    \[
        \int_{X} T(x, A, n) \dif{\mu}(x) = \sum_{j = 0}^{n-1} \int_{X} \chi_A(f^{j}(x)) \dif{\mu}(x) = \sum_{j = 0}^{n-1} \int_{X} \chi_A(x) \dif{\mu}(x) = n \cdot \mu(A)
    \]
    così per monotonia
    \begin{gather*}
        n \cdot \mu(A) = \int_{X} T(x, A, n) \dif\mu(x) \geq (n - 1 - M_\epsilon) \left(\int_{X}(\overline{\nu}(x, A) - \epsilon) \dif{\mu}(x)\right) \\
        \mu(A) \geq \frac{n - 1 - M_\epsilon}{n} \left(\int_{X}\overline{\nu}(x, A) \dif{\mu}(x) - \epsilon\right)
    \end{gather*}
    Passando prima al limite in $ n $ e poi all'estremo superiore su $ \epsilon $ otteniamo infine
    \[
        \mu(A) \geq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
    \]
    Ripetendo lo stesso ragionamento con $ \underline{\nu} $, cioè supponendo che la quantità
    \[
        \underline{\tau}(x, A, \epsilon) \coloneqq \min{\{n \in \N : \nu(x, A, n) \leq \overline{\nu}(x, A) + \epsilon\}}
    \]
    sia uniformemente limitata e dividendo il segmento di orbita da $ 0 $ a $ (n-1) $ in sotto-segmenti, giungiamo alla disuguaglianza opposta
    \[
        \mu(A) \leq \int_{X}\underline{\nu}(x, A) \dif{\mu}(x).
    \]
    Combinando le due disuguaglianze abbiamo quindi
    \[
        \int_{X}\underline{\nu}(x, A) \dif{\mu}(x) \geq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
    \]
    Ma d'altra parte essendo $ \underline{\nu}(x, A) \leq \overline{\nu}(x, A) $ per ogni $ x \in X $ anche
    \[
        \int_{X}\underline{\nu}(x, A) \dif{\mu}(x) \leq \int_{X}\overline{\nu}(x, A) \dif{\mu}(x).
    \]
    Così
    \[
        \int_{X} \overline{\nu} \dif{\mu} = \int_{X} \underline{\nu} \dif{\mu} \quad \Rightarrow \quad \int_{X} \left(\overline{\nu} - \underline{\nu}\right) \dif{\mu} = 0.
    \]
    Ma dato che l'integrando è positivo concludiamo che $ \mu $-q.o. si ha $ \overline{\nu}(x, A) = \underline{\nu}(x, A) $. \\
    \textcolor{red}{Dobbiamo ora rimuovere l'ipotesi della limitatezza.}
\end{proof}

\subsection{Unica ergodicità}
\begin{proposition}
    Sia $ (X, \mathcal{A}, \mu_1, f) $ un sistema ergodico e $ \mu_2 $ una misura di probabilità su $ (X, \mathcal{A}) $ $ f $-invariante. Allora i seguenti fatti sono equivalenti:
    \begin{enumerate}[label=(\roman*)]
        \item $ \mu_1 \neq \mu_2 $;
        \item $ \mu_2 $ non è assolutamente continua rispetto a $ \mu_1 $, cioè $ \exists A \in \mathcal{A} $ tale che $ \mu_1(A) = 0 $ ma $ \mu_2(A) > 0 $;
        \item $ \exists A \in \mathcal{A} $ $ f $-invariante tale che $ \mu_1(A) = 0 $ e $ \mu_2(A) > 0 $.
    \end{enumerate}
\end{proposition}

Tale risultato stabilisce che un sistema dinamico misurabile può essere ergodico rispetto a due misure ``che non si parlano''. Ci sono tuttavia dei sistemi dinamici che ammettono una sola misura invariante. In tale caso si dà la seguente
\begin{definition}[sistema unicamente ergodico]
    $ (X, \mathcal{A}, \mu, f) $ si dice unicamente ergodico se esiste un'unica misura di probabilità su $ (X, \mathcal{A}) $ che sia $ f $-invariante.
\end{definition}

Tale definizione ha senso perché un sistema unicamente ergodico è anche ergodico. Se infatti per assurdo $ (X, \mathcal{A}, \mu, f) $ non fosse ergodico esisterebbe $ A \in \mathcal{A} $ $ f $-invariante tale che $ \mu(A) > 0 $ e $ \mu(X \setminus A) > 0 $. Possiamo allora definire le misure
\[
     \nu_1(E) \coloneqq \frac{\mu(A \cap E)}{\mu(A)}
     \qquad
     \nu_2(E) \coloneqq \frac{\mu((X \setminus A) \cap E)}{\mu(X \setminus A)}
\]
che sono misure di probabilità diverse (basta prendere $ E = A $) e $ f $-invarianti contro l'ipotesi. Per l'invarianza basta osservare che per l'invarianza di $ \mu $ si ha
\[
    \nu_1(f^{-1}(E)) = \frac{\mu(A \cap f^{-1}(E))}{\mu(A)} = \frac{\mu(f^{-1}(A) \cap f^{-1}(E))}{\mu(A)} = \frac{\mu(f^{-1}(A \cap E))}{\mu(A)} = \frac{\mu(A \cap E)}{\mu(A)} = \nu_1(E).
\]
La seconda uguaglianza è giustificata dal fatto che $ A \cap B $ e $ f^{-1}(A) \cap B $ sono uguali a meno di insiemi di misura nulla per ogni $ B \in \mathcal{A} $. Infatti per $ f $-invarianza di $ A $ si ha $ A \subseteq f^{-1}(A) $ e quindi $ A \Delta f^{-1}(A) = f^{-1}(A) \setminus A $, così per $ f $-invarianza della misura $ \mu(A \Delta f^{-1}(A)) = \mu(f^{-1}(A)) - \mu(A) = \mu(A) - \mu(A) = 0 $; ma allora $ \mu\left((A \cap B) \Delta (f^{-1}(A) \cap B)\right) = \mu\left((A \Delta f^{-1}(A)) \cap B\right) = 0 $. \\
Per la $ f $-invarianza di $ \nu_2 $ basta osservare che essa può essere riscritta in funzione delle sole $ \mu $ e $ \nu_1 $ che sono invarianti:
\[
    \nu_2(E) = \frac{\mu((X \setminus A) \cap E)}{\mu(X \setminus A)}
    = \frac{\mu(E \setminus (A \cap E))}{\mu(X\setminus A)}
    = \frac{\mu(E) - \mu(A \cap E)}{1 - \mu(A)}
    = \frac{\mu(E) - \nu_1(E) \mu(A)}{1 - \mu(A)}.
\]


Se oltre alla struttura misurabile, abbiamo anche una struttura topologica, allora l'unica ergodicità permette di ottenere una convergenza più forte delle medie temporali degli osservabili, rispetto a quella puntuale quasi ovunque garantita dall'ergodicità.

\begin{thm} \label{thm:unic-ergodico-convergenza-uniforme}
    Se $ (X, \mathcal{B}, \mu, f) $, con $ X $ spazio topologico e $ \mathcal{B} $ la $ \sigma $-algebra dei boreliani, è un sistema unicamente ergodico e $ \varphi \colon X \to \R $ è continua, allora si ha convergenza uniforme della media temporale dell'osservabile:
    \[
        \frac{1}{n} \sum_{j=0}^{n-1} (\varphi \circ f^j)(x) \, \touf \, \int_X \varphi \dif{\mu}.
    \]
\end{thm}

Osserviamo che tale enunciato richiede in particolare che le osservabili siano continue e non permette quindi di rimuovere il $ \mu $-q.o. nella definizione di sistema ergodico, essendo le funzioni caratteristiche non continue. Possiamo tuttavia cavarcela per approssimazione dall'alto e dal basso: dato $ A \in \mathcal{B} $, siano $ (\varphi_k)_{k\in\N} $ e $ (\psi_k)_{k\in \N} $ due successioni di funzioni continue tali che $ \varphi_k \uparrow \chi_A $ e $ \psi_k \downarrow \chi_A $ puntualmente. Grazie al Teorema \ref{thm:unic-ergodico-convergenza-uniforme} sappiamo che $ \forall x \in X $ (e uniformemente in $ x $) vale da un lato
\begin{align*}
     \int_X \varphi_k \dif{\mu} = \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi_k \circ f^j)(x) &= \liminf_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\varphi_k \circ f^j)(x) \\ &\leq \liminf_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) = \underline{\nu}(x, A)
\end{align*}
e dall'altro
\begin{align*}
    \int_X \psi_k \dif{\mu} = \lim_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\psi_k \circ f^j)(x) &= \limsup_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\psi_k \circ f^j)(x) \\ &\geq \limsup_{n\to+\infty} \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) = \overline{\nu}(x, A).
\end{align*}
Ora per convergenza monotona sulle $ (\varphi_k) $
\[
    \mu(A) = \int_X \chi_A \dif{\mu} = \lim_{k\to+\infty} \int_X \varphi_k \dif{\mu} \leq \underline{\nu}(x, A)
\]
e similmente sulle $ (\psi_k) $
\[
    \mu(A) = \int_X \chi_A \dif{\mu} = \lim_{k\to+\infty} \int_X \psi_k \dif{\mu} \geq \overline{\nu}(x, A).
\]
Così infine si ha che $ \forall x \in X $, $ \mu(A) \leq \underline{\nu}(x, A) \leq \overline{\nu}(x, A) \leq \mu(A) $ ovvero $ \nu(x, A) $ esiste ovunque e vale
\[
    \nu(x, A, n) = \frac{1}{n} \sum_{j=0}^{n-1} (\chi_A \circ f^j)(x) \, \touf \, \mu(A).
\]

\begin{exercise}\label{ex:potenze_di_due_cancro}
    Sia $ x_j = \text{cifra più significativa di } 2^j $. Calcolare la frequenza di ciascuna cifra nella successione $ (x_j)_{j\in\N} $. \\
    \textcolor{red}{\emph{Hint}: le rotazioni irrazionali sul toro sono unicamente ergodiche e l'unica misura invariante è Lebesgue. Sarebbe bello mettere la dimostrazione di questa cosa: c'è sulle dispense di Grotto ma scompone la misura in Fourier.}
\end{exercise}
\begin{solution}
    Chiamiamo $ c_n $ la cifra più significativa di $ 2^n $, cioè il numero in $ \{1, \cdots, 9\} $ tale che
    \[ c_n \cdot 10^s \leq 2^n < (c_n+1) \cdot 10^s \]
    dove $ s = \lfloor \log_{10} 2^n \rfloor $. Prendendo ora il logaritmo si ha $ \log_{10}c_n + s \leq n\log_{10}2 < \log_{10}(c_n+1) + s $ e quindi
    \[ \log_{10}c_n \leq \{n\log_{10}2\} < \log_{10}(c_n+1) \]
    Considerando ora le rotazioni $ R_{\log_{10}(2)} \colon \T^1\to\T^1 $ possiamo riscrivere $ \{ n\log_{10}2 \} = R^n_{\log_{10}(2)}(0) $;
    se suddividiamo il toro come $ \T^1 = \bigsqcup_{k=1}^9 I_k $ con $ I_k = \left[\log_{10}k,\log_{10}(k+1)\right) $ la condizione che la cifra più significativa di $ 2^n $ sia $ c_n $ si traduce in
    \[ R^n_{\log_{10}(2)}(0) \in I_c \, . \]
    La sequenza delle potenze di 2 cercata è dunque la dinamica simbolica dell'orbita di 0 tramite la rotazione di $ \log_{10}2 $ secondo la suddetta partizione.
    
    Il sistema dinamico $ (\T^1, \mathcal{M}, \lambda, R_{\log_{10}(2)}) $ è unicamente ergodico in quanto $ \log_{10}2 $ è irrazionale; dunque le frequenze di visita convergono puntualmente e quindi la frequenza di visita dell'orbita di 0 agli intervalli $ I_k $ è uguale alla misura di Lebesgue degli intervalli stessi:
    \[ \nu(0,I_c) = \log_{10}\left(1+\frac{1}{c}\right) \, . \]
\end{solution}
\subsection{Sistemi di funzioni iterate}
\textcolor{red}{Introduzione}

\begin{example}[triangolo di Sierpiński] \label{ex:sierpinski}
    Consideriamo le seguenti mappe $ w_i \colon \R^2 \to \R^2 $
    \[
        w_1 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} \quad
        w_2 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} + \begin{pmatrix} 1/4 \\ \sqrt{3}/4 \end{pmatrix} \quad
        w_1 \begin{pmatrix} x \\ y \end{pmatrix} = \begin{pmatrix} x/2 \\ y/2 \end{pmatrix} + \begin{pmatrix} 1/2 \\ 0 \end{pmatrix}
    \]
    e il triangolo equilatero di lato unitario $ T \coloneqq \{(x, y) \in \R^2 : x \geq 0, \ y \leq \sqrt{3}x , \ y \leq -\sqrt{3}(x-1)\} $. Con riferimento alla Figure \ref{fig:sierpinski}, l'unione delle 3 immagini di $ T $ tramite le $ w_1 $, $ w_2 $ e $ w_3 $ è la forma più scura in figura. \textcolor{red}{Su ogni triangolo itero questo passo e ottengo il triangolo di Sierpiński}
\end{example}

\begin{example}[insieme di Cantor]
    \textcolor{red}{Mancante}
\end{example}

\begin{definition}[dimensione di box-counting]
    Sia $ F \subseteq \R^n $ \textcolor{red}{compatto} e, fissato $ \delta > 0 $ reale, $ N_\delta(F) $ il minimo numero di palle di raggio $ \delta $ necessarie per coprire $ F $\footnote{Esiste finito per compattezza.}. Definiamo allora
    \begin{itemize}
        \item dimensione di box-counting \emph{inferiore} \[ \underline{\dim} F \coloneqq \liminf_{\delta \to 0} \frac{\log N_\delta(F)}{-\log{\delta}}; \]
        \item dimensione di box-counting \emph{superiore} \[ \overline{\dim} F \coloneqq \limsup_{\delta \to 0} \frac{\log N_\delta(F)}{-\log{\delta}}. \]
    \end{itemize}
    Se la dimensione inferiore e superiore coincidono, definiamo la dimensione di box-counting di $ F $ come $ \dim F \coloneqq \underline{\dim} F = \overline{\dim} F $.
\end{definition}

\begin{definition}[distanza di Hausdorff]
    Sia $ (X, d) $ uno spazio metrico e $ A, B \subseteq X $. Definiamo la distanza di Hausdorff tra $ A $ e $ B $ come
    \[
        h(A, B) \coloneqq \max\{d(A, B), d(B, A)\}
    \]
    dove
    \[
        d(A, B) \coloneqq \sup_{x \in A} d(x, B) = \sup_{x \in A} \inf_{y \in B} d(x, y)
    \]
    e similmente per $ d(B, A) $.
\end{definition}

La distanza di Hausdorff è un modo per quantificare quanto sono distanti due sottoinsiemi di uno spazio metrico, secondo la metrica data. La definizione appena data, tuttavia, non è ben posta per sottoinsiemi generici di $ X $, in quanto non è nemmeno garantito che tale $ h $ sia finita. Se però $ A $ e $ B $ sono compatti non vuoti, la funzione $ y \mapsto d(x, y) $ è continua su un compatto e quindi $ \inf_{y \in B} d(x, y) = \min_{y \in B} d(x, y) $; similmente la funzione $ x \mapsto d(x, B) $ è continua su un compatto, così $ \sup_{x \in A} d(x, B) = \max_{x \in A} d(x, B) $. Discorso analogo vale per $ d(B, A) $. Se ci restringiamo a $ \mathcal{K}(X) $, lo spazio dei compatti di $ X $ non vuoti, la distanza di Hausdorff risulta quindi finita. In realtà, la distanza di Hausdorff è veramente una distanza su $ \mathcal{K}(X) $ come enunciato nel seguente
\begin{thm}
    Sia $ (X, d) $ uno spazio metrico, $ \mathcal{K}(X) = \{K \subseteq X : K \neq \emptyset, \ K \text{ compatto}\} $ e $ h $ la distanza di Hausdorff. Allora
    \begin{enumerate}[label=(\roman*)]
        \item lo spazio $ (\mathcal{K}(X), h) $ è uno spazio metrico;
        \item se $ (X, d) $ è completo, anche $ (\mathcal{K}(X), h) $ è completo;
        \item se $ (X, d) $ è compatto, anche $ (\mathcal{K}(X), h) $ è compatto.
    \end{enumerate}
\end{thm}

Sia ora $ (X, d) $ uno spazio metrico compatto. Consideriamo le funzioni $ v_1, \ldots, v_n \colon X \to X $ e le rispettive funzioni $ w_1, \ldots, w_n \colon \mathcal{K}(X) \to \mathcal{K}(X) $ date da $ w_i(K) \coloneqq \{v_i(x) : x \in K\} $ che agiscono sui sottoinsiemi compatti di $ X $. Essendo le immagini delle $ w_i $ degli insiemi è ben definita la mappa
\begin{align*}
    w \colon \mathcal{K}(X) & \to \mathcal{K}(X) \\
    K & \mapsto \bigcup_{i=1}^{n} w_i(K)
\end{align*}
Se le $ w_i $ sono continue secondo la topologia indotta dalla distanza di Hausdorff, si può verificare che anche $ w $ è continua e pertanto la terna $ (\mathcal{K}(X), h, w) $ è un sistema dinamico topologico.

\begin{proposition}
    Supponiamo che le $ w_i $ siano contrazioni su $ (\mathcal{K}(X), h) $ con costante di Lipschitz $ s_i \in [0, 1) $. Allora anche $ w $ è una contrazione su $ (\mathcal{K}(X), h) $ con costante di Lipschitz $ s \coloneqq \max s_i $.
\end{proposition}

Tale proposizione risulta particolarmente utile nel caso in cui $ (X, d) $ sia anche uno spazio metrico completo (oltre che compatto). Infatti in tale caso anche $ (\mathcal{K}(X), h) $ è completo e quindi il teorema di Banach-Caccioppoli o delle contrazioni assicura l'esistenza di un unico punto fisso per $ w $, ovvero di un unico insieme $ K_w \in \mathcal{K}(X) $ tale $ w(K_w) = K_w $ detto \emph{attrattore globale} del sistema. Come noto della dimostrazione del teorema delle contrazioni, il punto fisso si trova come punto limite delle iterazioni di $ w $ a partire da un qualunque elemento di $ \mathcal{K}(X) $: pertanto tutte le orbite nello spazio delle fasi tramite $ w $ convergeranno allo stesso $ K_w $ secondo la distanza di Hausdorff.

Il triangolo di Sierpiński è un esempio di attrattore globale per l'unione delle mappe definire nell'Esempio \ref{ex:sierpinski}. Infatti non è necessario partire da triangolo per ottenere il triangolo di Sierpiński, come si può vedere nella Figura \ref{fig:sierpinski-acaso}.

\textcolor{red}{Figura mancante}
