\section{Lezione del 16/10/2018 [Marmi]}
\begin{definition}[orbita pre-periodica e periodica] Sia $f\colon X \to X$ un sistema dinamico. Un'orbita $\mathcal{O}^f(x)$ si dice pre-periodica se contiene un numero finito di elementi. Se inoltre $ f $ è invertibile l'orbita si dice periodica e la sua cardinalità si dice periodo. \\
Infine, se $f$ non è invertibile, possono esistere punti $ x $ (che costituiscono il pre-periodo) tali che $ \forall n > 0\ f^n (x) \neq x $.
\end{definition}

\begin{example}[Congettura di Collatz]
Si consideri il sistema dinamico $f \colon \N \to \N$:
\[
    f(n) \coloneqq
    \begin{cases}
        n/2  & \text{se $ n $ è pari}    \\
        3n+1 & \text{se $ n $ è dispari}
    \end{cases}
\]
La congettura\footnote{È attualmente un problema aperto.} di Collatz asserisce che tutti gli $n \in \N$ sono preperiodici e che l'unico ciclo è $ 1 \to 4 \to 2 \to 1 $.
\end{example}

\subsection{Coniugazione e misure invarianti}
\begin{definition}[sistemi dinamici coniugati]
Siano $f\colon X \to X$ e $g\colon Y \to Y$ due sistemi dinamici. Questi si dicono coniugati se esiste $h \colon X \to Y$ invertibile tale che $h \circ f = g \circ h$, cioè tale da far commutare il seguente diagramma:
\begin{equation}\label{cd:coniugazione}
    \centering
    \begin{tikzcd}
        X \arrow[r, "f"] \arrow[d, "h"]  & X \arrow[d, "h"] \\
        Y \arrow[r, "g"] & Y
    \end{tikzcd}
\end{equation}
Se $h$ è solamente surgettiva si dice che $g$ è un \emph{fattore} di $f$ oppure che $f$ è un'\emph{estensione} di $g$. Se invece $h$ è solo iniettiva allora si dice che $f$ è un \emph{sottosistema} di $g$.
\end{definition}

\begin{example} \label{ex:Ulam_Mandelbrot}
Si considerino i seguenti sistemi dinamici $ \C \to \C$:
\[ Q_\lambda(z) \coloneqq \lambda z (1-z) \]
\[ P_c(z) \coloneqq z^2 + c \qquad \text{con } c = - \frac{\lambda^2}{4} +  \frac{\lambda}{2}. \]
Le funzioni $ Q_\lambda $ sono dette \emph{trasformazioni di Ulam-Von Neumann}, mentre $P_c$ è la funzione che genera l'\emph{insieme di Mandelbrot}.
I due sistemi risultano coniugati attraverso la funzione
\[ h_\lambda(z) = -\lambda z + \frac{\lambda}{2}\;. \]
Infatti si verifica che $ h\circ Q_\lambda = P_c \circ h $.
\end{example}

\begin{example} \label{ex:Ulam_tenda}
    Sia $ Q_4 \colon [0,1] \to [0,1] $ come definita nell'esempio \ref{ex:Ulam_Mandelbrot} e sia $ T\colon [0,1] \to [0,1] $ la mappa a tenda:
    \[
        T(x) \coloneqq
        \begin{cases}
            2x   & \text{se } 0 \leq x \leq 1/2 \\
            2-2x & \text{se } 1/2 \leq x \leq 1
        \end{cases}.
    \]
    Allora i due sistemi sono coniugati tramite $ h(x) = \sin^2\left(\frac{\pi x}{2}\right) $, cioè si ha $ Q_4\circ h = h\circ T $.
    Inoltre, poiché $ T $ conserva la misura di Lebesgue, usando la \eqref{eq:pushforward-misure} si ottiene che $ Q_4 $ conserva la misura:
    \[ \dif{h_\sharp \lambda}(x) = \frac{\dif x}{\pi\sqrt{x(1-x)}}\; . \]

    \iffigureon
    \begin{figure}
        \begin{center}
            \subfloat[Mappa a tenda]
            { \input{img/ulam-tenda/left.tikz} }
            \subfloat[Trasformazione $ Q_4 $]
            { \input{img/ulam-tenda/right.tikz} }
        \end{center}
        \caption{funzione dell'Esempio \ref{ex:Ulam_tenda}.}
    \end{figure}
    \fi
\end{example}

\begin{example}
    Sia $ Q_4\colon (0,1)\to(0,1) $, $ S\colon \R \to \R $ definita come
    \[ S(y) \coloneqq \log\left(\frac{4 e^y}{(1-e^y)^2}\right) \]
    e $ h\colon (0,1)\to\R $:
    \[ h(x) \coloneqq \operatorname{logit}(x) \coloneqq \log\left(\frac{x}{1-x}\right) \]
    Allora $ h\circ Q_4 = S \circ h $ e S conserva la misura:
    \[ \dif\mu(y) = \frac{\dif y}{\pi\left( e^{y/2} - e^{-y/2} \right) } \; . \]
\end{example}

\subsection{Dinamica topologica}

Vogliamo ora studiare i sistemi dinamici dati dall'azione di una mappa $ f $ su di uno spazio topologico $ (X, \tau) $, cioè di un insieme $ X $ dotato di una topologia $ \tau $. La mappa deve essere un'applicazione dallo spazio in sé che conservi la struttura dello spazio. Per uno spazio topologico, l'insieme degli endomorfismi di $ X $ coincide con l'insieme delle funzioni continue da $ X $ in sé, mentre l'insieme degli automorfismo coincide con l'insieme delle funzioni continue con inversa continua cioè con l'insieme degli \emph{omeomorfismi} di $ X $.

Nonostante i risultati che andremo ad enunciare siano spesso validi nel contesto generale degli spazi topologici, per rendere la trattazione più semplice ci restringeremo alla dinamica topologica fatta su \emph{spazi metrici compatti}: indicheremo con la coppia $ (X, d) $ lo spazio metrico e la topologia $ \tau_d $ su $ X $ sarà quella indotta dalla distanza.

Nella trattazione seguente ci concentreremo su sistemi dinamici a tempo discreto, anche se molti dei risultati che vedremo sono validi per sistemi dinamici a tempo continuo.

\begin{definition}[sistema dinamico topologico]
    Un sistema dinamico topologico è una terna $ (X, d, f) $ dove
    \begin{enumerate}[label=(\roman*)]
        \item $ (X, d) $ è uno spazio metrico compatto;
        \item $ f \colon X \to X $ è una funzione continua oppure un omeomorfismo;
        \item la dinamica è data dall'iterazione di $ f $.
    \end{enumerate}
\end{definition}

\begin{definition}[sottoinsieme invariante]
    Un sottoinsieme $ A \subset X $ si dice $ f $-invariante se $ f(A) \subseteq A $ o equivalentemente, se $ \forall x \in A $ e $ \forall n \in \N $, $ f^n(x) \in A $. Se $ f $ è invertibile può anche essere che $ \forall x \in A $ e $ \forall n \in \Z $, $ f^n(x) \in A $: in tale caso si può distinguere tra insieme  \emph{positivamente} $ f $-invariante se tale proprietà vale per $ n \geq 0 $ o \emph{negativamente} $ f $-invariante se vale per $ n \leq 0 $.
\end{definition}

\begin{oss}
    Le orbite sono insiemi invarianti, così come lo sono unioni di orbite.
\end{oss}

\begin{definition}[punto errante]
    Un punto $ x \in X $ si dice errante se esiste $ U $ intorno di $ x $ tale che $ \left( \bigcup_{n \in \Z\setminus\{0\}} f^n(U) \right) \cap U = \emptyset $.
    Se $ f $ è solo un endomorfismo prenderemo solo gli $ n > 0 $. Nel caso di un sistema dinamico a tempo continuo, invece, si applica la stessa definizione dopo aver ``discretizzato'' il tempo ($ t = \tau\Z $).
\end{definition}
\begin{example}
    Sia $ X = \R $ e $ T_\alpha (x) \coloneqq x + \alpha $ la traslazione di $ \alpha $. In questo caso tutti gli $ x\in\R $ sono punti erranti.
\end{example}

\begin{proposition}
    Sia $ \Omega = \left\{ x\in X : x \text{ non è errante} \right\} $. Mostrare che $ \Omega $ è chiuso e $ f $-invariante e che $ X $ compatto $ \Rightarrow \Omega \neq \emptyset $.
\end{proposition}
\begin{proof}
    Supponiamo \emph{wlog} che $ f $ si un omeomorfismo. Sia $ \bar x \in \clo{\Omega} $. Allora $ \forall U $ intorno aperto di $ \bar{x} $ si ha che $ \exists y\in\Omega\cap U $. Ma $ U $ è intorno anche di $ y\in \Omega $, per cui $ \exists n\neq 0 : f^n(U)\cap U \neq \emptyset $ e quindi $ \bar x \in \Omega $, da cui $ \Omega $ è chiuso.

    Per dimostrare che $ \Omega $ è invariante basta far vedere che $ f(\Omega) \subseteq \Omega $. Considero dunque $ x\in f(\Omega) $, $ U\ni x $ intorno e $ V \coloneqq f^{-1}(U) $; allora $ \exists \bar x\in\Omega : x = f(\bar x) $. Poiché $ \bar x\in\Omega $ e $ V $ è intorno di $ \bar x $, $ \exists n\neq 0 : f^n(V) \cap V \neq \emptyset $. Ora $ f^n(U) \cap U = f^n(f(V)) \cap f(V) = f(f^n(V)) \cap f(V) \supseteq f(f^n(V)\cap V) \neq \emptyset $ e quindi $ x\in\Omega $.

    Supponiamo ora $ X $ compatto. Dato $ x \in X $ consideriamo la sua orbita $ \mathcal{O}_f(x) = \{f^{n}(x)\}_{n\in\Z} $. Per compattezza allora esiste $ y \in X $ e una successione $ (n_k)_{k\in\N} \subseteq \Z $ tale che la sottosuccessione $ f^{n_k}(x) \to y $. Sia ora $ U $ un intorno di $ y $; dalla convergenza sappiamo che esiste $ k \in \N $ tale che $ f^{n_{k}}(x) \in U $ e $ f^{n_{k+1}} \in U $. Posto allora $ z = f^{n_k}(x) $ e $ m = n_{k+1} - n_k $ si ha $ z \in U $ e $ f^{m}(z) \in U $ da cui $ f^m(U) \cap U \neq \emptyset $. Quindi $ z \in \Omega $ che è la tesi.

    Si può dare anche una dimostrazione puramente topologica passando per i ricoprimenti a cui accenniamo. Supponendo per assurdo che $ \Omega $ sia vuoto, abbiamo che $ \forall x \in X $ esiste $ U $ introno \emph{wlog} aperto di $ x $ tale che $ \forall n \neq 0, \ f^n(U)\cap U = 0 $. L'unione $ \bigcup_{x\in X}U_x $ è un ricoprimento aperto di $ X $, dal quale per compattezza si può estrarre un sottoricoprimento finito $ \{U_i\}_{i=1,\ldots,N} $. L'idea ora è che questi intorni sotto iterazione della mappa non possono intersecare l'intorno originario: dopo la prima iterazione ognuno degli $ U_i $ almeno deve finire dentro ad un intorno $ U_j $ con $ j \neq i $; alla seconda iterazione esso deve finire dentro un introno $ U_k $ con $ k $ diverso sia da $ i $ che da $ j $, essendo $ f(U_i) $ interno a $ U_j $; etc. Ma essendo il ricoprimento finito dopo un numero finito di iterazioni, $ f^{m}(U_i) $ no potrà più andare in nessuno degli intorni del ricoprimento da cui l'assurdo. Formalizzare quest'ultimo passaggio è tuttavia non banale.
\end{proof}

\begin{definition}[$ \alpha $ e $ \omega $-limite]
    Dato $ x\in X $ si definiscono gli insiemi:
    \begin{align*}
        \alpha(x) \coloneqq \left\{ y\in X : \exists\; n_j \searrow -\infty, f^{n_j}(x) \to y \right\} \\
        \omega(x) \coloneqq \left\{ y\in X : \exists\; n_j \nearrow +\infty, f^{n_j}(x) \to y \right\}
    \end{align*}
\end{definition}

\begin{definition}[punto ricorrente]
    Un punto $ x\in X $ si dice ricorrente se $ x\in \alpha(x) \cap \omega(x) $. Se vale solo $ x\in \alpha $ (risp. $ \omega $) il punto si dirà negativamente (risp. positivamente) ricorrente.
\end{definition}
\begin{exercise}
    Dato $ x\in X $, $ \alpha(x) $ e $ \omega(x) $ sono chiusi e invarianti.
\end{exercise}

\begin{example}
    Si consideri il sistema dinamico definito su $\R^2$ che in coordinate polari segue la legge
    \[
        \begin{cases}
            \dot{\theta}  = 1      \\
            \dot{r}       = (1-r)r
        \end{cases}
    \]
    Gli insiemi limite sono
    \begin{align*}
        \alpha(x) &=
        \begin{cases}
            \{ (0,0) \}                                & \norm{x} < 1 \\
            \left \{x \in \R^2: \norm{x} = 1 \right \} & \norm{x} = 1 \\
            \emptyset                                  & \norm{x} > 1
        \end{cases} \\
        \omega(x) &=
        \begin{cases}
            \{ (0,0) \} & x = (0,0) \\
            \left \{x \in \R^2: \norm{x} = 1 \right \} & x \neq (0,0)
        \end{cases}
    \end{align*}
\end{example}
%\begin{example} % Non è davvero utile e ci si mette una vita a farla in tikz
%    \textcolor{red}{Schifezza a forma di 8, è indispensabile la figura.}
%\end{example}
\begin{example}[rotazioni sul toro]
    Prendiamo come spazio delle fasi $ X = \T^1 $. Consideriamo il sistema dinamico dato dall'iterazione della rotazione $ R_\alpha\colon X\to X $ definita come
    \[ R_\alpha(x) \coloneqq x + \alpha \pmod{1}. \]
    Dotiamo inoltre $ X $ della distanza
    \[ d(x,y) \coloneqq \min_{p\in\Z} \abs{x-y-p}. \]
    Mostriamo innanzi tutto che le orbite sono periodiche $ \iff \alpha\in\Q $:
    \begin{itemize}
        \item[$\Rightarrow$] Si ha $ \forall x\in\T^1\; \exists m\in\Z : x = x + m\alpha \pmod{1} $. Ma allora $ m\alpha = k $ per qualche $ k\in Z $ e quindi dev'essere $ \alpha\in\Q $.
        \item[$\Leftarrow$] Sia $ \alpha = p/q $. $ R_\alpha^m(x) = x + m\frac{p}{q} \pmod{1} $, quindi dopo $ q $ passi $ x $ torna in se stesso.
    \end{itemize}
    Mostriamo ora che se $ \alpha\in\R\setminus\Q $ $ \iff $ tutte le orbite sono dense nel toro. Innanzi tutto se $ \alpha \in \Q $ tutte le orbite hanno cardinalità finita e quindi non sono dense in $ \T^1 $.
    Siano invece $ \alpha \in \R\setminus\Q $, $ x\in\T^1 $, $ \epsilon > 0 $ e $ N \in \N $ tale che $ \frac{1}{N} < \epsilon $. Scriviamo il toro come $ \T^1 = \bigcup_{i=0}^{N-1} \left[ \frac{i}{N}, \frac{i+1}{N} \right) $. Poiché $ \mathcal{O}(x) $ non è periodica, dopo al più $ N $ iterazioni ci saranno due punti $ R^{n_1}_{\alpha}(x) $ e $ R^{n_2}_{\alpha}(x) $ appartenenti allo stesso intervallo e quindi che distano meno di $ \epsilon $. Osserviamo che
    \[ d(R_\alpha^{n_1}(x), R_\alpha^{n_2}(x)) = \norm{(n_1\alpha + x) - (n_2\alpha + x) }_\Z = \norm{[(n_1-n_2)\alpha + x]-x}_\Z = d(R_\alpha^{n_1-n_2}(x), x) < \epsilon, \]
    quindi fissato un $\epsilon$ esiste un intero $N_\epsilon$ tale che dopo $N_\epsilon$ iterazioni disto sicuramente meno di $\epsilon$ dal punto di partenza. Consideriamo ora l'insieme 
    \[A_\epsilon = \left\{ R_\alpha^{k \cdot N_\epsilon}(x)\, |\, k \in \N \right\},\]
    che di fatto è un orbita di una rotazione di un numero minore di $\epsilon$. Questo vuol dire che dato $y \in \T^1$ si ha che $d(y, A_\epsilon) < \epsilon/2$, ma essndo $A_\epsilon$ una sotto-orbita di $\mathcal{O}(x)$ si ha che $d(y, \mathcal{O}(x)) < \epsilon$, che per l'arbitrarietà di $\epsilon$ è la tesi.
\end{example}

\begin{exercise}
    Dimostrare che per ogni successione finita di cifre decimali esiste una potenza di 2 tale che la sua rappresentazione decimale inizi con tale successione.
\end{exercise}
\begin{solution}
    Sia $ N\in\N $ il numero corrispondente alla data sequenza di cifre decimali e sia $ n\in\N $ tale che $ 2^n \geq N $. Allora la differenza $ d $ tra il numero di cifre di $ 2^n $ e il numero di cifre di $ N $ (espressi in base 10) è
    \[ d = \lfloor \log_{10}2^n\rfloor - \lfloor \log_{10}N \rfloor \]
    La condizione da imporre è dunque che esista un $ n\in\N $ tale che
    \[ 0 \leq \frac{2^n}{10^{d}} - N < 1 \, . \]
    Manipolando questa relazione, otteniamo
    \[ 0 \leq \{ n\log_{10}2 \} - \{ \log_{10}N \} < \log_{10}\left( 1 + \frac{1}{N} \right) \]
    Come nell'Esercizio \ref{ex:potenze_di_due_cancro}, abbiamo $ \{ n\log_{10}2 \} = R^n_{\log_{10}(2)}(0) $. L'esistenza di un $ n $ che soddisfa la precedente relazione segue  dalla densità delle orbite delle rotazioni irrazionali nel toro, avendo preso $ \epsilon = \log_{10}\left( 1 + 1/N \right) $.
\end{solution}

\begin{exercise}[teorema di Dirichlet]
    Sia $ \alpha \in \R\setminus\Q $. Allora l'equazione
    \[ \abs{\alpha - \frac{p}{q}} < \frac{1}{q^2} \]
    ha infinite soluzioni $ p/q \in\Q $ distinte.
\end{exercise}

\begin{definition}[numeri diofantei]
    Dato $ \gamma > 0 $ e $ \tau \geq 0 $, definiamo l'insieme
    \[ \mathrm{CD(\gamma,\tau)} \coloneqq \left\{ \alpha\in\R\setminus\Q : \abs{\alpha - \frac{p}{q}} \geq \frac{\gamma}{q^{2+\tau}}\quad \forall p/q\in\Q \right\} \]
    Definiamo poi
    \[ \mathrm{CD(\tau)} \coloneqq \bigcup_{\gamma > 0} \mathrm{CD}(\gamma,\tau) \]
    e infine l'insieme dei numeri diofantei
    \[ \mathrm{CD} \coloneqq \bigcup_{\tau \geq 0} \mathrm{CD}(\tau) \]
\end{definition}

\begin{definition}[numeri di Liouville]
    Diciamo che $ x $ è di Liouville se $ x\in (\R\setminus\Q) \setminus \mathrm{CD} $.
\end{definition}

\begin{exercise}
    $ \sum_{n=0}^{+\infty} 10^{-n!} $ è di Liouville.
\end{exercise}
\begin{exercise}
    Gli irrazionali algebrici sono diofantei.
\end{exercise}
\begin{exercise}
    Quasi ogni reale è diofanteo.\\
    Hint: stimare la misura di Lebesgue di $ ( (0,1) \setminus \mathrm{CD}(\gamma,\tau) ),\; \tau > 0 $.
\end{exercise}
