\section{Lezione del 27/11/2018 [Marmi]}

\subsection{Mescolamento di un sistema dinamico misurabile}
\emph{Setting}: $ (X, \mathcal{A}, \mu, f) $ sistema dinamico misurabile. \\

Intuitivamente in un sistema è ``mescolante'' se un insieme viene spalmato su tutto lo spazio delle fasi sotto l'iterazione di $ f $. Se $ A $ e $ B $ sono insiemi misurabili, consideriamo l'evoluzione di $ A $ sotto $ f $: essendo lo spazio di probabilità, $ \mu(f^{-n}(A) \cap B) $ è la ``percentuale'' di $ f^{-n}(A) $ contenuta in $ B $; se allora $ \mu $ è $ f $-invariante, $ \mu(A) $ viene preservato dall'iterazione di $ f $ e quindi ci aspettiamo che $ \mu(f^{-n}(A) \cap B) $ tenda a $ \mu(A) \mu(B) $. Più formalmente diamo la seguente

\begin{definition}[mescolamento forte]
    Un sistema dinamico misurabile si dice (fortemente) mescolante se $ \forall A, B \in \mathcal{A} $
    \[
        \lim_{n \to +\infty} \mu\left(f^{-n}(A) \cap B\right) = \mu(A) \mu(B).
    \]
\end{definition}
Osserviamo che, per il Teorema di Cesaro sulle medie, il mescolamento forte implica il mescolamento in media \eqref{eq:mescolamento_in_media}.

\begin{definition}[operatore di Koopman]
    Definiamo l'operatore di Koopman $ \mathcal{U}_f $ associato a $ (X, \mathcal{A}, \mu, f) $ come la mappa che a $ \varphi \in L^2(X, \mathcal{A}, \mu; \R) $ associa la funzione $ \mathcal{U}_f \varphi \coloneqq \varphi \circ f $.
\end{definition}

\begin{exercise}
    Mostrare che $ \mathcal{U}_f $ è un'isometria di $ L^2(X, \mathcal{A}, \mu; \R) $.
\end{exercise}
\begin{solution}
    Basta mostrare che per ogni $ \varphi, \psi \in L^2(X, \mathcal{A}, \mu, \R) $ si ha $ \langle{\mathcal{U}_f \varphi, \mathcal{U}_f \psi}\rangle = \langle{\varphi, \psi}\rangle $ dove $ \langle{\, , }\rangle $ è il prodotto scalare standard di $ L^2 $. Infatti usando l'$ f $-invarianza della misura
   \begin{align*}
       \int_{X} \mathcal{U}_f \varphi \; \mathcal{U}_f \psi \dif{\mu} & =  \int_{X} \varphi(f(x)) \psi(f(x)) \dif{\mu}(x) \\
       & = \int_{f(X)} \varphi(y) \psi(y) \dif{\mu}(f^{-1}(y)) = \int_{f(X)} \varphi(y) \psi(y) \dif{\mu}(y) \\
       & = \int_{X} \varphi(y) \psi(y) \dif{\mu}(y).
   \end{align*}
   Nell'ultimo passaggio abbiamo usato il fatto che $ X $ e $ f(X) $ sono uguali a meno di insiemi di misura nulla, e pertanto gli integrali coincidono.
\end{solution}

\begin{proposition} \label{prop:correlazione-mescolamento}
    Un sistema dinamico misurabile $ (X, \mathcal{A}, \mu, f) $ è fortemente mescolante se e solo se $ \forall \varphi, \psi \in L^2(X, \mathcal{A}, \mu; \R) $ l'$ n $-esimo coefficiente di correlazione
    \[
        c_n(\varphi, \psi) \coloneqq \langle\mathcal{U}_f^n \varphi, \psi\rangle = \int_X \mathcal{U}_f^n \varphi \; \psi \dif{\mu}
    \]
    tende al prodotto dei valori di aspettazione $ c_n(\varphi, \psi) \to \langle\varphi, 1\rangle \langle 1, \psi\rangle $\footnote{L'ordine nel prodotto scalare risulta rilevante nel caso in cui si considerino funzioni a valori complessi.}.
\end{proposition}
\begin{proof}
    Se i coefficienti di correlazione tendono al prodotto dei valori di aspettazione, allora chiaramente il sistema è mescolante (basta scegliere $ \varphi $ e $ \psi $ funzioni indicatrici). \\
    Viceversa supponiamo che $ (X, \mathcal{A}, \mu, f) $ sia mescolante. Pertanto la tesi vale per le funzioni indicatrici, e per linearità di $ \mathcal{U}_f $ e del prodotto scalare vale anche per le step-functions. Ma le funzioni semplici sono dense in $ L^2 $. Quindi fissato $ \epsilon > 0 $ esistono $ \alpha, \beta \in L^2(X, \mathcal{A}, \mu; \R) $ step-functions tali che $ \norm{\varphi - \alpha} < \epsilon $ e $ \norm{\psi - \beta} < \epsilon $ dove $ \norm{ \cdot } $ è la norma di $ L^2 $. Sia inoltre $ n_\epsilon $ tale che $ \forall n \geq n_\epsilon $ valga
    \[
        \abs{\langle\mathcal{U}_f^n \alpha, \beta\rangle - \langle\alpha, 1\rangle \langle 1, \beta\rangle} < \epsilon.
    \]
    Così per ogni $ n $ vale
    \begin{align*}
        \abs{\langle\mathcal{U}_f^n \varphi, \psi\rangle - \langle\mathcal{U}_f^n \alpha, \beta\rangle}
        & = \abs{\langle\mathcal{U}_f^n \varphi, \psi\rangle - \langle\mathcal{U}_f^n \varphi, \beta\rangle + \langle\mathcal{U}_f^n \varphi, \beta\rangle - \langle\mathcal{U}_f^n \alpha, \beta\rangle} \\
        & \leq \abs{\langle\mathcal{U}_f^n \varphi, \psi - \beta\rangle} + \abs{\langle\mathcal{U}_f^n \varphi - \mathcal{U}_f^n\alpha, \beta\rangle} \\
        & \leq \norm{\mathcal{U}_f^n \varphi} \ \norm{\psi - \beta} + \norm{\mathcal{U}_f^n(\varphi - \alpha)} \norm{\beta} \\
        & \leq \norm{\varphi} \epsilon + \norm{\beta} \epsilon
    \end{align*}
    dove nell'ultimo passaggio abbiamo usato il fatto che $ \mathcal{U}_f $ è un'isometria. Inoltre
    \begin{align*}
        \abs{\langle\alpha, 1\rangle \langle 1, \beta\rangle - \langle\varphi, 1\rangle \langle 1, \psi\rangle}
        & \leq  \abs{\langle\alpha, 1\rangle \langle 1, \beta\rangle - \langle\alpha, 1\rangle \langle 1, \psi\rangle} +  \abs{\langle\alpha, 1\rangle \langle 1, \psi\rangle - \langle\varphi, 1\rangle \langle 1, \psi\rangle} \\
        & \leq \norm{\alpha} \norm{\beta - \psi} + \norm{\psi}\norm{\alpha - \psi} \\
        & \leq \norm{\alpha} \epsilon + \norm{\psi} \epsilon.
    \end{align*}
    Ma allora per $ n \geq n_\epsilon $
    \begin{align*}
        \abs{\langle\mathcal{U}_f^n \varphi, \psi\rangle - \langle\varphi, 1\rangle \langle 1, \psi\rangle}
        & = \abs{\langle\mathcal{U}_f^n \varphi, \psi\rangle - \langle\mathcal{U}_f^n \alpha, \beta\rangle} + \abs{\langle\mathcal{U}_f^n \alpha, \beta\rangle - \langle\alpha, 1\rangle \langle 1, \beta\rangle} \\ & \qquad + \abs{\langle\alpha, 1\rangle \langle 1, \beta\rangle - \langle\varphi, 1\rangle \langle 1, \psi\rangle} \\
        & \leq \left(\norm{\varphi} + \norm{\beta}\right) \epsilon + \epsilon  + \left(\norm{\alpha} + \norm{\psi}\right) \epsilon.
    \end{align*}
    Quindi per arbitrarietà di $ \epsilon $ otteniamo la tesi.
\end{proof}

\begin{proposition}
    Sia $ f_A \colon \T^n \mapsto \T^n $ un automorfismo lineare iperbolico del toro. Il sistema dinamico $ (\T^n, \mathcal{M}, \lambda, f_A) $, dove $ \mathcal{M} $ è l'insieme dei Lebesgue-misurabili e $ \lambda $ è la misura di Lebesgue, è un sistema mescolante.
\end{proposition}
\begin{proof}
    Si lascia come esercizio di dimostrare che il sistema dinamico in questione è un sistema dinamico misurabile. Sia $ \{e_{\vec k}(x) \coloneqq e^{2\pi i \vec k \cdot \vec x}\}_{\vec k \in \Z^n} $ la base di Fourier di $ L^2(\T^n) $ dove con $ \vec k \cdot \vec x = k_1 x_1 + \cdots + k_n x_n $ indichiamo il prodotto scalare standard in $ \R^n $. Mostriamo prima di tutto che vale la tesi della Proposizione \ref{prop:correlazione-mescolamento} per le funzioni della base di Fourier. \\
    Siano quindi $ \vec k, \vec h \in \Z^n $. Se $ \vec k = \vec 0 $ allora chiaramente $ \langle\mathcal{U}_{f_A}^n e_{\vec 0}, e_{\vec h}\rangle = \langle1, e_{\vec h}\rangle \to \langle e_{\vec 0}, 1\rangle \langle1, e_{\vec h}\rangle $. Sia quindi $ \vec k \neq \vec 0 $. Affermo che $ \mathcal{U}_{f_A}^n e_{\vec k} = e_{(A^T)^n \vec k} $ dove $ A $ è la matrice associata all'applicazione lineare $ f_A $: infatti
    \[
        (\mathcal{U}_{f_A}^n e_{\vec k})(x) = e_{\vec k}(f_A^{n}(x)) = e^{2\pi i \vec k \cdot (A^n \vec x)} = e^{2\pi i \left[(A^T)^n \vec k\right] \cdot \vec x}.
    \]
    Ora $ (A^T)^n \vec k = \vec h $ per al più un $ n \in \N $, altrimenti se esistessero $ n $ e $ m $ che realizzano tale uguaglianza si avrebbe
    \[
        \left((A^T)^{n-m} - \Id\right) \vec k = 0
    \]
    con $ k \neq 0 $. Ma essendo l'automorfismo iperbolico, $ A $ ha spettro disgiunto dal cerchio unitario, ovvero per ogni autovalore $ \lambda\in\C $ vale $ \abs{\lambda} \neq 1 $; osserviamo che $ A $ e $ A^T $ hanno lo stesso spettro, e che se $ \lambda $ è autovalore di $ A $ allora $ \lambda^{n-m} $ è autovalore per $ A^{n-m} $, dunque anche $ (A^T)^{n-m} $ avrà spettro disgiunto dal cerchio unitario. Quindi l'unica possibilità è che sia $ \vec k = \vec 0 $ contro l'ipotesi. Allora, essendo la base ortogonale, si ha $ \langle\mathcal{U}_{f_A}^n e_{\vec k}, e_{\vec h}\rangle = 0 $ definitivamente in $ n $, che è il limite voluto, dato che per $ \vec k \neq \vec 0 $, $ e_{\vec k} $ è ortogonale alla funzione $ 1 = e_{\vec 0} $. \\
    Dunque per gli elementi della base di Fourier vale
    \[
        \langle\mathcal{U}_{f_A}^n e_{\vec k}, e_{\vec h}\rangle \to \langle e_{\vec k}, 1\rangle \langle 1, e_{\vec h}\rangle.
    \]
    Se ora $ \varphi, \psi \in L^2(\T^n) $ possiamo scomporle in serie di Fourier $ \varphi(x) = \sum_{\vec k\in\Z^n} \hat{\varphi}(\vec k) e_{\vec k} $ e $ \psi(x) = \sum_{\vec h\in\Z^n} \hat{\psi}(\vec k) e_{\vec k} $. Così, per linearità dell'operatore di Koopman e del prodotto scalare, otteniamo la tesi
    \[
        \langle\mathcal{U}_f^n \varphi, \psi\rangle = \sum_{\vec h, \vec k \in \Z^n} \hat{\varphi}(\vec k) \hat{\psi}(\vec h) \langle\mathcal{U}_{f_A}^n e_{\vec k}, e_{\vec h}\rangle \to \sum_{\vec h, \vec k \in \Z^n} \hat{\varphi}(\vec k) \hat{\psi}(\vec h) \langle e_{\vec k}, 1\rangle \langle 1, e_{\vec h}\rangle = \langle\varphi, 1\rangle \langle 1, \psi\rangle.
    \]
    Dunque per la Proposizione \ref{prop:correlazione-mescolamento} il sistema è mescolante.
\end{proof}

\subsection{Schemi di Bernoulli}
Gli \emph{schemi di Bernoulli} sono l'equivalente misurabile dello \emph{shift} su $ m $ simboli. Sia quindi $ \Sigma_m \coloneqq \{0, \ldots, m-1\}^{\N} $ l'insieme delle sequenze numerabili di $ m $ simboli e $ \sigma \colon \Sigma_m \to \Sigma_m $ lo \emph{shift} sinistro che a $ x = (x_i)_{i \in \N} \in \Sigma_m $ associa la sequenza $ \sigma(x) $ tale che $ (\sigma(x))_i = x_{i+1} $ per ogni $ i \in \N $. \\
Vogliamo ora costruire su $ \Sigma_m $ una struttura di spazio di probabilità con una misura che sia invariante per lo \emph{shift}. Definiamo i \emph{cilindri} come gli insiemi che fissano le prime $ k $ cifre di una sequenza
\begin{equation}
    \begin{pmatrix}
        0 & \cdots & k-1 \\
        j_0 & \cdots & j_{k-1}
    \end{pmatrix}
    \coloneqq
    \left\{ (x_i)_{i\in\N} \in \Sigma_m : x_0 = j_0, \ldots, x_{k-1} = j_{k-1} \right\}.
\end{equation}
Per esempio, se $ k=1 $ il cilindro $ \binom{0}{m-1} $ è l'insieme di tutte le sequenze che iniziano con la cifra $ m-1 $. Tale cilindro coincide con la palla di centro $ x = (m-1, 0, 0, \ldots) $ e raggio $ m^{-1} $ secondo la distanza definita nella sezione \ref{subsec:shift-top}, e in generale un cilindro coincide con una palla di raggio opportuno centrata nella sequenza $ x = (j_0, \ldots, j_{k-1}, 0, 0, \ldots) $. È possibile dare una definizione più generale di cilindro, utile in altri contesti, come l'insieme delle successioni con cifre coincidenti di $ k $ posizioni generiche; tuttavia in questo caso si perde l'identificazione tra cilindri e palle. \\
Fissato un vettore di probabilità di lunghezza $ m $, ovvero $ p \in \{(p_0, \ldots, p_{m-1}) \in \R^{m}_{+} : \sum_{i=0}^{m-1} p_i = 1\} $, definiamo la misura di un cilindro come
\begin{equation}
    \mu_p{
        \begin{pmatrix}
        0 & \cdots & k-1 \\
        j_0 & \cdots & j_{k-1}
        \end{pmatrix}
    }
    \coloneqq p_{j_0} \cdots p_{j_{k-1}}.
\end{equation}
In altre parole abbiamo assegnato una probabilità ad ogni simbolo e stiamo affermando che la probabilità di ottenere una sequenza contenente i simboli $ j_0 $ in posizione $ 0 $, $ \ldots $, $ j_{k-1} $ in posizione $ k-1 $ è il prodotto delle probabilità dei simboli fissati. \\
\textcolor{red}{Per motivi da giustificare} la misura $ \mu_p $ si può estendere a una misura di probabilità definita sulla $ \sigma $-algebra generata dai cilindri, che indicheremo con $ \mathcal{C} $. Con lieve abuso di notazione continueremo a indicare con $ \mu_p $ la misura di probabilità definita su $ \mathcal{C} $. Per concludere che la quaterna $ (\Sigma_m, \mathcal{C}, \mu_p, \sigma) $ è un sistema dinamico misurabile dobbiamo mostrare che $ \mu_p $ è $ \sigma $-invariante. \textcolor{red}{È sufficiente verificare l'invarianza sui cilindri}: per farlo basta infatti osservare che
\[
    \sigma^{-1}{
        \begin{pmatrix}
        0 & \cdots & k-1 \\
        j_0 & \cdots & j_{k-1}
        \end{pmatrix}
    }
    = \bigcup_{j=0}^{m-1}
    \begin{pmatrix}
    0 & 1 & \cdots & k \\
    j & j_0 & \cdots & j_{k-1}
    \end{pmatrix}
\]
ed essendo cilindri che fissano almeno una cifra diversa nella stessa posizione disgiunti otteniamo
\begin{align*}
    \mu_p\left(
        \sigma^{-1}{
            \begin{pmatrix}
            0 & \cdots & k-1 \\
            j_0 & \cdots & j_{k-1}
            \end{pmatrix}
        }
    \right)
    & = \sum_{j=0}^{m-1} \mu_p{
        \begin{pmatrix}
        0 & 1 & \cdots & k \\
        j & j_0 & \cdots & j_{k-1}
        \end{pmatrix}
    }
    = \sum_{j=0}^{m-1} p_j p_{j_0} \cdots p_{j_{k-1}} \\
    & = \left(\sum_{j=0}^{m-1} p_j\right) p_{j_0} \cdots p_{j_{k-1}} = p_{j_0} \cdots p_{j_{k-1}}.
\end{align*}
essendo $ p = (p_0, \ldots, p_{m-1}) $ vettore di probabilità. \\

Il sistema dinamico $ (\Sigma_m, \mathcal{C}, \mu_p, \sigma) $ è detto \emph{schema di Bernoulli} e viene spesso indicato con $ \operatorname{BS}(p) = \operatorname{BS}(p_0, \ldots, p_{m-1}) $. È un sistema ergodico come conseguenza di una proprietà più forte, enunciata nella seguente

\begin{proposition}
    Gli schemi di Bernoulli sono fortemente mescolanti.
\end{proposition}
\begin{proof}
    Sia $ \operatorname{BS}(p_1, \ldots, p_{m-1}) $ uno schema di Bernoulli. Essendo $ \mathcal{C} $ la $ \sigma $-algebra generata dai cilindri è \textcolor{red}{sufficiente verificare il mescolamento sui cilindri}. Siano
    \[
        A = \begin{pmatrix}
        0 & \cdots & k-1 \\
        a_0 & \cdots & a_{k-1}
        \end{pmatrix}
        \qquad
        B = \begin{pmatrix}
        0 & \cdots & h-1 \\
        b_0 & \cdots & b_{h-1}
        \end{pmatrix}
    \]
    Osserviamo che fissato $ n \in \N $
    \[
        \sigma^{-n}(A)
        = \bigcup_{j_0, \ldots, j_{n-1}=0}^{m-1}
        \begin{pmatrix}
        0 & \cdots & n-1 & n & \cdots & n+k-1 \\
        j_0 & \cdots & j_{n-1} & a_0 & \cdots & a_{k-1}
        \end{pmatrix}
    \]
    pertanto se $ n > h $ allora $ \sigma^{-n}(A) $ contiene i cilindri che iniziano con $ b_0, \ldots, b_{h-1} $. Quindi
    \[
    \sigma^{-n}(A) \cap B
    = \bigcup_{j_0, \ldots, j_{n-h-1}=0}^{m-1}
    \begin{pmatrix}
    0 & \cdots & h-1 & h & \cdots & n-1 & n & \cdots & n+k-1 \\
    b_0 & \cdots & b_{h-1} & j_0 & \cdots & j_{n-h-1} & a_0 & \cdots & a_{k-1}
    \end{pmatrix}.
    \]
    I cilindri nell'unione sono disgiunti, pertanto per $ n > h $
    \begin{align*}
        \mu_p(\sigma^{-n}(A) \cap B)
        & = \sum_{j_0, \ldots, j_{n-h-1}=0}^{m-1} \mu_p{
            \begin{pmatrix}
            0 & \cdots & h-1 & h & \cdots & n-1 & n & \cdots & n+k-1 \\
            b_0 & \cdots & b_{h-1} & j_0 & \cdots & j_{n-h-1} & a_0 & \cdots & a_{k-1}
            \end{pmatrix}
        } \\
        & = \sum_{j_0, \ldots, j_{n-h-1}=0}^{m-1} p_{a_0} \cdots p_{a_{k-1}} p_{j_0} \cdots p_{j_{n-h-1}} p_{b_0} \cdots p_{b_{h-1}} \\
        & = p_{a_0} \cdots p_{a_{k-1}} p_{b_0} \cdots p_{b_{h-1}} \sum_{j_0, \ldots, j_{n-h-1}=0}^{m-1} p_{j_0} \cdots p_{j_{n-h-1}} \\
        & = \mu_p(A) \mu_p(B) \left(\sum_{j_0=0}^{m-1} p_{j_0}\right) \cdots \left(\sum_{j_{n-h-1}=0}^{m-1} p_{j_{n-h-1}}\right) \\
        & = \mu_p(A) \mu_p(B) .
    \end{align*}
    dove nell'ultimo passaggio abbiamo usato che $ (p_1, \ldots, p_{m-1}) $ è un vettore di probabilità. Dunque definitivamente in $ n $ vale $ \mu_p(\sigma^{-n}(A) \cap B) = \mu_p(A) \mu_p(B) $ e quindi il sistema è mescolante.
\end{proof}
