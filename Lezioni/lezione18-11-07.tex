\section{Lezione del 07/11/2018 [Tantari]}
\subsection{Catene di Markov}
Sia $ I $ un insieme (che nel prosieguo supporremo essere di cardinalità finita, salvo esplicita menzione), che chiameremo \emph{spazio degli stati}.
Consideriamo uno spazio di probabilità ambiente $ (\Omega, \mathcal{F}, \PP) $ e una successione di variabili aleatorie $ {(X_{n})}_{n\in\N} $ a valori nello spazio degli stati
\[ X_{n}\colon \Omega\to I. \]
Possiamo considerare il push-forward della misura su $ \Omega $ nello spazio degli stati, ovvero, per ogni $ i\in I $ definiamo
\[ (X_{\sharp}\PP)(i) \coloneqq \PP\left(\left\{ \omega\in \Omega : X(\omega) = i \right\}\right) \equiv \PP(X=i). \]
Definiamo inoltre un \emph{vettore di probabilità} come un vettore di $ \R^{n} $ a entrate non negative i cui elementi sommino a 1, e una \emph{matrice stocastica} come una matrice $ n\times n $ a entrate non negative le cui righe sommino a 1.
Possiamo allora dare in questo contesto la seguente
\begin{definition}[Catena di Markov]
  Sia $ {(X_{n})}_{n\in\N} $ una successione definita come sopra; siano inoltre $ \lambda $ un vettore di probabilità e $ P $ una matrice stocastica aventi come dimensione la cardinalità di $ I $. Diciamo che $ {(X_{n})}_{n\in\N} $ è Markov $ (\lambda, P) $ se
  \begin{enumerate}[label=(\roman*)]
  \item $ {(X_{0})}_{\sharp}\PP = \lambda $, ovvero più esplicitamente: $ \forall i\in I\quad \lambda_{i} = \PP(X_{0} = i) $;
  \item $ \forall n\in\N, \forall i_{0}, \ldots, i_{n+1} \in I$ vale
    \[ \PP \left( X_{n+1} = i_{n+1} | X_{0} = i_{0}, \ldots, X_{n} = i_{n} \right) = \PP \left( X_{n+1} = i_{n+1} | X_{n} = i_{n} \right) = P_{i_{n}i_{n+1}}. \]
  \end{enumerate}
\end{definition}
\textcolor{red}{Grafo orientato e pesato}

\begin{thm}
  $ {(X_{n})}_{n\in\N} $ è Markov $ (\lambda, P) $ se e solo se $ \forall n\in\N, \forall i_{0}, \ldots, i_{n} \in I $
  \begin{equation}\label{eq:markov-prodotto}
    \PP\left( X_{0} = i_{0}, \ldots, X_{n} = i_{n} \right) = \lambda_{i_{0}} P_{i_{0}i_{1}} \cdots P_{i_{n-1}i_{n}}
  \end{equation}
  \begin{proof}
    \textcolor{red}{Mancante}
  \end{proof}

  \begin{thm}[Proprietà di Markov]
    Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora, se condiziono a $ X_{m} = i $ per un certo $ i\in I $, $ {(X_{m+n})}_{n\in\N} $ è Markov $ (\delta_{i}, P) $\footnote{Ricordiamo che $ \delta_{i}(j) \coloneqq \delta_{ij}\ \forall j\in I $.}  ed è indipendente da $ X_{0}, \ldots, X_{m} $. In simboli
    \begin{multline*}
      \PP\left( X_{0} = i_{0}, \ldots, X_{m} = i_{m}, \ldots, X_{m+n} = i_{m+n} | X_{m} = i \right) = \\
      = \PP\left( X_{0} = i_{0}, \ldots, X_{m-1} = i_{m-1} | X_{m} = i \right) \PP\left(X_{m} = i_{m}, \ldots, X_{m+n} = i_{m+n} | X_{m} = i \right)
    \end{multline*}
  \end{thm}

  \begin{thm}\label{thm:markov-prob}
    Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora $ \forall m,n\in\N $
    \begin{enumerate}[label=(\roman*)]
    \item\label{pt:markov-prob-gen} $ \PP\left(X_{n} = j\right) = {(\lambda P^{n})}_{j} $;
    \item $ \PP\left(X_{n} = j | X_{0} = i \right) = \PP\left( X_{m+n} = j | X_{m} = i \right) = {(P^{n})}_{ij} $.
    \end{enumerate}
  \end{thm}
  \begin{proof}
    \begin{enumerate}[label=(\roman*)]
    \item
      Usando la~\eqref{eq:markov-prodotto} abbiamo
      \begin{align*}
        \PP\left(X_{n} = j\right) & = \PP\left( \bigsqcup_{i_{0} \cdots i_{n-1}} \left\{ X_{0} = i_{0}, \ldots, X_{n-1} = i_{n-1}, X_{n} = j \right\} \right) \\
                                  & = \sum_{i_{0} \cdots i_{n-1}} \lambda_{i_{0}} P_{i_{0}i_{1}} \cdots P_{i_{n-1}j} = \sum_{i_{0}} \lambda_{i_{0}} {(P^{n})}_{i_{0}j} = {\left(\lambda P^{n}\right)}_{j};
      \end{align*}
    \item \textcolor{red}{Segue dalla proprietà di Markov.}\qedhere
    \end{enumerate}
  \end{proof}
\end{thm}

\subsubsection{Accessibilità degli stati}
\begin{definition}\
  \begin{itemize}
  \item Diciamo che \emph{esiste un cammino} da $ i $ a $ j $ ($ i\to j $), se $ \exists n\in\N$ tale che $ {(P^{n})}_{ij}>0 $;
  \item Diciamo che due stati $ i $ e $ j $ sono \emph{comunicanti} ($ i \leftrightarrow j $) se esistono $ m,n\in\N $ tali che $ {(P^{m})}_{ij} > 0 $ e $ {(P^{n})}_{ji} > 0 $. Notiamo che la condizione di essere comunicanti definisce una relazione di equivalenza tra gli stati;
  \item Diciamo che un sottoinsieme $ C \subseteq I $ è chiuso se $ \forall i\in C, \forall j \in I \setminus C $, $ i \nrightarrow j $ ($ i $ non comunica con $ j $);
  \item Chiamiamo \emph{stato assorbente} un singoletto chiuso (contenuto in $ I $);
  \item Un sottoinsieme $ B\subseteq I $ si dice \emph{irriducibile} se $ \forall i,j\in B\ i \leftrightarrow j $.
    Se invece tutto $ I $ è irriducibile, diremo che anche la matrice stocastica $ P $ associata alla catena di Markov è irriducibile. Più esplicitamente
    \[ \forall i,j\in I\ \exists n\in\N : {(P^{n})}_{ij} > 0. \]
    Osserviamo infine che le classi di equivalenza della relazione $ \leftrightarrow $ sono gli insiemi irriducibili massimali.
    Questo naturalmente implica che $ P $ sia anche irriducibile. Si può inoltre verificare che se $ P $ è primitiva, allora $ \forall i,j\in I\ {(P^{m})}_{ij}>0 $ definitivamente in $ m $.
  \item Uno stato $ i\in I $ si dice \emph{aperiodico} se definitivamente in $ n $ vale $ {(P^{n})}_{ii} > 0 $. Se $ i $ non è aperiodico, definiamo il \emph{periodo} di $ i $ come $ d_{i} \coloneqq \operatorname{MCD}\left\{n\in\N : {(P^{n})}_{ii} > 0 \right\} $;
  \end{itemize}
\end{definition}

\begin{proposition}
  Sia $ P $ una matrice stocastica irriducibile di dimensione $ n $. Allora \[ \tilde{P} = \frac{1}{2}(P+\Id_{n}) \] è anche primitiva.
\end{proposition}
\begin{proof}
  Dato che ogni matrice commuta con l'identità possiamo scrivere $ {(P+\Id_{n})}^{m} = \sum_{k=0}^{m} \binom{m}{k} P^{k}\Id_{n}^{{m-k}} \geq \sum_{k=0}^{m} P^{k} $ e quindi $ \left({(P+\Id_{n})}^{m}\right)_{ij} \geq \sum_{k=0}^{m} {(P^{k})}_{ij} > 0 $ se scegliamo $ m $ maggiore o uguale al massimo tra gli esponenti che rendono $ P $ irriducibile.
\end{proof}

\begin{proposition}
  Uno stato $ i\in I $ è aperiodico se e solo se $ d_{i} = 1 $.
  \textcolor{red}{Prima è stato definito solo per stati periodici.}
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

\begin{proposition}
  Sia $ P $ matrice stocastica irriducibile, e supponiamo esista $ i\in I $\footnote{È qui essenziale l'ipotesi iniziale che la cardinalità di $ I $ sia finita.}  aperiodico. Allora $ P $ è primitiva.
\end{proposition}
\begin{proof}
  \textcolor{red}{Mancante}
\end{proof}

\begin{example}
  Consideriamo $ P = \begin{psmallmatrix} 0 & 1 \\ 1 & 0 \end{psmallmatrix} $. Si può verificare che questa è irriducibile ma non primitiva.
  Considerando invece $ \tilde{P} = \frac{1}{2}(P+\Id_{2}) = \begin{psmallmatrix} 1/2 & 1/2 \\ 1/2 & 1/2 \end{psmallmatrix} $ otteniamo una matrice stocastica primitiva (che corrisponde ad aver aggiunto la possibilità di rimanere in ciascuno stato a una data estrazione).
\end{example}

\subsubsection{Ricorrenza degli stati}
\begin{definition}
  Per ogni $ i\in I $ definiamo le variabili aleatorie $ V_{i}\colon \Omega\to\N\cup\{+\infty\} $ nel seguente modo:
  \[ V_{i} \coloneqq \sum_{n=0}^{+\infty} \chi_{\{X_{n} = i\}}. \]
  Le $ V_{i} $ contano dunque quante volte lo stato $ i $ viene visitato nella ``evoluzione temporale''.
  Introduciamo inoltre il numero di visite allo stato $ i $ prima del tempo $ n $
  \[ V_{i}(n) \coloneqq \sum_{k=0}^{n-1} \chi_{\{X_{k} = i\}}. \]
\end{definition}
\begin{definition}
  Uno stato $ i\in I $ si dice
  \begin{itemize}
  \item \emph{ricorrente} se $ \PP\left(V_{i} = +\infty \right) = 1 $ cioè se lo stato $ i $ viene visitato infinite volte quasi certamente;
  \item \emph{transiente} se $ \PP\left(V_{i} = +\infty \right) = 0 $.
  \end{itemize}
\end{definition}

\begin{definition}[tempo di $ r $-esimo passaggio]
  Definiamo ulteriori variabili aleatorie $ \Omega\to\N\cup \{+\infty\} $:
  \[ T_{i}^{(r)} \coloneqq \inf\{ n\geq T_{i}^{(r-1)}+1 : X_{n} = i \}. \]
  Il caso $ r = 1 $, cioè il tempo di primo passaggio, è invece definito come
  \[ T_{i}^{(1)} \equiv T_{i} \coloneqq \inf\{n\geq 1 : X_{n} = i \} \]
  ricordando che $ \inf\emptyset = +\infty $.
\end{definition}

\begin{definition}[Lunghezza della $ r $-esima escursione]
  \begin{equation}\label{eq:markov-escursione}
    S_{i}^{(r)} \coloneqq
    \begin{cases}
      T_{i}^{(r)} - T_{i}^{(r-1)} & \text{se } T_{i}^{(r-1)} < +\infty \\
      0                           & \text{altrimenti}
    \end{cases}
  \end{equation}
\end{definition}

\begin{lemma}
  Sia $ A $ un evento generato da $ \left\{ X_{m} : m \leq T_{i}^{(r-1)} \right\} $. Allora
  \[ \PP\left( S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty, A \right) = \PP\left( S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty \right). \]
\end{lemma}

\begin{lemma}
  Vale
  \[ \PP\left(S_{i}^{(r)} = k \middle| T_{i}^{(r-1)} < +\infty \right) = \PP\left( T_{i} = k \middle| X_{0} = i \right). \]
\end{lemma}

\begin{lemma}
  Posto $ f_{i} \coloneqq \PP(T_{i} < +\infty | X_{0} = i ) $, si ha
  \[ \PP\left( V_{i} > r \middle| X_{0} = i \right) = {(f_{i})}^{r}. \]
\end{lemma}

\begin{thm}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $. Allora
  \begin{enumerate}[label=(\roman*)]
  \item $ f_{i} = 1\quad \iff\quad i $ è ricorrente $ \quad \iff\quad \sum_{n\geq 0} {(P^{n})}_{ii} = +\infty $;
  \item $ f_{i} < 1\quad \iff\quad i $ è transiente $ \quad \iff\quad \sum_{n\geq 0} {(P^{n})}_{ii} < +\infty $.
  \end{enumerate}
\end{thm}

\subsubsection{Convergenza}
Ci occupiamo qui di enunciare alcuni risultati che riguardano la convergenza della successione
\[ \lambda^{(n)} \coloneqq \lambda P^{n}, \]
che rappresenta, per il punto~\ref{pt:markov-prob-gen} del Teorema~\ref{thm:markov-prob}, la probabilità degli stati al tempo $ n $-esimo.
Se tale successione ammette limite $ \pi $, necessariamente dev'essere
\[ \pi P = \left( \lim_{n\to+\infty} \lambda P^{n} \right) P = \lim_{n\to+\infty} \lambda P^{n+1} = \pi \]
cioè la distribuzione $ \pi $ è \emph{invariante}.

\begin{thm}[convergenza all'equilibrio]\label{thm:markov-convergenza}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $, con $ P $ irriducibile e ogni $ i\in I $ aperiodico. Allora $ \exists! \pi $ distribuzione invariante. Inoltre
  \begin{tasks}(2)
    \task\label{pt:markov-convergenza-prob} $ \lim\limits_{n\to+\infty} \PP(X_{n} = j) = \pi_{j} \quad \forall j\in I $;
    \task\label{pt:markov-convergenza-mat} $ \lim\limits_{n\to+\infty} {(P^{n})}_{ij} = \pi_{j} \quad \forall i,j\in I $.
  \end{tasks}
\end{thm}

\begin{example}
  Facciamo un controesempio al Teorema~\ref{thm:markov-convergenza} nel caso in cui $ P $ non sia primitiva. Prendendo $ P = \begin{psmallmatrix} 0 & 1 \\ 1 & 0 \end{psmallmatrix} $ si ha
  \[
    \lambda^{(n)} =
    \begin{cases}
      \begin{psmallmatrix}
        \lambda_{1} & \lambda_{2}
      \end{psmallmatrix}
      & \text{per }n \text{ pari}\\
      \begin{psmallmatrix}
        \lambda_{2} & \lambda_{1}
      \end{psmallmatrix}
      & \text{per }n \text{ dispari}
    \end{cases}
  \]
  e quindi non può convergere.
\end{example}

\begin{thm}[ergodico per catene irriducibili]\label{thm:markov-ergodico}
  Sia $ {(X_{n})}_{n\in\N} $ Markov $ (\lambda, P) $, con $ P $ irriducibile. Allora
  \begin{enumerate}[label=(\roman*)]
  \item Posto $ m_{i} \coloneqq \E_{i}[T_{i}] $ vale
    \[ \PP\left(\frac{V_{i}(n)}{n} \to \frac{1}{m_{i}} \right) = 1; \]
  \item $ \forall f\colon I\to\R $ limitata vale
    \[ \PP\left(\frac{1}{n} \sum_{k=0}^{n-1} f(X_{k}) \to \sum_{i\in I} \frac{f(i)}{m_{i}} \right) = 1. \]
  \end{enumerate}
\end{thm}
