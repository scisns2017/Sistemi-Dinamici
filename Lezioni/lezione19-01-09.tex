\section{Lezione del 09/01/2019 [Marmi]}
\subsection{Entropia metrica o di Kolmogorov-Sinai}

\begin{definition}[join di due partizioni]
	Se $ \mathcal{P} \coloneqq \{A_1, \ldots, A_n\} $ e $ \mathcal{Q} \coloneqq \{B_1, \ldots, B_m\} $ sono due partizioni di $ X \pmod{0} $ si definisce il join di $ \mathcal{P} $ e $ \mathcal{Q} $ la partizione
	\[
	\mathcal{P} \vee \mathcal{Q} \coloneqq \{A_i \cap B_j : i = 1, \ldots, n \text{ e } j = 1, \ldots, m\}.
	\]
\end{definition}

Consideriamo ora un sistema dinamico misurabile $ (X, \mathcal{A}, \mu, f) $ e il join delle iterazioni di $ f $ su $ \mathcal{P} $. Posto $ f^{-k}\mathcal{P} \coloneqq \{f^{-k}(A_i) : A_i \in \mathcal{P}\} $ tale join è
\[
\mathcal{P}^{(n)} \coloneqq \mathcal{P} \vee (f^{-1}\mathcal{P}) \vee \cdots \vee (f^{-(n-1)}\mathcal{P}) = \bigvee_{i=0}^{n-1}f^{-i}\mathcal{P}.
\]
Tale costruzione ha lo scopo di quantificare l'informazione media che si ottiene facendo la \emph{dinamica simbolica} dell'iterazione di $ f $ usando la partizione $ \mathcal{P} $. Alla partizione $ \mathcal{P} = \{A_1, \ldots, A_n\} $ associamo l'insieme di simboli $ \{a_1, \ldots, a_n\} $. Consideriamo l'orbita di un punto $ x \in X $ sotto l'azione di $ f $: al passo $ n $-esimo dell'iterazione codifichiamo il blocco della partizione in cui sta $ f^{n}(x) $ con il simbolo $ a_{i_n} $ associato alla partizione, e così per ogni $ n \in \N $. In tale modo all'orbita corrisponde una successione $ (a_{i_n})_{n\in\N} $ che è la codifica dell'orbita data dalla scelta della partizione.

Ora se $ B \in \mathcal{P}^{(n)} $ vuol dire che è della forma $ B = A_{i_0} \cap f^{-1}(A_{i_1}) \cap \cdots \cap f^{-(n-1)}(A_{i_{n-1}}) $: più esplicitamente se $ x \in B $ vuol dire che $ x \in A_{i_0}, f(x) \in A_{i_1}, \ldots, f^{n-1}(x) \in A_{i_{n-1}} $. In altri termini $ B \in \mathcal{P}^{(n)} $ è l'insieme degli $ x \in X $ che hanno come dinamica simbolica fino all'$ n $-esima iterazione la stringa $ (a_{i_0}, \ldots, a_{i_{n-1}}) $.

Pertanto $ H(\mathcal{P}^{(n)}) $ quantifica, in un certo senso, l'informazione media che si ottiene sulla dinamica di $ f $ conoscendo i primi $ n $ blocchi in cui è ``passata'' l'orbita di un elemento di $ X $ sotto $ f $. Osserviamo però che $ H(\mathcal{P}^{(n)}) $ è una quantità \emph{estensiva} in $ n $, nel senso che si stima dall'alto con $ n H(\mathcal{P}) $ (basta usare la Proposizione \ref{prop:entropia-metrica-subadditiva} e osservare che $ H(f^{-1}\mathcal{P}) = H(\mathcal{P}) $). Pertanto ha senso considerare l'entropia media per unità di lunghezza stringa: dividendo $ H(\mathcal{P}^{(n)}) $ per $ n $ (la lunghezza della stringa della dinamica simbolica) e prendendo il limite in $ n $ otteniamo quindi l'informazione media che si ottiene sulla dinamica di $ f $ facendo la dinamica simbolica usando come partizione dello spazio $ \mathcal{P} $. In formule
\begin{equation}
h_\mu(f, \mathcal{P}) \coloneqq \lim_{n\to+\infty} \frac{1}{n}H(\mathcal{P}^{(n)}) = \lim_{n\to+\infty} \frac{1}{n}H\left(\bigvee_{i=0}^{n-1}f^{-i}\mathcal{P}\right).
\end{equation}

Chiaramente tale limite dipende dalla partizione scelta, che poteva per esempio non essere ottimale per la codifica di $ f $: per definire un'entropia associata al sistema dinamico $ (X, \mathcal{A}, \mu, f) $ dovremo quindi prendere l'estremo superiore di tale limite al variare della partizione.

\begin{definition}[entropia di Kolmogorov-Sinai]
	Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. L'entropia del sistema è
	\begin{equation}
	h_\mu(f) \coloneqq \sup_\mathcal{P} h_\mu(f, \mathcal{P}) = \sup_\mathcal{P} \lim_{n\to+\infty} \frac{1}{n}H\left(\bigvee_{i=0}^{n-1}f^{-i}\mathcal{P}\right)
	\end{equation}
	dove $ H $ è l'entropia di Shannon della partizione e l'estremo superiore è fatto al variare di $ \mathcal{P} $ partizione finita di $ X \pmod{0} $.
\end{definition}
Per mostrare che questa è una buona definizione dimostriamo il seguente
\begin{lemma}[Fekete] \label{lem:fekete}
	Se $ a \colon \N\setminus\{0\} \to \R $ è una successione subadditiva (cioè tale che $ a_{n+m} \leq a_n + a_m $) allora esiste
	\[
	\lim_{n\to+\infty} \frac{a_n}{n} = \inf_{n\geq1}\frac{a_n}{n}.
	\]
\end{lemma}
\begin{proof}
	Sia $ \alpha = \inf_{n\geq1} (a_n/n) $. La disuguaglianza $ \liminf_{n\to\infty} (a_n/n) \geq \alpha $ è ovvia. Ci basta dimostrare che $ \limsup_{n\to\infty} (a_n/n) \leq \alpha $. Sia dunque $ \epsilon > 0 $; allora esiste $ \bar{n} $ tale che $ a_{\bar{n}}/\bar{n} \leq \alpha + \epsilon $. Riscriviamo ora $ n $ come $ n = k \bar{n} + r $ per opportuni $ k\in\N $ e $ r\in\{0,\ldots, \bar{n}-1\} $. Per la subaddività si ha $ a_{k\bar{n}+r} \leq a_{k\bar{n}} + a_r \leq ka_{\bar{n}} + a_r $ e quindi
	\[ \frac{a_n}{n} \leq \frac{k}{n}a_{\bar{n}} + \frac{1}{n}a_r \leq \frac{1}{\bar{n}}a_{\bar{n}} + \frac{1}{n} a_{\bar{r}}\]
	dove $ \bar{r} = \argmax\limits_{r\in\{ 0,\ldots, \bar{n}-1 \}} a_r $. Prendendo il $ \limsup $ si ha
	\[ \limsup_{n\to\infty} \frac{a_n}{n} \leq \frac{a_{\bar{n}}}{\bar{n}} \leq \alpha + \epsilon \]
	da cui, per l'arbitrarietà di $ \epsilon $, si conclude.
\end{proof}
Grazie a tale lemma ci basta allora dimostrare che la successione $ H(\mathcal{P}^{(n)}) $ è subadditiva in $ n $. Ora
\[ H(\mathcal{P}^{(n+m)}) = H\left(\bigvee_{i=0}^{n+m-1}f^{-i}\mathcal{P}\right) = H\left(P^{(m)} \vee \bigvee_{i=m}^{n+m-1}f^{-i}\mathcal{P}\right) \]
e d'altra parte
\[ H\left(\bigvee_{i=m}^{n+m-1}f^{-i}\mathcal{P}\right) = H\left(f^{-m}\bigvee_{i=0}^{n-1}f^{-i}\mathcal{P}\right) = H\left(\bigvee_{i=0}^{n-1}f^{-i}\mathcal{P}\right) = H(\mathcal{P}^{(n)}) \]
in quanto per $ f $-invarianza della misura, se $ \bigvee_{i=0}^{n-1}f^{-i}\mathcal{P} = \{B_1, \ldots, B_k\} $ allora $ f^{-m}\bigvee_{i=0}^{n-1}f^{-i}\mathcal{P} = \{f^{-m}(B_1), \ldots, f^{-m}(B_k)\} $ così $ \mu(f^{-m}(B_i)) = \mu(B_i) $ e l'entropia di Shannon delle due partizioni è la stessa. Per concludere ci basta basta quindi mostrare la seguente

\begin{proposition} \label{prop:entropia-metrica-subadditiva}
	Date due partizioni $ \mathcal{P} $ e $ \mathcal{Q} $ vale $ H(\mathcal{P} \vee \mathcal{Q}) \leq H(\mathcal{P}) + H(\mathcal{Q}) $.
\end{proposition}
\begin{proof}
	Siano $ \mathcal{P} = \{A_1, \ldots, A_n\} $ e $ \mathcal{Q} = \{B_1, \ldots, B_m\} $. Allora, per il Teorema \ref{thm:Shannon} punto \ref{pt:formula}, abbiamo che
	\[ H(\mathcal{P} \vee \mathcal{Q}) = H(\mathcal{P}) + \sum_{i, j} p_i f(t_{ij}) \]
	dove $ p_i = \mu(A_i) $ e $ t_{ij} = \frac{\mu(A_i \cap B_j)}{\mu(A_i)} $ e $ f(x) = - x \log{x} $. Essendo $ f $ concava e $ \sum_{i} p_i = 1 $ otteniamo per la disuguaglianza di Jensen
	\[ \sum_{i} p_i f(t_{ij}) \leq f\left(\sum_{i} p_i t_{ij}\right) = f(\mu(B_j)) = - \mu(B_j) \log{\mu(B_j)} \]
	così
	\[ \sum_{ij} p_j f(t_{ij}) = \sum_{j} \sum_{i} p_j f(t_{ij}) \leq - \sum_{j} \mu(B_j) \log{\mu(B_j)} = H(\mathcal{Q}). \qedhere \]
\end{proof}

\subsection{Entropia metrica e generatori}
\begin{definition}
    Siano $ \mathcal{F}_1 $ e $ \mathcal{F}_2 $ due sotto-$ \sigma $-algebre di $ (X, \mathcal{A}, \mu) $ spazio di misura. Diciamo che
    \begin{itemize}
        \item $ \mathcal{F}_1 \subseteq \mathcal{F}_2 \pmod{\mu} $ se $ \forall F_1 \in \mathcal{F}_1, \ \exists F_2 \in \mathcal{F_2} : \mu(F_1 \Delta F_2) = 0 $;
        \item $ \mathcal{F}_1 = \mathcal{F}_2 \pmod{\mu} $ se $ \mathcal{F}_1 \subseteq \mathcal{F}_2 \pmod{\mu} $ e $ \mathcal{F}_2 \subseteq \mathcal{F}_1 \pmod{\mu} $.
    \end{itemize}
\end{definition}

Sia ora $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile e $ \alpha $ una partizione di $ X \pmod{0} $. Definiamo
\begin{itemize}
    \item $ \alpha_{0}^{+\infty} = \bigvee_{i=0}^{+\infty}f^{-i}\alpha $ come la $ \sigma $-algebra generata da $ \bigcup_{i=0}^{+\infty} f^{-i}\alpha $ (ovvero la più piccola $ \sigma $-algebra che contiene tale unione);
    \item $ \alpha_{-\infty}^{+\infty} = \bigvee_{i=-\infty}^{+\infty}f^{-i}\alpha $ come la $ \sigma $-algebra generata da $ \bigcup_{i=-\infty}^{+\infty} f^{-i}\alpha $.
\end{itemize}

Diamo ora una caratterizzazione dell'entropia misurabile che permette di rimuovere l'estremo superiore quando si considera una classe particolare di partizioni.

\begin{definition}[generatore]
    Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Una partizione $ \alpha $ di $ X \pmod{0} $ è
    \begin{itemize}
        \item generatore se $ \alpha_{-\infty}^{+\infty} = \mathcal{A} \pmod{\mu} $;
        \item generatore \emph{forte} se $ \alpha_{0}^{+\infty} = \mathcal{A} \pmod{\mu} $.
    \end{itemize}
\end{definition}

\begin{exercise}
    Consideriamo $ ([0,1], \mathcal{B}, \lambda, f) $ dove $ \mathcal{B} $ è la $ \sigma $-algebra dei boreliani, $ \lambda $ è la misura di Lebesgue e $ f(x) = 2x \pmod{1} $. Mostrare che $ \alpha = \{[0, 1/2), [1/2, 1]\} $ è generatore forte. \\
    \emph{Hint}: gli intervalli diadici sono una base dei boreliani.
\end{exercise}

\begin{thm}[Kolmogorov-Sinai] \label{thm:Sinai}
     Sia $ (X, \mathcal{A}, \mu, f) $ un sistema dinamico misurabile. Se $ \alpha $ è una partizione di $ X \pmod{0} $ generatore allora
    \[
        h_{\mu}(f) = h_{\mathrm{\mu}}(f, \alpha).
    \]
\end{thm}

\subsubsection{Entropia metrica degli schemi di Bernoulli}
Sia $ \mathrm{BS}(p_0, \ldots, p_{m-1}) $ uno schema di Bernoulli, ovvero il sistema dinamico misurabile $ (\tilde{\Sigma}_m, \mathcal{C}, \mu_p, \sigma) $. Vogliamo mostrare che
\begin{equation}
    h_{\mu_p}(\sigma) = -\sum_{j=1}^{n} p_j\log{p_j}.
\end{equation}
Basta osservare che l'insieme del cilindri che fissano la posizione 0
\[
\alpha \coloneqq \left\{
\begin{pmatrix}
    0 \\ 0
\end{pmatrix}
,
\begin{pmatrix}
    0 \\ 1
\end{pmatrix}
, \cdots ,
\begin{pmatrix}
    0 \\ m-1
\end{pmatrix}
\right\}
=
\left\{
\begin{pmatrix}
    0 \\ j
\end{pmatrix}
\right\}_{j\in I}
\]
è una partizione finita di $ X \pmod{0} $ che è anche un generatore. Che sia una partizione è ovvio. Per quanto riguarda l'essere generatore si ha per ogni $ i\in\Z $
\[
    \sigma^{-i}\alpha = \left\{
    \begin{pmatrix}
    i \\ 0
    \end{pmatrix}
    ,
    \begin{pmatrix}
    i \\ 1
    \end{pmatrix}
    , \cdots ,
    \begin{pmatrix}
    i \\ m-1
    \end{pmatrix}
    \right\}
    =
    \left\{
    \begin{pmatrix}
    i \\ j
    \end{pmatrix}
    \right\}_{j\in I}
\]
da cui $ \bigcup_{i=-\infty}^{i=+\infty} \sigma^{-i}\alpha $ è composta da tutti gli insiemi della forma
\[
    \begin{pmatrix}
    i_0 & \cdots & i_k \\
    A_0 & \cdots & A_k
    \end{pmatrix}
\]
dove $ i_0, \ldots, i_k \in \Z $ e $ A_0, \ldots, A_k \subseteq I $. Chiaramente la $ \sigma $-algebra generata da $ \bigcup_{i=-\infty}^{i=+\infty} \sigma^{-i}\alpha $ coincide con la $ \sigma $-algebra generata dai cilindri. \\

Per il Teorema \ref{thm:Sinai} possiamo allora ridurci a calcolare l'entropia metrica sulla partizione $ \alpha $. Come fatto per l'entropia topologica si ha
\[
    \bigvee_{i=0}^{n-1}\sigma^{-i}\alpha = \left\{
    \begin{pmatrix}
    0 & \cdots & n-1 \\
    j_0 & \cdots & j_{n-1}
    \end{pmatrix} \right\}_{j_{0}, \ldots j_{n-1}=0}^{m-1}
\]
da cui
\begin{align*}
    H\left(\bigvee_{i=0}^{n-1}\sigma^{-i}\alpha\right)
    & = \sum_{j_0, \ldots, j_{n-1}=0}^{m-1}
    \mu_p\begin{pmatrix}
    0 & \cdots & n-1 \\
    j_0 & \cdots & j_{n-1}
    \end{pmatrix}
    \; \log{\left[\mu_p
        \begin{pmatrix}
        0 & \cdots & n-1 \\
        j_0 & \cdots & j_{n-1}
        \end{pmatrix}
        \right]
    } \\
    & = \sum_{j_0, \ldots, j_{n-1}=0}^{m-1} p_{j_0} \cdots p_{j_{n-1}} \log\left(p_{j_0} \cdots p_{j_{n-1}}\right) \\
    & = \sum_{j_0, \ldots, j_{n-1}=0}^{m-1} p_{j_0} \cdots p_{j_{n-1}} \log{p_{j_0}} + \cdots + \sum_{j_0, \ldots, j_{n-1}=0}^{m-1} p_{j_0} \cdots p_{j_{n-1}} \log{p_{j_{n-1}}} \\
    & = \sum_{j_0=0}^{m-1} p_{j_0} \log{p_{j_0}} + \cdots + \sum_{j_{n-1}=0}^{m-1} p_{j_{n-1}} \log{p_{j_{n-1}}} \\
    & = n \sum_{j=0}^{m-1} p_{j} \log{p_{j}}
\end{align*}
dove nella quarta uguaglianza abbiamo sommato sugli indici in cui non compare il logaritmo e usato che $ \sum_{j=0}^{m-1}p_j = 1 $. Così infine
\[
    h_{\mu_p}(\sigma) = \lim_{n\to+\infty} \frac{1}{n} H\left(\bigvee_{i=0}^{n-1}\sigma^{-i}\alpha\right) = \lim_{n\to+\infty} \frac{1}{n} n \sum_{j=0}^{m-1} p_{j} \log{p_{j}} = \sum_{j=0}^{m-1} p_{j} \log{p_{j}}.
\]

\subsection{Entropia e coniugazione/isomorfismi}
I seguenti risultati assicurano che le definizioni di di entropia che abbiamo dati sono invarianti rispetto alla coniugazione o isomorfismo di sistemi dinamici.
\begin{thm}
    Siano $ (X, d_X, f) $ e $ (Y, d_Y, g) $ due sistemi dinamici topologici topologicamente coniugati. Allora $ h_{\mathrm{top}}(f) = h_{\mathrm{top}}(g) $.
\end{thm}
\begin{thm}
    Siano $ (X, \mathcal{A}, \mu, f) $ e $ (Y, \mathcal{B}, \nu, g) $ due sistemi dinamici misurabili isomorfi. Allora $ h_\mu(f) = h_\nu(g) $.
\end{thm}
Il seguente teorema mostra che per gli schemi di Bernoulli vale anche l'implicazione inversa.
\begin{thm}[Ornstein]
    Due schemi di Bernoulli $ \mathrm{BS}(p_1, \ldots, p_n) $ e $ \mathrm{BS}(q_1,\ldots,q_n) $ sono isomorfi (come sistemi dinamici misurabili) se e solo se hanno la stessa entropia metrica.
\end{thm}

\subsection{Catene di Markov topologiche}
Sia $ \Gamma \subseteq \{1, \ldots, N\}^{2} $ un grafo \emph{connesso} e \emph{diretto} sui vertici $ \{1, \ldots, N\} $ con al più una sola freccia da $ i \to j $. A tale grafo possiamo associare la \emph{matrice di adiacenza} $ A $ che ci dice se esiste o meno un cammino da $ i \to j $:
\[
    A_{ij} = \begin{cases}
        1 & (i, j) \in \Gamma \\
        0 & (i, j) \notin \Gamma
    \end{cases}
\]

\begin{example} \label{ex:markov-top}
    \textcolor{red}{Immagine e matrice di adiacenza dell'esempio fatto a lezione, molto utile per avere un'idea concreta della definizione.}
\end{example}

Vogliamo modellizzare come sistema dinamico una ``passeggiata'' infinita nel futuro e nel passato su tale grafo. A tale scopo consideriamo l'insieme dei possibili cammini sul grafo
\[
    \Sigma_A \coloneqq \{x = (x_i)_{i\in\Z} \in \{1, \ldots, N\}^{\Z} : \forall i \in \Z, \ (x_i, x_{i+1}) \in \Gamma\}
\]
e lo \emph{shift} sinistro su $ N $ simboli $ \sigma \colon \Sigma_N \to \Sigma_N $ dove $ \Sigma_N \coloneqq \{1, \ldots, N\}^{\Z} $. Sappiamo già che se dotiamo $ \Sigma_N $ della distanza $ d(x, y) = N^{-a(x, y)} $ dove $ a(x, y) = \inf\{\abs{i} : x_i \neq y_i\} $, $ \Sigma_N $ diventa uno spazio metrico compatto e $ \sigma $ un omeomorfismo. Osserviamo ora che
\begin{itemize}
    \item $ \Sigma_A $ è invariante per lo \emph{shift} ovvero $ \sigma(\Sigma_A) \subseteq \Sigma_A $;
    \item $ \Sigma_A $ è un chiuso di $ \Sigma_N $ rispetto alla topologia indotta da $ d $ e quindi compatto.
\end{itemize}
Pertanto è ben definita la mappa $ \sigma_A \colon \Sigma_A \to \Sigma_A $ data dalla restrizione $ \sigma_A \coloneqq \sigma\lvert_{\Sigma_A} $ e la terna $ (\Sigma_A, d, \sigma_A) $ risulta essere un sistema dinamico topologico. Chiamiamo tale sistema dinamico \emph{catena di Markov topologica}.

\begin{exercise}
    Trovare una funzione $ f \colon [0, 1] \to [0, 1] $ lineare a tratti e una partizione di $ [0, 1] $ la cui dinamica simbolica sia la catena di Markov dell'esempio \ref{ex:markov-top}. \\
    \emph{Hint}: se fossero tutti collegati la mappa $ 3x \pmod{1} $ andrebbe bene.
\end{exercise}

\begin{definition}
    Sia $ A $ una matrice reale $ N \times N $ a coefficienti non negativi. Diciamo che $ A $ è:
    \begin{itemize}
        \item \emph{irriducibile} se $ \forall i, j, \; \exists k \in \N : (A^k)_{ij} > 0 $;
        \item \emph{primitiva} se $ \exists k \in \N : \forall i, j, \ (A^k)_{ij} > 0 $;
    \end{itemize}
\end{definition}

Ricordiamo inoltre i seguenti risultati
\begin{itemize}
    \item $ A $ irriducibile $ \Rightarrow $ $ \Gamma $ fortemente connesso, ovvero $ \forall i, j $ esiste un cammino finito $ i \to j $;
    \item $ A $ irriducibile $ \Rightarrow $ $ A + \Id $ primitiva;
    \item $ A $ primitiva con $ k \in \N $ tale che $ \forall i, j, \ (A^k)_{ij} > 0 $ $ \Rightarrow $ $ \forall h \geq k, \ \forall i, j, \ (A^{h})_{ij} > 0 $.
\end{itemize}

Al fine di calcolare l'entropia topologica di una catena di Markov (con qualche ipotesi aggiuntiva) enunciamo il seguente teorema ``sublime''.
\begin{thm}[Perron-Frobenius]
    Sia $ A $ una matrice primitiva. Allora esiste $ \lambda_A > 0 $ reale autovalore destro (o sinistro) di $ A $ che gode inoltre delle seguenti proprietà    \begin{enumerate}[label=(\roman*)]
        \item $ \forall \lambda \neq \lambda_A $ autovalore di $ A $ si ha $ \abs{\lambda} < \lambda_A $;
        \item autovettori destri (o sinistri) relativi a $ \lambda_A $ hanno componenti reali tutte strettamente positive;
        \item l'autospazio relativo a $ \lambda_A $ ha dimensione 1;
        \item $ \lambda_A $ è una radice semplice del polinomio caratteristico $ P_A(\lambda) $.
    \end{enumerate}
\end{thm}

Grazie al teorema di Perron-Frobenius possiamo in realtà mostrare il seguente enunciato che richiede ipotesi più deboli su $ A $ ma che ci fornisce disuguaglianze larghe.
\begin{thm}
    Sia $ A $ una matrice reale a coefficienti non negativi. Allora esiste un autovalore destro (o sinistro) $ \lambda_A \geq 0 $ tale che $ \forall \lambda $ autovalore destro (o sinistro) vale $ \abs{\lambda} \geq \lambda_A $ e autovettori destro (o sinistri) di $ \lambda_A $ hanno componenti reali non negative.
\end{thm}
\begin{proof}
    \textcolor{red}{Fissato $ \epsilon > 0 $ sia $ A(\epsilon) $ una matrice $ N\times N $ data da $ A_{ij}(\epsilon) = A_{ij} + \epsilon $. Essendo $ A $ a coefficienti non negatici, $ A(\epsilon) $ è a coefficienti strettamente positivi e dunque primitiva. Per il teorema di Perron-Frobenius esiste allora un autovalore destro reale $ \lambda_A(\epsilon) > 0 $ con le proprietà date dal teorema. Da qui si conclude passando le varie disuguaglianze al limite in $ \epsilon \to 0 $.}
\end{proof}

\begin{proposition} \label{thm:Perron-Frobenius-debole}
    Sia $ (\Sigma_A, d, \sigma_A) $ una catena di Markov topologica. Supponiamo che $ A $ matrice di adiacenza di $ \Gamma $ primitiva e sia $ \lambda_A $ l'autovalore dato dal teorema di Perron-Frobenius. Allora
    \begin{equation}
        h_{\mathrm{top}}(\sigma_A) = \log{(\lambda_A)}.
    \end{equation}
\end{proposition}
\begin{proof}
    \textcolor{red}{Mancante, serve un lemma di analisi numerica}
\end{proof}

\subsection{Catene di Markov misurabili}
Sia $ \Gamma \subseteq \{1, \ldots, N\}^{2} $ un grafo \emph{connesso} e \emph{diretto} sui vertici $ \{1, \ldots, N\} $ con al più una sola freccia da $ i \to j $. Oltre alla matrice di adiacenza $ A $, consideriamo una \emph{matrice stocastica} $ P $, ovvero una matrice $ N \times N $ a entrate reali non negative tale che $ \sum_{j=1} P_{ij} = 1 $ per ogni riga $ i $ della matrice. L'elemento $ P_{ij} $ di tale matrice rappresenta la probabilità di transire dal vertice $ i $ al vertice $ j $ e pertanto se $ A_{ij} = 0 $ poniamo $ P_{ij} = 0 $, mentre se $ A_{ij} = 1 $ si avrà $ P_{ij} > 0 $.

Mostriamo ora che esiste un autovettore sinistro di $ P $ con autovalore 1. Sia infatti $ \lambda $ autovalore destro di $ P $ e $ v $ relativo autovettore $ Pv = \lambda v $: essendo $ P $ stocastica, $ 0 \leq P_{ij} \leq 1 $ quindi per ogni $ i \in \{1, \ldots, N\} $ si ha
\[
    \abs{\lambda} \abs{v_i} = \abs{\sum_{j} P_{ij} v_j} \leq \sum_{j} P_{ij} \abs{v_j} \leq \max_{j} \abs{v_j}
\]
da cui passando al massimo su $ i $ si ottiene $ \abs{\lambda} \leq 1 $. D'altra parte, sempre grazie al fatto che $ P $ è stocastica, $ v = (1, \ldots, 1) $ è autovettore con autovalore 1. Ma autovalori sinistri sono anche autovalori destri e viceversa, quindi per il Teorema \ref{thm:Perron-Frobenius-debole} sappiamo che esiste un autovettore sinistro di $ P $ con entrate non negative e con autovalore sinistro massimo, cioè 1. Sia $ p $ un tale autovettore con $ \sum_{i} p_i = 1 $. \\

Vogliamo ora generalizzare gli schemi di Bernoulli, come fatto per lo \emph{shift} nel caso topologico, ai percorsi permessi dalla matrice di adiacenza. Come nel caso topologico sia $ \Sigma_A = \{x \in \{1, \ldots, N\}^{\Z} : \forall i \in \Z, \ (x_i, x_{i+1}) \in \Gamma\} $ e $ \sigma_A \colon \Sigma_A \to \Sigma_A $ la restrizione dello \emph{shift} sinistro. Vogliamo dotare $ \Sigma_A $ di una $ \sigma $-algebra e di una misura che sia invariante per $ \sigma_A $. Definiamo i cilindri come la restrizione dei cilindri definiti per gli schemi di Bernoulli ai percorsi permessi
\[
    \begin{pmatrix}
    i_1 & \cdots & i_m \\
    j_1 & \cdots & j_m
    \end{pmatrix}_A
    \coloneqq
    \{x \in \Sigma_A : x_{i_1} = j_1, \ldots x_{i_m} = j_m\}
\]
Sia ora $ P $ la matrice stocastica di prima e $ p = (p_1, \ldots, p_N) $ un vettore di probabilità tale che $ p P = p $, che rappresenta la probabilità di un dato simbolo in $ j \in \{1, \ldots, N\} $ e $ P_{ij} $. Definiamo allora in modo naturale la misura del cilindro come
\[
    \mu{\left(\begin{pmatrix}
    i_1 & \cdots & i_k \\
    j_1 & \cdots & j_k
    \end{pmatrix}_A\right)}
    \coloneqq
    p_{j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k}.
\]
Osserviamo che tale misura è ben definita grazie al fatto che $ p $ è autovettore sinistro con autovalore 1: infatti
\[
    \begin{pmatrix}
    i_1 & \cdots & i_m \\
    j_1 & \cdots & j_m
    \end{pmatrix}_A
    =
    \bigsqcup_{j=1}^{N}
    \begin{pmatrix}
        i_1 - 1 & i_1 & \cdots & i_k \\
        j & j_1 & \cdots & j_k
    \end{pmatrix}_A
\]
da cui vogliamo che valga
\begin{align*}
    p_{j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k} & =
    \mu{\left(\begin{pmatrix}
        i_1 & \cdots & i_m \\
        j_1 & \cdots & j_m
        \end{pmatrix}_A\right)}
    =
    \sum_{j=1}^{N}
    \mu{\left(\begin{pmatrix}
        i_1 - 1 & i_1 & \cdots & i_k \\
        j & j_1 & \cdots & j_k
    \end{pmatrix}_A\right)} \\
    & = \sum_{j=1}^{N}
    p_j P_{j j_1} P_{j_1 j_2} \cdots P_{j_{k-1} j_k} = \left(\sum_{j=1}^{N}
    p_j P_{j j_1}\right) P_{j_1 j_2} \cdots P_{j_{k-1} j_k}
\end{align*}
ovvero $ \sum_{j} p_j P_{ji} = p_i $ per ogni $ i \in \{1, \ldots, N\} $. Tale misura si estende, in modo analogo a quanto fatto con gli schemi di Bernoulli, a una misura di probabilità sulla $ \sigma $-algebra $ \mathcal{C}_A $ generata dai cilindri su $ \Sigma_A $. Con lieve abuso di notazione continueremo ad indicare con $ \mu $ tale misura. Osserviamo che la misura dei cilindri su $ \Sigma_A $ è invariante per $ \sigma_A $, in quanto tale funzione agisce su tali cilindri cambiando gli indici ma non i simboli, e pertanto invariante su ogni elemento di $ \mathcal{C}_A $. \\

La quaterna $ (\Sigma_A, \mathcal{C}_A, \mu, \sigma_A) $ è quindi un sistema dinamico misurabile detto \emph{catena di Markov misurabile} data la \emph{matrice di transizione} $ P $ e la \emph{distribuzione iniziale} $ p $.

\begin{proposition}
    Sia $ (\Sigma_A, \mathcal{C}_A, \mu, \sigma_A) $ una catena di Markov misurabile con matrice di transizione $ P $ e distribuzione iniziale $ p $. Allora
    \begin{equation}
        h_{\mu}(\sigma_A) = -\sum_{i,j} p_i P_{ij} \log{P_{ij}}.
    \end{equation}
\end{proposition}
\begin{proof}
    \textcolor{red}{Mancante.}
\end{proof}
