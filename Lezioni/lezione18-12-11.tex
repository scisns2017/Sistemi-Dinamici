\section{Lezione dell'11/12/18 [Bindini]}
\subsection{Identità di Parseval}
\emph{Setting}: $ H $ spazio di Hilbert su $ \K = \R \text{ o } \C $. \\

Vedremo adesso un'ulteriore condizione equivalente alla completezza per un sistema ortonormale in uno spazio di Hilbert.
\begin{proposition}[identità di Parseval]\label{prop:Parseval}
    Sia $ \{e_n\} $ un sistema ortonormale in $ H $. Allora $ \{e_n\} $ è completo se e solo se vale l'identità di Parseval:
    \[ \forall v \in H,\ \norm{v}^2 = \sum_{n = 0}^{+\infty}\abs{\langle v, e_n \rangle}^2. \]
\end{proposition}
\begin{proof}
    Mostriamo che se $ \{e_n\} $ è completo vale l'identità di Parseval, utilizzando la prima caratterizzazione di completezza fornita dalla Proposizione \ref{prop:completezza}. Abbiamo che $\forall v \in H$, posto
    \[ S_N = \sum_{n=0}^{N}\langle v, e_n \rangle e_n, \]
    $ S_n \to v $ in norma $L^2$. Si ottiene quindi che:
    \[ \norm{S_N - v} \to 0 \quad \Rightarrow \quad \norm{S_N}^2 \to \norm{v}^2. \]
    Per Pitagora, però, essendo gli $ \{e_n\} $ ortonormali:
    \[ \norm{S_N}^2 = \sum_{n=0}^{N}\abs{\langle v, e_n \rangle}^2 \]
    da cui la tesi.\\
    Per l'implicazione opposta useremo invece la quarta caratterizzazione di completezza fornita dalla Proposizione \ref{prop:completezza}, e supponiamo quindi che per un qualche $v \in H$, $ \forall n \in \N,\ \langle v, e_n \rangle = 0 $. Parseval diventa:
    \[ \norm{v}^2 = \sum_{n=0}^{+\infty}\abs{\langle v, e_n \rangle}^2 = 0 \]
    da cui, per non degenerazione del prodotto scalare, $ v = 0 $, e quindi $ \{e_n\} $ è completo.
\end{proof}

\begin{exercise}
    Si consideri $ H = L^2([0,2\pi]) $ con la base di Fourier, ed il seguente elemento di $ H $:
    \[
    f(\theta) \coloneqq
    \begin{cases}
    1  & \text{se } 0 \leq \theta \leq \pi \\
    -1 & \text{se } \pi < \theta \leq 2\pi
    \end{cases}
    \]
    e si sfrutti l'identità di Parseval per calcolare $ \sum_{n=0}^{+\infty}\frac{1}{(2n+1)^2} $
\end{exercise}
\begin{solution}
    \textcolor{red}{Mancante}
\end{solution}

\subsection{Convergenza puntuale della serie di Fourier}
\emph{Setting}: $ H = L^2(\T^1) $ con $ \{e_n\}\ped{n\in\N} $ la base di Fourier.\\

Dimostreremo adesso alcuni criteri che forniscono la convergenza puntuale (o uniforme) della serie di Fourier di una funzione $ f \in H $, i cui coefficienti saranno indicati con $ a_n $ e le cui somme parziali saranno indicate con $ S_N = \sum_{n=0}^{N} a_n e_n $

\begin{proposition}\label{prop:fourier_puntuale_1}
    Sia $ f \in \mathcal{C}^k(\T^1) $\footnote{Ricordiamo che la continuità e differenziabilità sul toro $\T^1$ sono più forti delle rispettive condizioni su $[0,2\pi]$, in quanto richiedono anche la continuità o differenziabilità in $ 0\equiv 2\pi $.} per qualche $ k \geq 0 $. Vale:
    \[ \forall n \in \N,\ \abs{a_n} \leq \frac{c}{n^k} \]
    dove $ c $ dipende da $ f $ ma non da $ n $.
\end{proposition}
\begin{proof}
    Osserviamo innanzitutto che $ f $ appartiene sicuramente ad $ L^2(\T^1) $, in quanto funzione continua su un compatto e quindi limitata. Procediamo adesso per induzione, osservando che il caso $ k = 0 $ è banale, in quanto essendo i coefficienti $ (a_n)_{n\in\N} $ in $\ell^2$, essi sono limitati. Supponiamo adesso vera la tesi per funzioni in $ \mathcal{C}^k $ e consideriamo $ f \in \mathcal{C}^{k+1} $. Integrando per parti:
    \begin{align*}
    a_n &= \frac{1}{\sqrt{2\pi}} \int_{0}^{2\pi} f(x) e^{-inx}\dif{x} = \left[ \frac{1}{-in\sqrt{2\pi}} f(x) e^{-inx} \right]_0^{2\pi} + \frac{1}{in\sqrt{2\pi}} \int_0^{2\pi} f'(x)e^{-inx}\dif{x}\\
    &= \frac{1}{in\sqrt{2\pi}} \int_0^{2\pi} f'(x)e^{-inx}\dif{x} = \frac{1}{in}b_n
    \end{align*}
    dove $ b_n $ sono i coefficienti di Fourier della derivata di $ f $, che è una funzione di $ \mathcal{C}^k $; per ipotesi induttiva vale quindi:
    \[ \abs{a_n} = \frac{\abs{b_n}}{n} \leq \frac{c}{n^k\cdot n} = \frac{c}{n^{k+1}} \]
    che dimostra il passo induttivo.
\end{proof}

\begin{corollary}
    Se $ f \in \mathcal{C}^2(\T^1) $ la serie di Fourier converge uniformemente:
    \[ \frac{1}{\sqrt{2\pi}}\sum_{n=-N}^{N}\langle f, e_n \rangle e^{inx}\  \touf \ f(x). \]
\end{corollary}
\begin{proof}
    Per $ k = 2 $ la Proposizione \ref{prop:fourier_puntuale_1} fornisce:
    \[ \abs{a_n} \leq \frac{c}{n^2}  \]
    ma, essendo $ \norm{e_n} = 1\ \forall n $ ed essendo la serie $ \sum_{n=1}^{+\infty}\frac{1}{n^2} $ assolutamente convergente, si ha convergenza totale della serie di Fourier e quindi convergenza uniforme.
\end{proof}

Diamo adesso un risultato più forte:
\begin{proposition}\label{prop:fourier_puntuale_2}
    Se $ f \in \mathcal{C}(\T^1) $ è $ \mathcal{C}^1 $ a tratti con derivata limitata, si ha convergenza uniforme della serie di Fourier:
    \[ \frac{1}{\sqrt{2\pi}}\sum_{n=0}^{N}\langle f, e_n \rangle e^{inx}\  \touf \ f(x). \]
\end{proposition}
\begin{proof}
    Analogamente a quanto fatto nella dimostrazione della Proposizione \ref{prop:fourier_puntuale_1} integrando per parti la definizione degli $ a_n $ si ottiene che:
    \[ a_n = -\frac{1}{in}b_n \]
    dove i $ b_n $ sono i coefficienti di Fourier della derivata, che sono ben definiti in quanto essa è limitata e quindi $ L^2 $. Da Parseval si ottiene:
    \[ \sum_{n\in\Z} \abs{b_n}^2 = \norm{f'} < +\infty \]
    da cui:
    \[ \sum_{n\in\Z} n^2\abs{a_n}^2 = \sum_{n\in\Z} \abs{b_n}^2 < +\infty. \]
    Per un noto lemma di analisi\footnote{Utilizzando la diseguaglianza di H\"older si ha: \[ \sum_{n=0}^{N} \abs{c_n} = \sum_{n=0}^{N} \frac{1}{n} \cdot n \abs{c_n} \leq \left(\sum_{n=0}^{N}\frac{1}{n^2}\right)^{1/2} \left(\sum_{n=0}^{N}n^2\abs{c_n}^2\right)^{1/2} \] e passando al limite in $ N $ si conclude.} ciò implica:
    \[ \sum_{n\in\Z} \abs{a_n} < +\infty \]
    e quindi la serie di Fourier converge totalmente e quindi uniformemente.
\end{proof}

Introduciamo un lemma che sarà utile nella dimostrazione del prossimo criterio:
\begin{lemma}[Riemann - Lebesgue]
    Sia $ f \in L^1(\R) $, $ \zeta \in \R $. Allora:
    \[ \lim\limits_{\abs{\zeta} \to +\infty} \int_\R f(x)e^{i\zeta x}\dif{x} = 0. \]
\end{lemma}
\begin{proof}
    Sia intanto $ f = \chi_{[a,b]} $. In tal caso:
    \[ \int_{\R} \chi_{[a,b]}(x)e^{i\zeta x} \dif{x} = \frac{1}{i\zeta}\left(e^{i\zeta b} - e^{i\zeta a}\right) \to 0. \]
    Procedendo per linearità e per densità delle caratteristiche in $ L^1 $ si conclude.\\ \textcolor{red}{Esplicitare meglio la conclusione.}
\end{proof}

Dimostriamo adesso un risultato che fornisce la convergenza puntuale della serie di Fourier in un singolo punto:

\begin{proposition}\label{prop:fourier_puntuale_3}
    Sia $ f\colon \T^1 \to \T^1 $ limitata e differenziabile in $ x_0 $. Allora la serie di Fourier converge puntualmente in $ x_0 $:
    \[ \lim\limits_{N \to +\infty} \frac{1}{\sqrt{2\pi}} \sum_{n=-N}^{N} \langle f, e_n \rangle e^{inx_0} = f(x_0). \]
\end{proposition}
\begin{proof}
    Sia:
    \[ S_N(x) = \frac{1}{\sqrt{2\pi}} \sum_{n=-N}^{N} \langle f, e_n \rangle e^{inx}. \]
    Allora:
    \begin{align*}
    S_N(x_0) &= \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(y) e^{-iny} e^{inx_0} \dif{y} = \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(y) e^{-in(y-x_0)} \dif{y}\\
    &= \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{-x_0}^{2\pi-x_0} f(z + x_0) e^{-inz} \dif{z} = \frac{1}{2\pi} \sum_{n=-N}^{N} \int_{0}^{2\pi} f(z + x_0) e^{-inz} \dif{z}\\
    &= \frac{1}{2\pi} \int_{0}^{2\pi} f(z + x_0) \left(\sum_{n=-N}^{N} e^{-inz} \right) \dif{z}
    \end{align*}
    dove si è operato il cambio di variabile $ z = y-x_0 $ e nella penultima uguaglianza si è usato il fatto che sia $ f $ che $ e^{inz} $ sono $ 2\pi $-periodiche. Riconosciamo adesso il nucleo di Dirichlet\footnote{o, più semplicemente, la somma parziale di una serie geometrica.}:
    \[ D_N(z) \coloneqq \sum_{n=-N}^{N} e^{-inz} = \frac{\sin\left[(2N+1)\frac{z}{2}\right]}{\sin\left(\frac{z}{2}\right)} \]
    e, osservando che $ \int_{0}^{2\pi} D_N(z) \dif{z} = 1 $ otteniamo:
    \begin{align*}
        S_N(x_0) - f(x_0) &=  \frac{1}{2\pi} \int_{0}^{2\pi} f(z + x_0) \left(\sum_{n=-N}^{N} e^{-inz} \right) \dif{z} - f(x_0)\\
         &= \int_{0}^{2\pi} D_N(z) f(z+x_0) \dif{z} - \int_{0}^{2\pi} D_N(z) f(x_0) \dif{z} \\
        &= \frac{1}{2\pi} \int_{0}^{2\pi} \frac{f(z + x_0) - f(x_0)}{2\sin\left(\frac{z}{2}\right)}\cdot 2\sin\left[(2N+1)\frac{z}{2}\right] \dif{z}.
    \end{align*}
    Osserviamo che il primo fattore è $ L^1 $ in quanto limitato, poiché per $ z\to 0 $ si ha $ 2\sin(\frac{z}{2}) \sim z $ e quindi esso tende a $ f'(x_0) $, mentre il secondo fattore si può scrivere come $ e^{i\frac{2N+1}{2}z} - e^{-i\frac{2N+1}{2}z} $; si può quindi applicare il lemma di Riemman - Lebesgue, ottenendo:
    \[ \lim\limits_{N\to +\infty}\left[S_N(x_0) - f(x_0)\right] = 0. \qedhere\]
\end{proof}

\begin{exercise}
    Si consideri $ f = \frac{x}{2} $. Si sfruttino l'identità di Parseval e la Proposizione \ref{prop:fourier_puntuale_3} con $ x_0 = \frac{\pi}{2} $ per calcolare $ \sum_{n=1}^{+\infty}\frac{1}{n^2} $ e $ \sum_{n=0}^{+\infty}\frac{(-1)^n}{2n+1} $.
\end{exercise}
\begin{solution}
    \textcolor{red}{Mancante}
\end{solution}
