\section{Lezione dell'08/01/2019 [Marmi]}

\subsection{Entropia topologica}

\emph{Setting}: $ (X, d) $ spazio metrico compatto non vuoto.

\begin{definition}[ricoprimento congiunto]
    Siano $ \alpha \coloneqq \{A_i\}_{i=1}^{N} $ e $ \beta \coloneqq \{B_j\}_{j=1}^{M} $ due ricoprimenti aperti finiti di $ X $\footnote{Esistono per compattezza.}. Definiamo il ricoprimento congiunto o join di $ \alpha $ e $ \beta $ come il ricoprimento aperto finito dato da
    \[
        \alpha \vee \beta \coloneqq \{A_i \cap B_j \neq \emptyset : A_i \in \alpha, B_j \in \beta\}.
    \]
    Data una collezione finita di ricoprimenti finiti $ \alpha^1, \ldots, \alpha^k $ di cardinalità $ N_r \in \N $ per $ r = 1, \ldots, k $ si definisce
    \[
        \bigvee_{r=1}^{k} \alpha^r \coloneqq \{A_{i_1} \cap \ldots \cap A_{i_k} \neq \emptyset : r = 1, \ldots, k, \ A_{i_r} \in \alpha^{r}, \ 1 \leq i_r \leq N_r\}.
    \]
\end{definition}

Sia ora $ f \colon X \to X $ una funzione continua. Dato che la preimmagine di un aperto tramite $ f $ è ancora un aperto, fissato un ricoprimento aperto finito $ \alpha \coloneqq \{A_1\, \ldots, A_m\} $, ha senso considerare $ f^{-k}\alpha \coloneqq \{f^{-k}(A_i) : A_i \in \alpha\} $ che è ancora un ricoprimento aperto finito di $ X $, e il ricoprimento congiunto delle preimmagini di $ \alpha $ tramite $ f $
\[
    \bigvee_{i=1}^{n-1} f^{-i}\alpha \coloneqq \{A_{i_0} \cap f^{-1}(A_{i_1}) \cap \ldots \cap f^{-(n-1)}(A_{i_{n-1}}) \neq \emptyset : r = 0, \ldots, n-1, \ 0 \leq i_r \leq m\}.
\]

\begin{definition}[sottoricoprimento]
    $ \beta $ è un sottoricoprimento di $ \alpha \coloneqq \{A_1, \ldots, A_m\} $ ricoprimento aperto finito di $ X $ se $ \beta \subseteq \alpha $ ed è ancora un ricoprimento, ovvero $ X \subseteq \bigcup_{B \in \beta} B $ e $ \forall B \in \beta, \exists i \in \{1, \ldots, m\} : B = A_i $.
\end{definition}

\begin{definition}[entropia di un ricoprimento]
    Sia $ (X, d) $ uno spazio metrico compatto. L'entropia di un ricoprimento aperto finito $ \alpha $ di $ X $ si definisce come
    \begin{equation}
        H(\alpha) \coloneqq \log N(\alpha)
    \end{equation}
    dove
    \[
        N(\alpha) \coloneqq \min{\{\card{(\beta)} : \text{$ \beta $ è un sottoricoprimento di $ \alpha $}}\}.
    \]
\end{definition}

\begin{lemma} \label{lem:entropia-topologica}
    L'entropia di un ricoprimento gode delle seguenti proprietà:
    \begin{enumerate}[label=(\roman*)]
        \item $ H(\alpha) \geq 0 $;
        \item se $ \beta $ è un sottoricoprimento di $ \alpha $ allora $ H(\beta) \geq H(\alpha) $;
        \item \emph{subadditività}: se $ \alpha $ e $ \beta $ sono ricoprimenti aperti finiti allora $ H(\alpha \vee \beta) \leq H(\alpha) + H(\beta) $;
        \item se $ f \colon X \to X $ continua allora $ H(f^{-1}\alpha) \leq H(\alpha) $. Se $ f $ è inoltre suriettiva allora si ha l'uguaglianza $ H(f^{-1}\alpha) = H(\alpha) $.
    \end{enumerate}
\end{lemma}
\begin{proof} Mostriamo i vari punti.
    \begin{enumerate}[label=(\roman*)]
        \item Essendo $ X \not = \emptyset $, $ N(\alpha) \geq 1\ \forall\alpha $, quindi $ H(\alpha) \geq 0 $.
        \item Notiamo che se $ \beta $ è un sottoricoprimento di $ \alpha $ allora tutti i sottoricoprimenti di $ \beta $ sono anche sottoricoprimenti di $ \alpha $. Il minimo nella definizione di $ N(\beta) $ è quindi preso su un insieme di ricoprimenti più piccolo, ed è pertanto maggiore. Per monotonia del logaritmo si ha la tesi.
        \item Siano $ \alpha' $ e $ \beta' $ sottoricoprimenti rispettivamente di $ \alpha $ e $ \beta $ di cardinalità minima, ovvero tali per cui $ N(\alpha) = \card(\alpha') $ e $ N(\beta) = \card(\beta') $. Allora ovviamente $ \alpha' \vee \beta' $ è un sottoricoprimento di $ \alpha \vee \beta $, e quindi per il punto precedente vale:
        \[ N(\alpha \vee \beta) \leq N(\alpha' \vee \beta') \Rightarrow H(\alpha \vee \beta) \leq H(\alpha' \vee \beta'). \]
        Si ha però che:
        \begin{align*}
        H(\alpha' \vee \beta') & \leq \log(\card(\alpha' \vee \beta')) \leq \log(\card(\alpha') \cdot \card(\beta')) \\
        &= \log(\card(\alpha')) + \log(\card(\beta')) = H(\alpha) + H(\beta).
        \end{align*}
        \item Sia $ \alpha' $ sottoricoprimento di $ \alpha $ di cardinalità minima. Si ha che $ f^{-1}\alpha' \subseteq f^{-1}\alpha $ e che:
        \[ \bigcup_{A\in\alpha'}f^{-1}(A) = f^{-1}\left(\bigcup_{A\in\alpha'}A\right) = f^{-1}(X) = X \]
        ovvero che $ f^{-1}\alpha' $ è un sottoricoprimento di $ f^{-1}\alpha $, da cui:
        \[ N(f^{-1}\alpha) \leq \card(f^{-1}\alpha') \leq \card(\alpha') = N(\alpha) \Rightarrow H(f^{-1}\alpha) \leq H(\alpha). \]
        Sia ora $ f $ suriettiva, e sia $ \beta $ un sottoricoprimento di $ f^{-1}\alpha $ di cardinalità minima. Per ogni $ B \in \beta $ esiste, per definizione, un $ A_B\in\alpha $ tale per cui $ B = f^{-1}(A_B) $, e per suriettività di $ f $ tale $ A_B $ è unico. Sia allora $ \alpha'' = \{ A_B : B\in\beta \} \subseteq \alpha$, e vale quindi $ \card(\alpha'') = \card(\beta) = N(f^{-1}\alpha) $. Per suriettività si ha inoltre:
        \[ \bigcup_{A\in\alpha''}A = f\left(f^{-1}\left(\bigcup_{A\in\alpha''} A \right)\right) = f\left(\bigcup_{A\in\alpha''} f^{-1}(A) \right) = f(X) = X \]
        da cui $ \alpha'' $ è un sottoricoprimento di $ \alpha $. Se ne conclude:
        \[ N(\alpha) \leq \card(\alpha'') = N(f^{-1}\alpha) \Rightarrow H(\alpha) \leq H(f^{-1}\alpha). \qedhere \]
    \end{enumerate}
\end{proof}
Osserviamo che se $ f \colon X \to X $ è continua si ha:
{
\let\oldvee\bigvee
\renewcommand{\bigvee}{\textstyle\oldvee}
% Se sei un pazzo puoi usare anche questa versione:
%\renewcommand{\bigvee}{\mathop{\vphantom{sum}\vee}}
\begin{align*}
    H{\left(\bigvee_{i=0}^{n+m-1}f^{-i}\alpha\right)} &=
    H{\left(\left(\bigvee_{i=0}^{m-1}f^{-i}\alpha\right) \vee \left(\bigvee_{i=m}^{n+m-1}f^{-i}\alpha\right)\right)}\\
    &\leq H{\left(\bigvee_{i=0}^{m-1}f^{-i}\alpha\right)} + H{\left(\bigvee_{i=m}^{n+m-1}f^{-i}\alpha\right)}\\
    &= H{\left(\bigvee_{i=0}^{m-1}f^{-i}\alpha\right)} + H{\left(f^{-m}\left(\bigvee_{i=0}^{n-1}f^{-i}\alpha\right)\right)}\\
    &\leq H{\left(\bigvee_{i=0}^{m-1}f^{-i}\alpha\right)} + H{\left(\bigvee_{i=0}^{n-1}f^{-i}\alpha\right)}
    .
\end{align*}
}
Per il lemma di Fekete (Lemma \ref{lem:fekete}) la seguente è quindi una buona definizione.

\begin{definition}[entropia topologica o di Adler, Konheim, McAndrew]
    Sia $ (X, d, f) $ un sistema dinamico topologico. Fissato un ricoprimento aperto finito $ \alpha $ di $ X $ definiamo
    \begin{equation}
        h_{\mathrm{top}}(f, \alpha) \coloneqq \lim_{n \to +\infty} \frac{1}{n} H{\left(\bigvee_{i=0}^{n-1}f^{-i}\alpha\right)}.
    \end{equation}
    Definiamo quindi l'entropia topologica di $ (X, d, f) $ come
    \begin{equation}
        h_{\mathrm{top}}(f) \coloneqq \sup_\alpha{h_{\mathrm{top}}(f, \alpha)}.
    \end{equation}
    dove l'estremo superiore è fatto al variare di $ \alpha $ ricoprimento aperto finito di $ X $.
\end{definition}

La definizione di entropia topologia è molto simile a quella di entropia di Kolmogorov - Sinai data per sistemi dinamici misurabili. In questo caso, però, consideriamo gli aperti invece dei misurabili e assegnamo la stessa ``misura'' ad ogni insieme, pari a $ 1/N(\alpha) $.

\begin{exercise}
    Mostrare che l'identità su $(X, d) $ ha entropia topologica nulla.
\end{exercise}

Diamo ora una caratterizzazione dell'entropia topologica che permette di rimuovere l'estremo superiore quando si considera una classe particolare di ricoprimenti.

\begin{definition}[generatore]
    Sia $ (X, d, f) $ un sistema dinamico topologico con $ f \colon X \to X $ continua. Un ricoprimento aperto finito $ \alpha $ è generatore \emph{forte} se
    \[
        \forall \epsilon > 0, \ \exists N > 0 : \bigvee_{i=0}^{N}f^{-i}\alpha = \{B_1, \ldots, B_m\} \text{ e } \sup_{i} \diam{B_i} < \epsilon.
    \]
    Se $ f $ è in più un omeomorfismo allora si parla semplicemente di generatore se
    \[
        \forall \epsilon > 0, \ \exists N > 0 : \bigvee_{i=-N}^{N}f^{-i}\alpha = \{B_1, \ldots, B_m\} \text{ e } \sup_{i} \diam{B_i} < \epsilon.
    \]
    Notiamo infine che se la proprietà è valida per un qualche $ N $, è valida anche per tutti gli $ n \geq N $.
\end{definition}

\begin{definition}[raffinamento]
    $ \beta $ è un raffinamento di un ricoprimento aperto finito $ \alpha $ di $ X $ se $ \beta $ è un ricoprimento aperto finito di $ X $ e $ \forall B \in \beta, \exists A \in \alpha : B \subseteq A $. In tale caso scriviamo che $ \alpha \preceq \beta $ nel senso che $ \beta $ è ``più fine'' di $ \alpha $.
\end{definition}

Mostreremo adesso alcune proprietà dei raffinamenti che ci saranno utili.

\begin{lemma}\label{lem:prop-raffinamenti}
    Siano $ \alpha $, $ \beta $ e $ \gamma $ ricoprimenti aperti finiti, sia $ f $ continua. Valgono le seguenti:
    \begin{enumerate}[label=(\roman*)]
        \item\label{prop-raf:monotonia-join} $ \alpha \preceq \beta \ \Rightarrow\ (\alpha \vee \gamma) \preceq (\beta \vee \gamma) $
        \item\label{prop-raf:monotonia-preimm} $ \alpha \preceq \beta \ \Rightarrow\ f^{-1}\alpha \preceq f^{-1}\beta $
        \item\label{prop-raf:entropia} $ \alpha \preceq \beta \ \Rightarrow\ H(\alpha) \leq H(\beta) $
        \item\label{prop-raf:ent-top} $ \alpha \preceq \beta \ \Rightarrow\ h\ped{top}(f,\alpha) \leq h\ped{top}(f,\beta) $
    \end{enumerate}
\end{lemma}
\begin{proof}
	\textcolor{red}{Esercizio.}
\end{proof}

Prima di dimostrare il prossimo teorema, dimostreremo un'ulteriore proprietà dei ricoprimenti generatori

\begin{lemma}\label{lem:generatore-raffina}
    Se $ f \colon X \to X $ è continua, $ \alpha $ è un ricoprimento generatore forte e $ \beta $ è un ricoprimento, allora:
    \[ \exists N > 0\ :\ \forall n \geq N,\ \beta \preceq \bigvee_{i=0}^{n-1}f^{-i}\alpha \]
\end{lemma}
\begin{proof}
    Definiamo il numero di Lebesgue associato a $ \beta $ come:
    \[ \delta \coloneqq \sup \{r > 0\ :\ \forall x\in X, \exists B\in\beta : B_r(x) \subseteq B \} \]
    dove $ B_r(x) $ è la palla aperta di centro $ x $ e raggio $ r $. Per la proprietà di generatore forte:
    \[ \exists N>0 : \forall n \geq N,\ \sup \left\{\diam{A} : A\in\textstyle\bigvee_{i=0}^{n-1}f^{-i}\alpha \right\} < \delta. \]
    Sia allora $ n\geq N $ e $ A \in \bigvee_{i=0}^{n-1}f^{-i}\alpha $ che, avendo diametro minore di $ \delta $, è contenuto in una palla $ B_\delta(x_0) $ per qualche $ x_0 \in X $; per definizione di numero di Lebesgue esiste allora un $ B \in \beta $ tale per cui $ B_\delta(x_0) \subseteq B $, e quindi $ A \subseteq B_\delta(x_0) \subseteq B $.
\end{proof}

\begin{thm} \label{thm:entropia-top-generatore}
    Sia $ (X, d, f) $ un sistema dinamico topologico. Se $ \alpha $ è un ricoprimento generatore forte allora
    \[
        h_{\mathrm{top}}(f) = h_{\mathrm{top}}(f, \alpha).
    \]
    Se inoltre $ f $ è un omeomorfismo è sufficiente che $ \alpha $ sia un generatore.
\end{thm}
\begin{proof}
    Dimostreremo il teorema nel caso in cui $ f $ è continua e $ \alpha $ è un ricoprimento generatore forte.
    Per il Lemma \ref{lem:generatore-raffina} $ \exists N>0 : \forall n\geq N,\ \beta \preceq \bigvee_{i=0}^{n-1}f^{-i}\alpha $; sia allora $ \alpha_N = \bigvee_{i=0}^{N-1}f^{-i}\alpha $ e sia $ k > 0 $.
    Per i punti \ref{prop-raf:monotonia-preimm} e \ref{prop-raf:monotonia-join} del Lemma \ref{lem:prop-raffinamenti} si ha:
    \[ \left( \bigvee_{i=0}^{k-1} f^{-i}\alpha_N \right) \succeq \left( \bigvee_{i=0}^{k-1}f^{-i}\beta \right). \]
    Per il punto \ref{prop-raf:entropia} del lemma \ref{lem:prop-raffinamenti} vale allora:
    \[ H\left( \bigvee_{i=0}^{k-1} f^{-i}\alpha_N \right) \geq H\left( \bigvee_{i=0}^{k-1}f^{-i}\beta \right). \]
    Si ha però che:
    \begin{align*}
    \bigvee_{i=0}^{k-1} f^{-i}\alpha_N &= \bigvee_{i=0}^{k-1} f^{-i}\left( \bigvee_{j=0}^{N-1} f^{-j} \alpha\right) =  \bigvee_{i=0}^{k-1} \bigvee_{j=0}^{N-1} f^{-(i+j)} \alpha
    = \bigvee_{i=0}^{k-1} \bigvee_{h=i}^{N+i-1} f^{-h} \alpha \\ &= \alpha \vee \left(f^{-1}\alpha \vee f^{-1}\alpha\right) \vee \cdots \vee \left(f^{-(N+k-3)}\alpha \vee f^{-(N+k-3)}\alpha\right) \vee f^{-(N+k-2)}\alpha.
    \end{align*}
    Notiamo adesso che, anche se in generale per un ricoprimento $ \beta $ vale $ \beta \vee \beta \succeq \beta $, ai fini del calcolo dell'entropia:
    \[ H(\beta \vee \beta) = H(\beta) \]
    perché se $ \beta' $ è un sottoricoprimento di $ \beta \vee \beta $ di cardinalità minima, a ogni insieme $ B = B_1 \cap B_2 \in \beta' \subseteq \beta \vee \beta $ possiamo sostituire $ B_1 $ o $ B_2 $, che sono anch'essi elementi di $ \beta \vee \beta $, ottenendo un nuovo sottoricoprimento con la stessa cardinalità. Si ottiene quindi, estendendo questa osservazione al caso di join multipli ed applicandola a quanto ottenuto finora, che
    \[ H\left(\bigvee_{i=0}^{k-1} f^{-i}\alpha_N\right) = H\left(\bigvee_{h = 0}^{N+k-2} f^{-h}\alpha\right) \]
    e quindi che
    \[ H\left(\bigvee_{h = 0}^{N+k-2} f^{-h}\alpha\right) \geq H\left( \bigvee_{i=0}^{k-1}f^{-i}\beta \right). \]
    Dividendo per $ k $ e passando al limite si ottiene quindi
    \begin{align*}
    h\ped{top}(f, \alpha) &= \lim\limits_{k\to\infty}\frac{N+k-1}{k}\frac{1}{N+k-1} H\left(\bigvee_{h = 0}^{N+k-2} f^{-h}\alpha\right)\\
    &\geq \lim\limits_{k\to\infty}\frac{1}{k} H\left( \bigvee_{i=0}^{k-1}f^{-i}\beta \right)\\
    &= h\ped{top}(f, \beta)
    \end{align*}
    e passando quindi infine al $ \sup $ in $ \beta $ si ottiene
    \[ h\ped{top}(f,\alpha) \geq h\ped{top}(f) \Rightarrow h\ped{top}(f,\alpha) = h\ped{top}(f). \qedhere \]
\end{proof}

\subsubsection{Entropia topologica degli schemi di Bernoulli}
Sia $ X \coloneqq \{0, \ldots, m-1\}^{\Z} = I^{\Z} $ e $ \sigma \colon X \to X $ lo \emph{shift} sinistro su $ m $ simboli $ (\sigma(x))_i = x_{i+1} $. Abbiamo già visto che con la distanza $ d(x, y) \coloneqq m^{-a(x, y)} $ dove $ a(x, y) \coloneqq \min\{\abs{i} : x_{i} \neq y_{i}\} $ lo spazio $ X $ risulta essere metrico compatto e $ \sigma $ un omeomorfismo. \\
Vogliamo mostrare che il sistema dinamico $ (X, d, \sigma) $ ha entropia topologica
\begin{equation}
    h_{\mathrm{top}}(\sigma) = \log{(m)}.
\end{equation}
Osserviamo che i cilindri
\[
    \begin{pmatrix}
    i_1 & \cdots & i_m \\
    j_1 & \cdots & j_m
    \end{pmatrix}
    \coloneqq
    \{x \in X : x_{i_1} = j_1, \ldots, x_{i_m} = j_m\}
\]
sono un ricoprimento generatore forte di $ X $ e pertanto per il Teorema \ref{thm:entropia-top-generatore} basterà calcolare l'entropia su tale particolare ricoprimento generatore. In realtà possiamo prendere come ricoprimento quello più semplice e naturale
\[
    \alpha \coloneqq \left\{
    \begin{pmatrix}
    0 \\ 0
    \end{pmatrix}
    ,
    \begin{pmatrix}
    0 \\ 1
    \end{pmatrix}
    , \cdots ,
    \begin{pmatrix}
    0 \\ m-1
    \end{pmatrix}
    \right\}
    =
    \left\{
    \begin{pmatrix}
    0 \\ j
    \end{pmatrix}
    \right\}_{j\in I} .
\]
Ciascuno di questi cilindri è un aperto perché corrisponde a mettere una palla di centro $ i $ e raggio $ m^{-1} $. Per mostrare $ \alpha $ è un generatore forte ci basterà mostrare che fissato un $ \epsilon $ esiste un $ N \in \N $ tale che $ \forall A \in \bigvee_{n=-N}^{n=N}\sigma^{-n}\alpha $ si ha che $ \diam{A} < \epsilon $. Osserviamo che
\[
    \alpha \vee \sigma^{-1}\alpha = \left\{
    \begin{pmatrix}
    0 & 1 \\
    j_0 & j_1
    \end{pmatrix} \right\}_{j_0, j_1\in I}
    \qquad
    \sigma^{1}\alpha \vee \alpha \vee \sigma^{-1}\alpha = \left\{
    \begin{pmatrix}
    -1 & 0 & 1 \\
    j_{-1} & j_0 & j_1
    \end{pmatrix} \right\}_{j_{-1}, j_0, j_1 \in I}
\]
e che induttivamente si ha
\[
    \bigvee_{n=-N}^{n=N}\sigma^{-n}\alpha = \left\{
    \begin{pmatrix}
    -N & \cdots & 0 & \cdots & N \\
    j_{-N} & \cdots & j_0 & \cdots & j_N
    \end{pmatrix} \right\}_{j_{-N}, \ldots j_{N} \in I} .
\]
Ma i cilindri in questione non sono altro che palle di raggio $ m^{-2N} $ nella definita su $ X $ e pertanto se $ A \in A \in \bigvee_{n=-N}^{n=N}\sigma^{-n}\alpha $ il duo diametro è $ \diam{A} = m^{-2N} $ da cui basta prendere $ \epsilon > -(\log_m \epsilon)/2 $ per avere $ \diam{A} < \epsilon $. \\
Ora essendo i cilindri insiemi disgiunti,
\[
    \bigvee_{i=0}^{n-1}\sigma^{-i}\alpha = \left\{
    \begin{pmatrix}
    0 & \cdots & n-1 \\
    j_0 & \cdots & j_{n-1}
    \end{pmatrix} \right\}_{j_{0}, \ldots j_{n-1} \in I}
\]
è una partizione di $ X $ in aperti e pertanto coincide con il suo sottoricoprimento di cardinalità minima. Quindi
\[
    N\left(\bigvee_{i=0}^{n-1}\sigma^{-i}\alpha\right) = m^{n}
\]
da cui
\[
    h_{\mathrm{top}}(\sigma) = \lim_{n\to+\infty} \frac{1}{n} \log N\left(\bigvee_{i=0}^{n-1}\sigma^{-i}\alpha\right) = \lim_{n\to+\infty} \frac{1}{n} n \log m = \log{m}.
\]

\subsubsection{Formulazioni equivalenti dell'entropia topologica (Bowen)}

Nel seguito indicheremo con $ d_n(x,y) $ il massimo delle distanze assunte dalle immagini di due punti $ x $ e $ y $ nel segmento di orbita lungo $ n $:
\[ d_n(x,y) \coloneqq \max_{0 \leq i < n} d\left(f^n(x),f^n(y)\right). \]

\begin{definition}[Insieme $ (n,\epsilon) $-spanning]
    Diciamo che $ S \subseteq X $ è $ (n,\epsilon) $-spanning se $ \forall x\in X\ \exists y\in S $ tale che $ d_n(x,y)< \epsilon $.
    Chiamiamo quindi
    \[ s(n,\epsilon) \coloneqq \min\{ \card(S) : S\subseteq X, S \text{ è } (n,\epsilon)\text{-spanning}\}. \]
\end{definition}
In altre parole, in un insieme $ (n,\epsilon) $-spanning esiste un punto la cui orbita approssima bene, fino alla $ n $-esima iterazione, quella di un dato punto dello spazio. Il valore di $ s(n,\epsilon) $ ci dice dunque quanti punti dello spazio sono necessari se vogliamo descrivere segmenti di orbita lunghi $ n $ con precisione $ \epsilon $.
\begin{definition}[Insieme $ (n, \epsilon) $-separato]
    Diciamo che $ R \subseteq X $ è $ (n, \epsilon) $-separato se $ \forall x,y\in R, x\neq y\ d_n(x,y) > \epsilon $ e chiamiamo
    \[ r(n,\epsilon) \coloneqq \max \{ \card(R) : R\subseteq X, R \text{ è } (n,\epsilon)\text{-separato}\}. \]
\end{definition}
I punti di un insieme $ (n,\epsilon) $-separato sono quelli che riusciamo a distinguere se assumiamo di avere precisione $ \epsilon $ e confondiamo i punti i cui segmenti di orbita lunghi $ n $ rimangono sempre più vicini di $ \epsilon $. $ r(n,\epsilon) $ è quindi il massimo numero di punti che riusciamo a distinguere avendo precisione $ \epsilon $.

Entrambi i valori sopra definiti ci dicono ``quanta informazione'' è necessaria per descrivere il sistema con una fissata precisione $ \epsilon $. È dunque naturale aspettarsi che essi siano in relazione con l'entropia del sistema quando la precisione tende a zero, risultato espresso dal seguente
\begin{thm}
    Sia $ (X, d, f) $ un sistema dinamico topologico. Allora valgono
    \begin{align}
        h_{\mathrm{top}}(f) &= \lim_{\epsilon\to 0} \limsup_{n\to\infty} \frac{1}{n} \log s(n,\epsilon) \\
        h_{\mathrm{top}}(f) &= \lim_{\epsilon\to 0} \limsup_{n\to\infty} \frac{1}{n} \log r(n,\epsilon).
    \end{align}
\end{thm}
