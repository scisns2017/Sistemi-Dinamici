#!/bin/bash

cd Lezioni
docente=$(grep -r "\[$1\]" | cut -f 1 -d ":" | sort)
for i in $docente; do
    echo "\\input{Lezioni/$i}"
done
